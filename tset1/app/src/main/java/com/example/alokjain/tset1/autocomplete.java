package com.example.alokjain.tset1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import static java.lang.System.*;

public class autocomplete extends Activity implements View.OnClickListener {

    AutoCompleteTextView atvPlaces;
    PlacesTask placesTask;
    ParserTask parserTask;
    Button b,c;
    EditText loc,house;
    AutoCompleteTextView at;
    String name,mob,pass;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autocomplete);
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        mob=getIntent().getStringExtra("mob");
        atvPlaces = (AutoCompleteTextView) findViewById(R.id.atv_places);
        loc=(EditText)findViewById(R.id.etsector);
        house=(EditText)findViewById(R.id.ethouse);

        atvPlaces.setThreshold(1);
        atvPlaces.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        at=(AutoCompleteTextView)findViewById(R.id.atv_places);
        b=(Button)findViewById(R.id.bplace);
        b.setOnClickListener(this);
        c=(Button)findViewById(R.id.skip);
        c.setOnClickListener(this);
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception ", e.toString());
            Log.d("Exceptiondownload", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.bplace:
           String add=at.getText().toString();
           String locality=loc.getText().toString();
           String hno=house.getText().toString();
           if(add.length()==0||locality.length()==0||hno.length()==0)
           {
               //String m=new String("All Fields are mandatory!");
               Toast.makeText(autocomplete.this, "All Fields are mandatory!", Toast.LENGTH_SHORT).show();
               //showMessage(m);
           }
            else
           {
            String s=new String(add+" "+"added to your delivery address!");
               Useraddress u=new Useraddress(add,locality,hno,mob);
               Log.e("autocomplete", mob);
               //showMessage(mob);
               address(u);
               Toast.makeText(autocomplete.this, s, Toast.LENGTH_SHORT).show();
               //showMessage(s);
           }
                break;
            case R.id.skip:
                Intent intent = new Intent(this, shopage.class);//GET APP CONTEXT
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                intent.putExtra("mob",mob);
                startActivity(intent);
                break;
        }
    }
    private void address(Useraddress user) {
        ServerRequests serverRequest = new ServerRequests(this);
        serverRequest.StoreUserAddressInBackground(user, new GetUserCallback2() {
            @Override
            public void done(Useraddress returnedUser) {
                Intent intent = new Intent(autocomplete.this, shopage.class);//GET APP CONTEXT
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                intent.putExtra("mob",mob);
                startActivity(intent);
            }
        });
    }
    private void showMessage(String s) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(autocomplete.this);
        dialogBuilder.setMessage(s);
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            String key = "key=AIzaSyBZH6rNzyNqXGVzohsx08vwNeW_62F1JmM";

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;

            try {
                // Fetching the data from we service
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};

            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

            // Setting the adapter
            atvPlaces.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
