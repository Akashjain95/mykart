package com.example.alokjain.tset1;

/**
 * Created by Alok Jain on 11/4/2015.
 */
interface GetUserCallback {

    /**
     * Invoked when background task is completed
     */

    public abstract void done(User returnedUser);

}