package com.example.alokjain.tset1;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class Itemlistpage extends ActionBarActivity implements View.OnClickListener {
    Button b;
    String category;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    String name,mob,pass;
    List<Item>a=new ArrayList<Item>();

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        Log.e("akash", "LOW memory!");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(" ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itemlistpage);
        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Log.e("akash", "oncreate!");
        name=getIntent().getStringExtra("name");
         pass=getIntent().getStringExtra("pass");
        mob=getIntent().getStringExtra("mob");
         category=getIntent().getStringExtra("category");
        get(category);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_itemlistpage, menu);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setLogo(R.drawable.logo1);
        return super.onCreateOptionsMenu(menu);

    }
    private void addDrawerItems() {
        String[] osArray = { " ","Home", "View Cart", "Add delivery address" };
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = "garbage";
                text = parent.getItemAtPosition(position).toString();
                if (text.equals("Home")) {
                    Intent intent = new Intent(Itemlistpage.this, shopage.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    startActivity(intent);
                } else if (text.equals("View Cart")) {
                    Intent intent = new Intent(Itemlistpage.this, cartview.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    startActivity(intent);
                } else if (text.equals("Add delivery address")) {
                    Intent intent = new Intent(Itemlistpage.this, autocomplete.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    startActivity(intent);

                }
                mDrawerList.setItemChecked(position, true);
                setTitle(text);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Welcome" + " " + name + "!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.imageView1:
                Intent intent = new Intent(Itemlistpage.this, shopage.class);
                intent.putExtra("mob",mob);
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                startActivity(intent);
                return true;
            case R.id.imageView2:
                Intent intent2 = new Intent(Itemlistpage.this, cartview.class);
                intent2.putExtra("mob",mob);
                intent2.putExtra("name",name);
                intent2.putExtra("pass",pass);
                startActivity(intent2);
                return true;
            case R.id.descending:
                getordered(category,0);
                return true;
            case R.id.ascending:
                getordered(category,1);
            return true;
            case R.id.popularity:
                getordered(category,2);
             return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void get(String category) {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        Log.e("akash", "Server request to be made!");
        DisplayServerRequest serverRequest = new DisplayServerRequest(this);
        Log.e("akash", "Server request  made!");
        Log.e("akas cat",category);
        serverRequest.fetchSellerDataAsyncTask(category, new GetSellerCallback() {
            @Override
            public void done(List<Item> all) {
                Log.e("happened", "2");
                Log.e("SIZE+", String.valueOf(all.size()));
                if (all == null||all.size()==0) {
                    Toast.makeText(Itemlistpage.this, "No Items in stock currently!", Toast.LENGTH_SHORT).show();
                    //showMessage(" No Items in stock currently!");
                } else {
                    a = all;

                    for (int i = 0; i < all.size(); i++) {
                        Log.e("price", (String) all.get(i).price);
                        Log.e("qty", (String) all.get(i).quantity);

                    }
                    Toast.makeText(Itemlistpage.this,String.valueOf(all.size())+" "+"Items availaible in this category.", Toast.LENGTH_SHORT).show();
                    //showMessage(String.valueOf(all.size())+" "+"Items availaible in this category.");
                    populateListview();
                }
            }
        });
    }
    private void getordered(String category,int order) {
        Log.e("akash", "Server request to be made!");
        ServerRequests serverRequest = new ServerRequests(this);
        Log.e("akash", "Server request  made!");
        serverRequest.fetchSellerDataAsyncTaskordered(category,order, new GetSellerCallback() {
            @Override
            public void done(List<Item> all) {
                Log.e("happened", "2");
                Log.e("SIZE+", String.valueOf(all.size()));
                if (all == null||all.size()==0) {
                    Toast.makeText(Itemlistpage.this, "No Items in stock currently!", Toast.LENGTH_SHORT).show();
                    //showMessage(" No Items in stock currently!");
                } else {
                    a = all;

                    for (int i = 0; i < all.size(); i++) {
                        Log.e("price", (String) all.get(i).price);
                        Log.e("qty", (String) all.get(i).quantity);

                    }
                    //Toast.makeText(Itemlistpage.this,String.valueOf(all.size())+" "+"Items availaible in this category.", Toast.LENGTH_SHORT).show();
                    //showMessage(String.valueOf(all.size())+" "+"Items availaible in this category.");
                    populateListview();
                }
            }
        });
    }
    private void showMessage(String s) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setMessage(s);
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }


    public void populateListview()
    {
        ArrayAdapter<Item> adapter=new MyListAdapter();
        ListView list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
    }

    @Override
    public void onClick(View v)
    {
    /*switch(v.getId())
    {
        case R.id.imageView1:
        Intent intent = new Intent(Itemlistpage.this, shopage.class);  //GET APP
            intent.putExtra("mob",mob);
            intent.putExtra("name",name);
            intent.putExtra("pass",pass);
            startActivity(intent);
        break;
        case R.id.imageView2:
            Intent intent2 = new Intent(Itemlistpage.this, cartview.class);// GET APP
            intent2.putExtra("mob",mob);
            intent2.putExtra("name",name);
            intent2.putExtra("pass",pass);
            startActivity(intent2);
        break;

    }*/
    }

    private class  MyListAdapter extends  ArrayAdapter<Item>
    {
        public MyListAdapter()
        {
            super(Itemlistpage.this,R.layout.activity_item_view,a);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemview=convertView;
            if(itemview==null)
            {
                itemview=getLayoutInflater().inflate(R.layout.activity_item_view,parent,false);
            }
            final Item currentitem =a.get(position);
            //IMAGE
            ImageView imageView=(ImageView)itemview.findViewById(R.id.img);
            imageView.setImageBitmap(currentitem.decodedbyte);
            //PRICE
            TextView text1=(TextView) itemview.findViewById(R.id.price);
            text1.setText(currentitem.price);
            //QUANTITY
            TextView text2=(TextView) itemview.findViewById(R.id.quantity);
            text2.setText(currentitem.quantity);
            //DESCRIPTION
            TextView text3=(TextView) itemview.findViewById(R.id.description);
            text3.setText(currentitem.description);
           //PNAME OR PID
           TextView text4=(TextView)itemview.findViewById(R.id.pname);
            text4.setText(currentitem.pname);

           // /GET QUANTITY SELECTED BY CUSTOMER
            final EditText et=(EditText)itemview.findViewById(R.id.brought);

            Button b1=(Button)itemview.findViewById(R.id.angry_btn);
            final View finalItemview = itemview;

            b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final String pname = currentitem.pname;
                    //FIRST CHECK CAN BE ADDED?? ADD TO CART REQUEST SATISIFIED ONLY IF REQUESTED+INCART<=STOCK

                    final DisplayServerRequest s = new DisplayServerRequest(Itemlistpage.this);
                    s.fetchOrderDataAsyncTask(mob, new GetOrderCallback2() {
                        @Override
                        public void done(List<order> allorder) {
                            int sum=0,g=0;
                            for (int i = 0; i < allorder.size(); i++) {
                                if (pname.equals(allorder.get(i).pid))
                                    sum = sum + allorder.get(i).quantity;
                            }
                            Log.e("U ALREADY PLACED=", String.valueOf(s));

                            EditText et1 = (EditText) finalItemview.findViewById(R.id.brought);
                            int qt = Integer.parseInt(et1.getText().toString());
                            if ((sum + qt) > Integer.parseInt(currentitem.quantity)) {
                                g= 1;
                                Toast.makeText(Itemlistpage.this, "You cannot add more items in Cart than in Stock!", Toast.LENGTH_SHORT).show();
                               // showMessage("You cannot add more items in Cart than in Stock!");
                            }
                            if(g==0)
                            {    // Toast.makeText(Itemlistpage.this, "You can click on Cart option to Checkout anytime!", Toast.LENGTH_SHORT).show();
                                //showMessage("You can click on Cart option to Checkout anytime!");
                                int cost = qt * Integer.parseInt(currentitem.price);
                                order neworder = new order(mob, pname, qt, cost);
                                //MAKE SERVER REQUEST TO ADD TO CART
                                ServerRequests serverRequests = new ServerRequests(Itemlistpage.this);
                                serverRequests.storeorderInBackground(neworder, new GetOrderCallback() {
                                    @Override
                                    public void done(int f) {
                                        if (f == 1) {
                                            Toast.makeText(Itemlistpage.this, "Order added to Cart successfully", Toast.LENGTH_SHORT).show();
                                            //showMessage("Order added to Cart successfully");
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
            Button b2=(Button)itemview.findViewById(R.id.addbtn);
            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                int q=Integer.parseInt(et.getText().toString());
                if(q<Integer.parseInt(currentitem.quantity))
                {
                    q++;
                et.setText(Integer.toString(q));
                }
                }
            });
            Button b3=(Button)itemview.findViewById(R.id.subbtn);
            b3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int q=Integer.parseInt(et.getText().toString());
                    if(q>1)
                    {
                    q--;
                        et.setText(Integer.toString(q));
                    }
                }
            });
            Button review=(Button)itemview.findViewById(R.id.reviewbtn);
            review.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(Itemlistpage.this, reviewactivity.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    intent.putExtra("pid",currentitem.pname);
                    startActivity(intent);
                }
            });
            return itemview;
        }
    }

}
