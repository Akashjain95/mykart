package com.example.alokjain.tset1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class offerzone extends Activity {
  String name,pass,mob;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offerzone);
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        mob=getIntent().getStringExtra("mob");

        ServerRequests s=new ServerRequests(offerzone.this);
        s.fetchrandom(new GetSellerCallback2() {
            @Override
            public void done(final Item item) {
                if(item==null||item.pname==null)
                {    Toast.makeText(offerzone.this, "Sorry No Offers For Today!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(offerzone.this, shopage.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    startActivity(intent);
                }
                else
                {
             TextView tv1=(TextView)findViewById(R.id.pname);
             TextView tv2=(TextView)findViewById(R.id.price);
                TextView tv3=(TextView)findViewById(R.id.quantity);
                TextView tv4=(TextView)findViewById(R.id.description);
                ImageView imageView=(ImageView)findViewById(R.id.img);
                imageView.setImageBitmap(item.decodedbyte);
                tv1.setText(item.pname);
                tv2.setText(item.price);
                tv3.setText(item.quantity);
                tv4.setText(item.description);
                //Log.e("prodd name",item.pname);
                // /GET QUANTITY SELECTED BY CUSTOMER
                final EditText et=(EditText)findViewById(R.id.brought);

                Button b1=(Button)findViewById(R.id.angry_btn);


                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final String pname = item.pname;
                        //FIRST CHECK CAN BE ADDED?? ADD TO CART REQUEST SATISIFIED ONLY IF REQUESTED+INCART<=STOCK

                        final DisplayServerRequest s = new DisplayServerRequest(offerzone.this);
                        s.fetchOrderDataAsyncTask(mob, new GetOrderCallback2() {
                            @Override
                            public void done(List<order> allorder) {
                                int sum=0,g=0;
                                for (int i = 0; i < allorder.size(); i++) {
                                    if (pname.equals(allorder.get(i).pid))
                                        sum = sum + allorder.get(i).quantity;
                                }
                                Log.e("U ALREADY PLACED=", String.valueOf(s));

                                EditText et1 = (EditText)findViewById(R.id.brought);
                                int qt = Integer.parseInt(et1.getText().toString());
                                if ((sum + qt) > Integer.parseInt(item.quantity)) {
                                    g= 1;
                                    Toast.makeText(offerzone.this, "You cannot add more items in Cart than in Stock!", Toast.LENGTH_SHORT).show();
                                    // showMessage("You cannot add more items in Cart than in Stock!");
                                }
                                if(g==0)
                                {    // Toast.makeText(offerzone.this, "You can click on Cart option to Checkout anytime!", Toast.LENGTH_SHORT).show();
                                    //showMessage("You can click on Cart option to Checkout anytime!");
                                    int cost = qt * Integer.parseInt(item.price);
                                    order neworder = new order(mob, pname, qt, cost);
                                    //MAKE SERVER REQUEST TO ADD TO CART
                                    Log.e("mobbb",mob);
                                    Log.e("pppname",pname);
                                    ServerRequests serverRequests = new ServerRequests(offerzone.this);
                                    serverRequests.storeorderInBackground(neworder, new GetOrderCallback() {
                                        @Override
                                        public void done(int f) {
                                            if (f == 1) {
                                                Toast.makeText(offerzone.this, "Order added to Cart successfully", Toast.LENGTH_SHORT).show();
                                                //showMessage("Order added to Cart successfully");
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
                Button b2=(Button)findViewById(R.id.addbtn);
                b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        int q=Integer.parseInt(et.getText().toString());
                        if(q<Integer.parseInt(item.quantity))
                        {
                            q++;
                            et.setText(Integer.toString(q));
                        }
                    }
                });
                Button b3=(Button)findViewById(R.id.subbtn);
                b3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int q=Integer.parseInt(et.getText().toString());
                        if(q>1)
                        {
                            q--;
                            et.setText(Integer.toString(q));
                        }
                    }
                });
                Button review=(Button)findViewById(R.id.reviewbtn);
                review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        Intent intent = new Intent(offerzone.this, reviewactivity.class);
                        intent.putExtra("mob", mob);
                        intent.putExtra("name", name);
                        intent.putExtra("pass", pass);
                        intent.putExtra("pid",item.pname);
                        startActivity(intent);
                    }
                });

            }}
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_offerzone, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
