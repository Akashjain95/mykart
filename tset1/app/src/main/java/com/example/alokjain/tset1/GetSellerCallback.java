package com.example.alokjain.tset1;

/**
 * Created by Alok Jain on 11/12/2015.
 */
        import java.util.List;

/**
 * Created by Alok Jain on 11/11/2015.
 */
interface GetSellerCallback {

    /**
     * Invoked when background task is completed
     */

    public abstract void done(List<Item> items);

}