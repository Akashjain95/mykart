package com.example.alokjain.tset1;

/**
 * Created by Alok Jain on 11/9/2015.
 */
interface GetUserCallback2{

    /**
     * Invoked when background task is completed
     */

    public abstract void done(Useraddress returnedaddress);

}