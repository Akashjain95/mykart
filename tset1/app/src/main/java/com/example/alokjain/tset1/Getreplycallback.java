package com.example.alokjain.tset1;

import java.util.List;

/**
 * Created by Alok Jain on 12/3/2015.
 */
interface Getreplycallback {

    /**
     * Invoked when background task is completed
     */

    public abstract void done(List<reply> allreply);

}