package com.example.alokjain.tset1;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class assistance extends Activity {
    List<reply> a=new ArrayList<reply>();
    String name,mob,pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assistance);
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        mob=getIntent().getStringExtra("mob");
        Button sendreply=(Button)findViewById(R.id.send_query);
        sendreply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // make a server request to add the complaint
                EditText p_id=(EditText)findViewById(R.id.p_id);
                EditText q_query=(EditText)findViewById(R.id.query);
                String query=q_query.getText().toString();
                String pid=p_id.getText().toString();
                if(p_id.length()==0|| p_id.length()>20)
                    Toast.makeText(assistance.this, "Please enter a valid product id!", Toast.LENGTH_SHORT).show();
                else if(query.length()==0||query.length()>100)
                {
                    Toast.makeText(assistance.this, "Please enter a valid query!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    ServerRequests s=new ServerRequests(assistance.this);
                    s.addquery(mob, pid, query, new GetOrderCallback() {
                        @Override
                        public void done(int f)
                        {     if(f==1)
                             Toast.makeText(assistance.this, "Your query has been registered successfully!", Toast.LENGTH_SHORT).show();
                        }
                    });


                }
            }
        });
        //make a server request to fetch all replies
  ServerRequests sr=new ServerRequests(assistance.this);
        sr.fetchreply(mob, new Getreplycallback() {
            @Override
            public void done(List<reply> allreply)
            {
            a=allreply;
            populateListview();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_assistance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void populateListview()
    {
        ArrayAdapter<reply> adapter=new MyListAdapter();
        ListView list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
    }
    private class  MyListAdapter extends  ArrayAdapter<reply>
    {
        public MyListAdapter()
        {
            super(assistance.this,R.layout.assistance_content,a);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemview=convertView;
            if(itemview==null)
            {
                itemview=getLayoutInflater().inflate(R.layout.assistance_content,parent,false);
            }
            final reply currentitem =a.get(position);
            // make a server  request to fetch reply by mobile no
            TextView date=(TextView)itemview.findViewById(R.id.timestamp);
            TextView rep=(TextView)itemview.findViewById(R.id.reply);
            date.setText(currentitem.date);
            rep.setText(currentitem.reply);
            return  itemview;
    }
    }
}
