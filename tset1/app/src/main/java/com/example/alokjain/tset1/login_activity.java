package com.example.alokjain.tset1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class login_activity extends Activity implements View.OnClickListener {
    UserLocalStore userLocalStore;
    EditText etName,etPassword;
    TextView forgot;
    Button bLogin,bSignup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_activity);
        /*final ImageView animImageView = (ImageView) findViewById(R.id.ivAnimation);
        animImageView.setBackgroundResource(R.drawable.anim);
        animImageView.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation =
                        (AnimationDrawable) animImageView.getBackground();
                frameAnimation.start();
            }
        });*/
        etName=(EditText)findViewById(R.id.etName);
        etPassword=(EditText)findViewById(R.id.etPassword);
        userLocalStore=new UserLocalStore(this);
        bLogin=(Button) findViewById(R.id.bLogin);
        bLogin.setOnClickListener(this);
        bSignup=(Button) findViewById(R.id.bSignup);
        bSignup.setOnClickListener(this);
        forgot = (TextView) this.findViewById(R.id.forgot);
        forgot.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.bLogin:
                final String username=etName.getText().toString();
                final String pass=etPassword.getText().toString();
                final String mob="";
                User user=new User(username,mob,pass);
                authenticate(user);
                userLocalStore.storeUserData(user);
                userLocalStore.setUserLoggedIn(true);
                break;
            case R.id.bSignup:
                startActivity(new Intent(this,MainActivity.class));
                break;
            case R.id.forgot:
                startActivity(new Intent(this,sendsms.class));
                break;
        }
    }
    private void authenticate(User user) {
        ServerRequests serverRequest = new ServerRequests(this);
        serverRequest.fetchUserDataAsyncTask(user, new GetUserCallback() {
            @Override
            public void done(User returnedUser) {
                Log.e("happened", "2");
                if (returnedUser == null) {
                    showErrorMessage(" incorrect user details");
                } else {
                    logUserIn(returnedUser);
                }
            }
        });
    }
    private void showErrorMessage(String s) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(login_activity.this);
        dialogBuilder.setMessage(s);
        //dialogBuilder.setMessage("Incorrect user details");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }

    private void logUserIn(User returnedUser) {
        userLocalStore.storeUserData(returnedUser);
        userLocalStore.setUserLoggedIn(true);
        Intent intent = new Intent(getApplicationContext(), flipview.class);
        intent.putExtra("mob",returnedUser.mob);
        intent.putExtra("name",returnedUser.name);
        intent.putExtra("pass",returnedUser.password);
        //showErrorMessage(returnedUser.mob);
        startActivity(intent);
    }
}

