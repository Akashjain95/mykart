package com.example.alokjain.tset1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class cartview extends Activity implements View.OnClickListener {
    List<order> a = new ArrayList<order>();
    String mobno,name,pass;
    int totalbill;
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartview);
        Button cod=(Button)findViewById(R.id.cod);
        cod.setOnClickListener(this);
        totalbill=0;
        mobno = getIntent().getStringExtra("mob");
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        getorders(mobno);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cartview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getorders(final String mobno) {
        DisplayServerRequest serverRequest = new DisplayServerRequest(this);
        serverRequest.fetchOrderDataAsyncTask(mobno, new GetOrderCallback2() {
            @Override
            public void done(List<order> allorder) {
                if (allorder == null || allorder.size() == 0)
                {
            /*GET APP  */  Toast.makeText(cartview.this, "No items in cart!Please go back to add to cart!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(cartview.this, shopage.class);//GET APP
                    intent.putExtra("mob",mobno);
                    intent.putExtra("name",name);
                    intent.putExtra("pass",pass);
                    startActivity(intent);
                    //--------------------->>>>>>usetext view to write no items in cart.....
                    //showMessage("No oders in your cart.Please go back to shop!");
                } else {
                    a = allorder;
                    for(int i=0;i<a.size();i++) {
                       totalbill+=a.get(i).cost;
                        Log.e("ono=", String.valueOf(a.get(i).orderno));
                        Log.e("qty=",String.valueOf(a.get(i).quantity));
                        Log.e("cost", String.valueOf(a.get(i).cost));

                    }
                    populateListview();
                }
            }
        });
    }

    public void populateListview() {
        ArrayAdapter<order> adapter = new MyListAdapter();
        ListView list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
    }

    @Override
    public void onClick(View v)
    {
    switch(v.getId())
    {
        case R.id.cod:
            Intent intent = new Intent(cartview.this, chooseaddress.class); //GET APPP
            intent.putExtra("mob",mobno);
            intent.putExtra("name",name);
            intent.putExtra("pass",pass);
            intent.putExtra("bill",String.valueOf(totalbill));
            startActivity(intent);
        break;
    }

    }

    private class MyListAdapter extends ArrayAdapter<order> {
        public MyListAdapter()
              {
            super(cartview.this, R.layout.activity_cartview_content, a);
               }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemview=convertView;
            if(itemview==null)
            {
                itemview=getLayoutInflater().inflate(R.layout.activity_cartview_content,parent,false);
            }
            final order currentorder =a.get(position);
            //order no
            TextView text1=(TextView) itemview.findViewById(R.id.orderno);
            text1.setText(String.valueOf(currentorder.orderno));
            //pid/pname
            TextView text2=(TextView) itemview.findViewById(R.id.pname);
            text2.setText(currentorder.pid);
            //cost
            TextView text3=(TextView) itemview.findViewById(R.id.cost);
            text3.setText(String.valueOf(currentorder.cost));
            //quantity
            TextView text4=(TextView)itemview.findViewById(R.id.quantity);
            text4.setText(String.valueOf(currentorder.quantity));
            //remove
            Button remove=(Button)itemview.findViewById(R.id.remove);
            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                ServerRequests serverRequest=new ServerRequests(cartview.this);
                serverRequest.deleteorderInBackground(currentorder.orderno, new GetOrderCallback() {
                    @Override
                    public void done(int f) {
                        Intent intent = getIntent();
                        finish();
                        intent.putExtra("mob",mobno);
                        intent.putExtra("name",name);
                        intent.putExtra("pass",pass);
                        startActivity(intent);
                        //recreate();
                    }
                });
                }
            });
            return  itemview;
        }
    }

}