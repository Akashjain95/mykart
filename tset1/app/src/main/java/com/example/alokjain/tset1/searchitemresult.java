package com.example.alokjain.tset1;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class searchitemresult extends ActionBarActivity {
String name,pass,mob,pid;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(" ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchitemresult);
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        mob=getIntent().getStringExtra("mob");
        pid=getIntent().getStringExtra("pid");
        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        final EditText et=(EditText)findViewById(R.id.brought);
        et.setFocusable(false); et.setClickable(false);
        DisplayServerRequest s=new DisplayServerRequest(this);
        s.fetchSellerDatabypid(pid, new GetSellerCallback2() {
            @Override
            public void done(final Item currentitem)
            {
                //IMAGE
                ImageView imageView=(ImageView)findViewById(R.id.img);
                imageView.setImageBitmap(currentitem.decodedbyte);
                //PRICE
                TextView text1=(TextView)findViewById(R.id.price);
                text1.setText(currentitem.price);
                //QUANTITY
                TextView text2=(TextView)findViewById(R.id.quantity);
                text2.setText(currentitem.quantity);
                //DESCRIPTION
                TextView text3=(TextView)findViewById(R.id.description);
                text3.setText(currentitem.description);
                //PNAME OR PID
                TextView text4=(TextView)findViewById(R.id.pname);
                text4.setText(currentitem.pname);
                //GET QUANTITY SELECTED BY CUSTOMER
                final EditText et=(EditText)findViewById(R.id.brought);

                Button b1=(Button)findViewById(R.id.angry_btn);
                b1.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        final String pname = currentitem.pname;
                        //FIRST CHECK CAN BE ADDED?? ADD TO CART REQUEST SATISIFIED ONLY IF REQUESTED+INCART<=STOCK

                        final DisplayServerRequest s = new DisplayServerRequest();
                        s.fetchOrderDataAsyncTask(mob, new GetOrderCallback2() {
                            @Override
                            public void done(List<order> allorder) {
                                int sum = 0, g = 0;
                                for (int i = 0; i < allorder.size(); i++) {
                                    if (pname.equals(allorder.get(i).pid))
                                        sum = sum + allorder.get(i).quantity;
                                }
                                Log.e("U ALREADY PLACED=", String.valueOf(s));
                                EditText et1 = (EditText)findViewById(R.id.brought);
                                int qt = Integer.parseInt(et1.getText().toString());
                                if ((sum + qt) > Integer.parseInt(currentitem.quantity)) {
                                    g = 1;
                                    Toast.makeText(searchitemresult.this, "You cannot add more items in Cart than in Stock!", Toast.LENGTH_SHORT).show();
                                    // showMessage("You cannot add more items in Cart than in Stock!");
                                }
                                if (g == 0) {    // Toast.makeText(Itemlistpage.this, "You can click on Cart option to Checkout anytime!", Toast.LENGTH_SHORT).show();
                                    //showMessage("You can click on Cart option to Checkout anytime!");
                                    int cost = qt * Integer.parseInt(currentitem.price);
                                    order neworder = new order(mob, pname, qt, cost);
                                    //MAKE SERVER REQUEST TO ADD TO CART
                                    ServerRequests serverRequests = new ServerRequests();
                                    serverRequests.storeorderInBackground(neworder, new GetOrderCallback() {
                                        @Override
                                        public void done(int f) {
                                            if (f == 1) {
                                                Toast.makeText(searchitemresult.this, "Order added to Cart successfully", Toast.LENGTH_SHORT).show();
                                                //showMessage("Order added to Cart successfully");
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
                Button b2=(Button)findViewById(R.id.addbtn);
                b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        int q=Integer.parseInt(et.getText().toString());
                        if(q<Integer.parseInt(currentitem.quantity))
                        {
                            q++;
                            et.setText(Integer.toString(q));
                        }
                    }
                });
                Button b3=(Button)findViewById(R.id.subbtn);
                b3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int q=Integer.parseInt(et.getText().toString());
                        if(q>1)
                        {
                            q--;
                            et.setText(Integer.toString(q));
                        }
                    }
                });
            }
        });
    }
    private void addDrawerItems() {
        String[] osArray = { " ","Home", "View Cart", "Add delivery address" };
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = "garbage";
                text = parent.getItemAtPosition(position).toString();
                if (text.equals("Home")) {
                    Intent intent = new Intent(searchitemresult.this, shopage.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    startActivity(intent);
                } else if (text.equals("View Cart")) {
                    Intent intent = new Intent(searchitemresult.this, cartview.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    startActivity(intent);
                } else if (text.equals("Add delivery address")) {
                    Intent intent = new Intent(searchitemresult.this, autocomplete.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    startActivity(intent);

                }
                mDrawerList.setItemChecked(position, true);
                setTitle(text);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Welcome" + " " + name + "!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_searchitemresult, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.imageView1:
                Intent intent = new Intent(searchitemresult.this, shopage.class);
                intent.putExtra("mob",mob);
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                startActivity(intent);
                return true;
            case R.id.imageView2:
                Intent intent2 = new Intent(searchitemresult.this, cartview.class);
                intent2.putExtra("mob",mob);
                intent2.putExtra("name",name);
                intent2.putExtra("pass",pass);
                startActivity(intent2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
    }
    }
}
