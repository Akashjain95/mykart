package com.example.alokjain.tset1;

import java.util.List;

/**
 * Created by Alok Jain on 11/29/2015.
 */
interface GetStatusCallback {

    /**
     * Invoked when background task is completed
     */

    public abstract void done(List<status> all);

}