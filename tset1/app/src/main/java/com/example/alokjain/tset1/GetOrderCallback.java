package com.example.alokjain.tset1;
interface GetOrderCallback {

    /**
     * Invoked when background task is completed
     */

    public abstract void done(int f);

}