package com.example.alokjain.tset1;

import java.util.List;

/**
 * Created by Alok Jain on 11/13/2015.
 */
interface GetAddressCallback {

    /**
     * Invoked when background task is completed
     */

    public abstract void done(List<Useraddress> address);

}