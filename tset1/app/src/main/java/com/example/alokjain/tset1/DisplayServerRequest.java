package com.example.alokjain.tset1;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DisplayServerRequest {
    ProgressDialog progressDialog;
    public static final int CONNECTION_TIMEOUT = 1000 * 15;
   // public static final String SERVER_ADDRESS = "http://192.168.43.208/";
    public static final String SERVER_ADDRESS = "http://php.akashjj619.s156.eatj.com/";
    public DisplayServerRequest(Context context) {
        progressDialog = new ProgressDialog((Context) context);
        progressDialog = new ProgressDialog((Context) context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
    }
    public  DisplayServerRequest()
    {

    }
    public void fetchOrderDataAsyncTask(String mobno,GetOrderCallback2 getOrderCallback)
    {
        progressDialog.show();
        Log.e("Akash", "Yahan tak toh pahuncha");
        new fetchOrderDataAsyncTask(mobno, getOrderCallback).execute();
    }
    public void fetchSellerDataAsyncTask(String category, GetSellerCallback userCallBack) {
        progressDialog.show();
        Log.e("Akash", "Yahan tak toh pahuncha");
        new fetchSellerDataAsyncTask(category, userCallBack).execute();
    }
  public void fetchSellerDatabypid(String pid,GetSellerCallback2 sellerCallback)
  {
      progressDialog.show();
      Log.e("jj",pid);
      Log.e("Akash", "Yahan tak toh pahuncha");
      new fetchSellerDatabypid(pid, sellerCallback).execute();
  }
    /**
     * parameter sent to task upon execution progress published during
     * background computation result of the background computation
     */
    public class fetchSellerDatabypid extends  AsyncTask<Void,Void,Item>
    {
        String pid;
        GetSellerCallback2 sellerCallBack;

        public fetchSellerDatabypid(String pid, GetSellerCallback2 sellerCallBack) {
            Log.e("ok", "ok");
            this.pid = pid;
            this.sellerCallBack = sellerCallBack;
        }

        @Override
        protected Item doInBackground(Void... params) {
            Log.e("ok", "ok");
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("pid", pid));
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchpid.php");

            //Item returnedItem[]=new Item[150];
            Item item=null;
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);
                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                Log.e("der", "b");
                Log.e("res=",result);
                JSONObject o=new JSONObject(result);
                if(o.length()!=0) {
                    String price = o.getString("price");
                    String qty = o.getString("qty");
                    String description = o.getString("description");
                    String image = o.getString("pic");
                    String pname = o.getString("pid");
                    String category = o.getString("category");
                    Log.e("found", pname);

                InputStream stream = new ByteArrayInputStream(Base64.decode(image.getBytes(), Base64.DEFAULT));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap bitmap = BitmapFactory.decodeStream(stream, null, options);
                 item=new Item(price, qty, bitmap, category, description,pname);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return item;
        }

        @Override
        protected void onPostExecute(Item item) {
            super.onPostExecute(item);
            progressDialog.dismiss();
            //Log.e("akash jiit",String.valueOf(.size()));
            sellerCallBack.done(item);
        }
    }

    public class fetchOrderDataAsyncTask extends AsyncTask<Void, Void, List<order>>
    {
        String mobno;
        GetOrderCallback2 getOrderCallback;
     public fetchOrderDataAsyncTask(String mobno,GetOrderCallback2 getOrderCallback)
     {
      this.mobno=mobno;
         this.getOrderCallback=getOrderCallback;
     }
        @Override
     protected  List<order> doInBackground(Void... params)
     {
         ArrayList<NameValuePair> dataToSend = new ArrayList<>();
         dataToSend.add(new BasicNameValuePair("mobno", mobno));
         HttpParams httpRequestParams = new BasicHttpParams();
         HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                 CONNECTION_TIMEOUT);
         HttpConnectionParams.setSoTimeout(httpRequestParams,
                 CONNECTION_TIMEOUT);

         HttpClient client = new DefaultHttpClient(httpRequestParams);
         HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchorder.php");
         //Item returnedItem[]=new Item[150];
         List<order> allorders = new ArrayList<order>();
         try {
             post.setEntity(new UrlEncodedFormEntity(dataToSend));
             HttpResponse httpResponse = client.execute(post);

             HttpEntity entity = httpResponse.getEntity();
             String result = EntityUtils.toString(entity);


             JSONArray a = new JSONArray(result);
             for (int i = 0; i < a.length(); i++) {
                 JSONObject o = (JSONObject) a.get(i);
                 int orderno = o.getInt("orderno");
                 String pid = o.getString("pid");
                 int quantity = o.getInt("quantity");
                 int cost =o.getInt("cost");
                 allorders.add(new order(mobno,pid,quantity,cost,orderno));
             }
         } catch (Exception e) {
             e.printStackTrace();
         }

         return allorders;
     }
        @Override
        protected void onPostExecute(List<order> allorder) {
            super.onPostExecute(allorder);
          progressDialog.dismiss();
            getOrderCallback.done(allorder);
        }
    }
        public class fetchSellerDataAsyncTask extends AsyncTask<Void, Void, List<Item>> {
        String category;
        GetSellerCallback userCallBack;

        public fetchSellerDataAsyncTask(String category, GetSellerCallback userCallBack) {
            Log.e("fetch aka cat", category);
            this.category = category;
            this.userCallBack = userCallBack;
        }

        @Override
        protected List<Item> doInBackground(Void... params) {
            Log.e("ok", "ok");
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("category", category));
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetch.php");

            //Item returnedItem[]=new Item[150];
            List<Item> allitems = new ArrayList<Item>();
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);


                JSONArray a = new JSONArray(result);
                Log.e("jiit",String.valueOf(a.length()));
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = (JSONObject) a.get(i);
                    String price = o.getString("price");
                    String qty = o.getString("qty");
                    String description = o.getString("description");
                    String image = o.getString("pic");
                    String pname=o.getString("pid");
                    InputStream stream = new ByteArrayInputStream(Base64.decode(image.getBytes(), Base64.DEFAULT));
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    Bitmap bitmap = BitmapFactory.decodeStream(stream, null, options);
                    allitems.add(new Item(price, qty, bitmap, category, description,pname));
                    Log.e("jiit",o.getString("pid"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return allitems;
        }

        @Override
        protected void onPostExecute(List<Item> all) {
            super.onPostExecute(all);
            progressDialog.dismiss();
            Log.e("akash jiit",String.valueOf(all.size()));
            userCallBack.done(all);
        }
    }
}
