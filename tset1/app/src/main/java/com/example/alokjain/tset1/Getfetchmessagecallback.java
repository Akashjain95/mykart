package com.example.alokjain.tset1;

import java.util.List;

/**
 * Created by Alok Jain on 12/1/2015.
 */
interface Getfetchmessagecallback {

    /**
     * Invoked when background task is completed
     */

    public abstract void done(List<messagedata> all);

}