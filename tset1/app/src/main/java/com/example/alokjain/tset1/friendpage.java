package com.example.alokjain.tset1;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class friendpage extends ActionBarActivity {
    String name,mob,pass;
    List<User> a=new ArrayList<>();
    List<status>arr=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Find your friends!");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friendpage);
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        mob=getIntent().getStringExtra("mob");
        ServerRequests s=new ServerRequests(friendpage.this);
        s.fetchallusers(mob, new GetAllUserCallback() {
            @Override
            public void done(final List<User> alluser) {
             if(alluser.size()==0)
                 Toast.makeText(friendpage.this, "Server error! Try later.", Toast.LENGTH_SHORT).show();
             else
             {
                 ServerRequests sr=new ServerRequests(friendpage.this);
                 sr.fetchstatus(mob, new GetStatusCallback() {
                     @Override
                     public void done(List<status> all) {
                         arr = all;
                         a = alluser;
                         populateListview();
                     }
                 });

             }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_friendpage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void populateListview()
    {

        ArrayAdapter<User> adapter=new MyListAdapter();
        ListView list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
    }
    private class  MyListAdapter extends  ArrayAdapter<User>
    {
        public MyListAdapter()
        {
            super(friendpage.this,R.layout.friendpagecontent,a);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemview = convertView;
            if (itemview == null) {
                itemview = getLayoutInflater().inflate(R.layout.friendpagecontent, parent, false);
            }
            final User currenuser=a.get(position);

            TextView mobtv=(TextView)itemview.findViewById(R.id.mobile);
            mobtv.setText(currenuser.mob);
            TextView name1=(TextView)itemview.findViewById(R.id.name);
            name1.setText(currenuser.name);

            final Button v1=(Button)itemview.findViewById(R.id.see);
            v1.setVisibility(View.GONE);
             final Button share=(Button)itemview.findViewById(R.id.viewpurchases);
             Log.e("sss", String.valueOf(arr.size()));
            for(int i=0;i<arr.size();i++)
            {
            if(arr.get(i).mob.equals(currenuser.mob))
            {   Log.e("jj", currenuser.mob);
                share.setText("Unshare Purchases");
                v1.setVisibility(View.VISIBLE);
                v1.setText("View Purchases");
            }
            }

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                 Button share=(Button)v;
                if(share.getText().toString().equals("Share Purchases!"))
                {
                    ServerRequests s=new ServerRequests(friendpage.this);
                    s.sharepurchases(mob, currenuser.mob, new GetOrderCallback() {
                        @Override
                        public void done(int f)
                        {
                        Log.e("donedd","akash");
                        }
                    });
                    share.setText("Unshare Purchases");
                    share.setVisibility(View.VISIBLE);
                    v1.setVisibility(View.VISIBLE);
                }
                    else
                {
                    ServerRequests s=new ServerRequests(friendpage.this);
                    s.unsharepurchases(mob, currenuser.mob, new GetOrderCallback() {
                        @Override
                        public void done(int f)
                        {
                            Log.e("donedd","akash");
                        }
                    });
                    share.setVisibility(View.VISIBLE);
                    share.setText("Share Purchases!");
                   v1.setVisibility(View.GONE);
                }
                }
            });

            v1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(friendpage.this, viewfriendsorder.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name",name);
                    intent.putExtra("pass", pass);
                    intent.putExtra("mob2",currenuser.mob);
                    startActivity(intent);
                }
            });
          Button send=(Button)itemview.findViewById(R.id.send);
          send.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v)
              {
                  Intent intent = new Intent(friendpage.this, viewfriendsmessage.class);
                  intent.putExtra("mob", mob);
                  intent.putExtra("name",name);
                  intent.putExtra("pass", pass);
                  intent.putExtra("mob2",currenuser.mob);
                  startActivity(intent);
              }
          });

            return  itemview;
        }
    }
}
