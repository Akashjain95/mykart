package com.example.alokjain.tset1;

import java.util.List;

interface GetOrderCallback2 {

    /**
     * Invoked when background task is completed
     */

    public abstract void done(List<order> allorder);

}