package com.example.alokjain.tset1;

/**
 * Created by Alok Jain on 11/22/2015.
 */

import java.util.List;

interface GetReviewCallback {
    public abstract void done(List<review> allreviews);

}