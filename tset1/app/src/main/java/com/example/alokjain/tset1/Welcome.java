package com.example.alokjain.tset1;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class Welcome extends Activity implements View.OnClickListener {
    Button r;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        ConnectionDetector cd=new ConnectionDetector(getApplicationContext());
        if(!cd.isConnectingToInternet())
        {
        TextView a=(TextView)findViewById(R.id.welcome);
        a.setText("No wifi connectivity");
        r=(Button)findViewById(R.id.brefresh);
        r.setOnClickListener(this);
        View c=findViewById(R.id.map);
        c.setVisibility(View.GONE);
        }
   else
        {
            TextView a=(TextView)findViewById(R.id.welcome);
            a.setText("Welcome");
            View b = findViewById(R.id.brefresh);
            b.setVisibility(View.GONE);
            View c=findViewById(R.id.map);
            c.setVisibility(View.VISIBLE);
            r=(Button)findViewById(R.id.map);
            r.setOnClickListener(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v)
    {
    switch(v.getId())
    {
        case R.id.brefresh:
            recreateActivityCompat(this);
            break;
        case R.id.map:
            String name=getIntent().getStringExtra("name");
            String pass=getIntent().getStringExtra("pass");
            String mob=getIntent().getStringExtra("mob");
            //showMessage(getIntent().getStringExtra("mob"));
            Intent intent = new Intent(getApplicationContext(), autocomplete.class);
            intent.putExtra("name",name);
            intent.putExtra("pass",pass);
            intent.putExtra("mob",mob);
            startActivity(intent);
            //startActivity(new Intent(this,autocomplete.class));
            break;
            // Refresh main activity upon close of dialog box
           /* Intent refresh = new Intent(this, Welcome.class);
            startActivity(refresh);
            this.finish(); */
    }
    }
    @SuppressLint("NewApi")
    public static final void recreateActivityCompat(final Activity a) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            a.recreate();
        } else {
            final Intent intent = a.getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            a.finish();
            a.overridePendingTransition(0, 0);
            a.startActivity(intent);
            a.overridePendingTransition(0, 0);
        }
    }
    private void showMessage(String s) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Welcome.this);
        dialogBuilder.setMessage(s);
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }
}


