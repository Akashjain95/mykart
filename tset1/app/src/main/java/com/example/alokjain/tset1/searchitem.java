package com.example.alokjain.tset1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class searchitem extends Activity {

    // List view
    private ListView lv;
    String name,mob,pass,category;
    // Listview Adapter
    ArrayAdapter<String> adapter;

    // Search EditText
    EditText inputSearch;
    List<Item>a=new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchitem);
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        mob=getIntent().getStringExtra("mob");
        DisplayServerRequest serverRequest = new DisplayServerRequest(this);
        Log.e("akash", "Server request  made!");
        category=new String("all");
        serverRequest.fetchSellerDataAsyncTask(category, new GetSellerCallback() {
            @Override
            public void done(List<Item> all) {
                Log.e("pp", "called");
                if (all == null || all.size() == 0) {
                    Toast.makeText(searchitem.this, "No Items in stock currently!", Toast.LENGTH_SHORT).show();
                    //showMessage(" No Items in stock currently!");
                    Log.e("akash", "nothing!");
                } else {
                    a = all;
                    String products[] = new String[a.size()];
                    Log.e("eee", String.valueOf(a.size()));
                    //products[0]=new String("garbage");
                    for (int i = 0; i < a.size(); i++) {
                        products[i] = new String(a.get(i).pname);
                        Log.e("akash jain", products[i]);
                    }
                    if (products.length == 0)
                        Toast.makeText(searchitem.this, "No Items in stock currently!", Toast.LENGTH_SHORT).show();
                    lv = (ListView) findViewById(R.id.list_view);
                    inputSearch = (EditText) findViewById(R.id.inputSearch);

                    // Adding items to listview
                    adapter = new ArrayAdapter<String>(searchitem.this, R.layout.activity_list_item, R.id.product_name, products);
                    lv.setAdapter(adapter);
                    inputSearch.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                            // When user changed the Text
                            searchitem.this.adapter.getFilter().filter(cs);
                        }

                        @Override
                        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                      int arg3) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void afterTextChanged(Editable arg0) {
                            // TODO Auto-generated method stub
                        }
                    });
                }
                ConnectionDetector cd = new ConnectionDetector(searchitem.this);

                    lv.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String s = a.get(position).pname;
                            Intent intent = new Intent(searchitem.this, searchitemresult.class);
                            intent.putExtra("mob", mob);
                            intent.putExtra("name", name);
                            intent.putExtra("pass", pass);
                            Log.e("selected=", s);
                            intent.putExtra("pid", s);
                            startActivity(intent);
                        }
                    });

            }
        });

    }
}
