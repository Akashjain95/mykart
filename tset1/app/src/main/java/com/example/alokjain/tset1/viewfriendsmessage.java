package com.example.alokjain.tset1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class viewfriendsmessage extends ActionBarActivity {
   List<messagedata> a= new ArrayList<messagedata>();
    String name,pass,mob,mob2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewfriendsmessage);
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        mob = getIntent().getStringExtra("mob");
        mob2 = getIntent().getStringExtra("mob2");
        Button sendit=(Button)findViewById(R.id.sendit);

            sendit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText text=(EditText)findViewById(R.id.textm);
                    String textstring=text.getText().toString();
                    if(textstring.length()==0)
                        Toast.makeText(viewfriendsmessage.this, "Invalid Message.", Toast.LENGTH_SHORT).show();
                    else  if(textstring.length()>250)
                        Toast.makeText(viewfriendsmessage.this, "Message can be maximum 250 characters.", Toast.LENGTH_SHORT).show();
                    else{
                        final message m=new message(textstring,mob,mob2);
                    ServerRequests s = new ServerRequests(viewfriendsmessage.this);
                    s.addmessage(m, new GetOrderCallback() {
                        @Override
                        public void done(int f)
                        {   EditText text=(EditText)findViewById(R.id.textm);
                            text.setText(" ");
                            Toast.makeText(viewfriendsmessage.this, "Message sent succesfully!", Toast.LENGTH_SHORT).show();
                            recreate();
                        }
                    });
                }}
            });
// fetch all messages where mob2==$mob2 and mob1=$mob1 or opposite....
ServerRequests sr=new ServerRequests(viewfriendsmessage.this);
        sr.fetchmessage(mob, mob2, new Getfetchmessagecallback() {
            @Override
            public void done(List<messagedata> all) {
                a=all;
                Log.e("ress=",String.valueOf(all.size()));
                populateListview();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_viewfriendsmessage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void populateListview() {
        ArrayAdapter<messagedata> adapter = new MyListAdapter();
        ListView list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
    }

    private class MyListAdapter extends ArrayAdapter<messagedata> {
        public MyListAdapter() {
            super(viewfriendsmessage.this, R.layout.activity_viewfriendsmessage, a);
        }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemview = convertView;
        if (itemview == null) {
            itemview = getLayoutInflater().inflate(R.layout.friendsmessagecontent, parent, false);
        }
        final messagedata currentitem =a.get(position);
        TextView user_name=(TextView)itemview.findViewById(R.id.name);
        user_name.setText(currentitem.sender);
        TextView user_mobile=(TextView)itemview.findViewById(R.id.user_mobile);
        user_mobile.setText(currentitem.s_mob);
        TextView user_message=(TextView)itemview.findViewById(R.id.message);
        user_message.setText(currentitem.text);
        TextView user_time=(TextView)itemview.findViewById(R.id.timestamp);
        user_time.setText(currentitem.time);
        Button deletem=(Button)itemview.findViewById(R.id.deletem);
        deletem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
            ServerRequests sr=new ServerRequests(viewfriendsmessage.this);
                Log.e("sender",currentitem.s_mob);
                Log.e("reciever",currentitem.r_mob);
                Log.e("deleter",mob);
            sr.deletemessage(currentitem.s_mob, currentitem.r_mob, currentitem.time,mob, new GetOrderCallback() {
                @Override
                public void done(int f) {
                 recreate();
                    Toast.makeText(viewfriendsmessage.this, "Message deleted successfully.", Toast.LENGTH_SHORT).show();
                }
            });

            }
        });
        return itemview;
    }
}

}