package com.example.alokjain.tset1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class reviewactivity extends Activity {
String name,pass,mob,pid;
    private ArrayAdapter<String> mAdapter;
    List<review> a=new ArrayList<review>();
    public void populateListview()
    {
        ArrayAdapter<review> adapter=new MyListAdapter();
        ListView list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviewactivity);
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        mob=getIntent().getStringExtra("mob");
        pid=getIntent().getStringExtra("pid");
        Log.e("reviewact", pid);
        ServerRequests s=new ServerRequests(reviewactivity.this);
        s.fetchallreviews(pid, new GetReviewCallback() {
            @Override
            public void done(List<review> allreviews) {
                a = allreviews;
                Log.e("review", String.valueOf(allreviews.size()));
                populateListview();
            }
        });
        //FETCH ALL REVIEWS ACC TO PID





        Button b1=(Button)findViewById(R.id.reviewbtn);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {    TextView review=(TextView)findViewById(R.id.etreview);
                final String reviewdata=review.getText().toString();
            if(reviewdata.length()==0)
            {
                Toast.makeText(reviewactivity.this, "Review cannot be empty!", Toast.LENGTH_SHORT).show();
            }
            else if(reviewdata.length()>200)
            {
                Toast.makeText(reviewactivity.this, "Review cannot be more than 200 words!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                ServerRequests s=new ServerRequests();
                //String name,String reviewdata,String mob,String pid
                review obj=new review(name,reviewdata,mob,pid);
              progressDialog.show();
                long delayInMillis = 2000;
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                }, delayInMillis);
                s.addreviewInBackground(obj, new GetReviewCallback2() {
                    @Override
                    public void done(int f) {
                        //progressDialog.dismiss();
                        if (f == 1)
                            Toast.makeText(reviewactivity.this, "Review added successfully!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(reviewactivity.this, shopage.class);
                        intent.putExtra("mob", mob);
                        intent.putExtra("name", name);
                        intent.putExtra("pass", pass);
                        startActivity(intent);
                    }
                });
            }
            }

        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reviewactivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class  MyListAdapter extends  ArrayAdapter<review>
    {
        public MyListAdapter()
        {
            super(reviewactivity.this,R.layout.activity_item_view,a);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemview=convertView;
            if(itemview==null)
            {
                itemview=getLayoutInflater().inflate(R.layout.reviewactivityitems,parent,false);
            }
            final review currentreview=a.get(position);
            TextView name=(TextView)itemview.findViewById(R.id.name);
            name.setText("By"+" "+currentreview.name);
            ImageView certified=(ImageView)itemview.findViewById(R.id.certified);
            if(Integer.parseInt(currentreview.iscertifed)==0)
            {
             certified.setVisibility(View.GONE);
            }
            TextView reviewdat=(TextView)itemview.findViewById(R.id.reviewtv);
            reviewdat.setText(currentreview.reviewdata);
            return itemview;
    }

}
}
