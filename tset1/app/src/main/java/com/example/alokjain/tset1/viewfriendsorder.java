package com.example.alokjain.tset1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;


public class viewfriendsorder extends ActionBarActivity {
String name,mob,mob2,pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(" ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewfriendsorder);
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        mob=getIntent().getStringExtra("mob");
        mob2=getIntent().getStringExtra("mob2");
        Log.e("mob2", mob2);
        ServerRequests s=new ServerRequests(viewfriendsorder.this);
        s.fetchexecutedorderbymob(mob2, new Getexceutedordercallback() {
            @Override
            public void done(List<String> all)
            {
                Log.e("aaaa",String.valueOf(all.size()));
                final int N = all.size(); // total number of textviews to add
                Log.e("iska len",String.valueOf(N));
                if(all.size()!=0) {

                     TextView rowTextView = new TextView(viewfriendsorder.this);
                    rowTextView.setText("Items purchased:");
                    rowTextView.setInputType(rowTextView.getInputType()| InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                    LinearLayout myLinearLayout = (LinearLayout) findViewById(R.id.myLinearLayout);
                    // add the textview to the linearlayout
                    myLinearLayout.addView(rowTextView);
                    for (int i = 0; i < N; i++) {
                        // create a new textview
                         rowTextView = new TextView(viewfriendsorder.this);
                        rowTextView.setText(all.get(i));

                         myLinearLayout = (LinearLayout) findViewById(R.id.myLinearLayout);
                        // add the textview to the linearlayout
                        myLinearLayout.addView(rowTextView);


                        // save a reference to the textview for later

                    }
                }
                else
                {

                    final TextView rowTextView = new TextView(viewfriendsorder.this);

                    // set some properties of rowTextView or something
                    rowTextView.setText("No items purchased.");
                    LinearLayout myLinearLayout = (LinearLayout) findViewById(R.id.myLinearLayout);
                    // add the textview to the linearlayout
                    myLinearLayout.addView(rowTextView);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_viewfriendsorder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
