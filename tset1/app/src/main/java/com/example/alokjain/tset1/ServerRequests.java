package com.example.alokjain.tset1;

/**
 * Created by Alok Jain on 11/4/2015.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class ServerRequests {
    ProgressDialog progressDialog;
    public static final int CONNECTION_TIMEOUT = 1000 * 15;
  // public static final String SERVER_ADDRESS = "http://192.168.43.208/";
public static final String SERVER_ADDRESS = "http://php.akashjj619.s156.eatj.com/";
    public ServerRequests(Context context) {
        progressDialog = new ProgressDialog((Context) context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
    }
    public void fetchreply(String mob,Getreplycallback getreplycallback)
    {
        progressDialog.show();
        new fetchreply(mob,getreplycallback).execute();
    }
    public void addquery(String mob,String pid,String complaint,GetOrderCallback getOrderCallback)
    {
     progressDialog.show();
     new addquery(mob,pid,complaint,getOrderCallback).execute();
    }
    public void deletemessage(String mob1,String mob2,String timestamp,String delmob,GetOrderCallback getOrderCallback)
    {
        progressDialog.show();
        new deletemessage(mob1,mob2,timestamp,delmob,getOrderCallback).execute();
    }
     public void fetchemail(String email,GetUserCallback getUserCallback)
     {
         progressDialog.show();
         new fetchemail(email,getUserCallback).execute();
     }
    public void fetchmessage(String mob1,String mob2,Getfetchmessagecallback getfetchmessagecallback)
    {
        progressDialog.show();
        new fetchmessage(mob1,mob2,getfetchmessagecallback).execute();
    }
    public void addmessage(message m,GetOrderCallback getOrderCallback)
    {
        progressDialog.show();
        new addmessage(m,getOrderCallback).execute();
    }
   public void fetchrandom(GetSellerCallback2 getSellerCallback)
   {
       progressDialog.show();
       new fetchrandom(getSellerCallback).execute();
   }
   public void sharepurchases(String mob1,String mob2,GetOrderCallback getOrderCallback)
   {
       progressDialog.show();
       new sharepurchases(mob1,mob2,getOrderCallback).execute();
   }
    public void unsharepurchases(String mob1,String mob2,GetOrderCallback getOrderCallback)
    {
        progressDialog.show();
        new unsharepurchases(mob1,mob2,getOrderCallback).execute();
    }
    public void fetchstatus(String mob,GetStatusCallback getStatusCallback)
    {
        progressDialog.show();
        new fetchstatus(mob,getStatusCallback).execute();
    }
    public void fetchemailbymob()
    {

    }
    public void fetchexecutedorderbymob(String mob,Getexceutedordercallback getexceutedordercallback)
    {
        progressDialog.show();
        new fetchexecutedorderbymob(mob,getexceutedordercallback).execute();
    }
    public ServerRequests() {
    }
     public void storeorderInBackground(order neworder,GetOrderCallback getOrderCallback)
     {
         progressDialog.show();
         new storeorderInBackgroundAsynctask(neworder,getOrderCallback).execute();
     }
    public void applycode(Useraddress add,String code,GetOrderCallback orderCallback)
    {
        progressDialog.show();
    new applycode(code,add,orderCallback).execute();
    }
    public void fetchallusers(String mob,GetAllUserCallback getAllUserCallback)
    {
     progressDialog.show();
     new fetchallusers(mob,getAllUserCallback).execute();
    }
    public void fetchallreviews(String pid,GetReviewCallback getReviewCallback)
    {
       progressDialog.show();
        new fetchallreviews(pid,getReviewCallback).execute();
    }
    public void addreviewInBackground(review r,GetReviewCallback2 getReviewCallback)
    {
     new addreviewInBackground(r,getReviewCallback).execute();
    }
    public void deleteorderInBackground(int orderno,GetOrderCallback getOrderCallback)
    {
        progressDialog.show();
        new delteorderAsynctask(orderno, getOrderCallback).execute();

    }

    public void executeorderInBackground(Useraddress info,GetOrderCallback orderCallback)
    {
        progressDialog.show();
       // ;
        Log.e("akash","constructor");
        new executeorderAsynctask(info,orderCallback).execute();
    }
    public void storeUserDataInBackground(User user,String email, GetUserCallback userCallBack) {
        progressDialog.show();
        new StoreUserDataAsyncTask(user,email, userCallBack).execute();
    }
    public void fetchUserAddressAsyncTask(String mobno,GetAddressCallback addressCallback)
    {
        progressDialog.show();
        Log.e("Akash", "Yahan tak toh pahuncha");
        new fetchUserAddressAsyncTask(mobno, addressCallback).execute();
    }
    public void fetchSellerDataAsyncTaskordered(String category,int order,GetSellerCallback SellerCallback)
    {
        progressDialog.show();
        Log.e("Akash", "Yahan tak toh pahuncha");
        new fetchSellerDataAsyncTaskordered(category,order,SellerCallback).execute();

    }

    public void fetchUserDataAsyncTask(User user, GetUserCallback userCallBack) {
        progressDialog.show();
        Log.e("Akash", "Yahan tak toh pahuncha");
        new fetchUserDataAsyncTask(user, userCallBack).execute();
    }

    public void fetchUserDataAsyncTaskbynumber(User user, GetUserCallback userCallBack) {
        progressDialog.show();
        Log.e("Akash","Yahan tak toh pahuncha2");
        new fetchUserDataAsyncTaskbynumber(user, userCallBack).execute();
    }
    public void StoreUserAddressInBackground(Useraddress user,GetUserCallback2 userCallBack)
    {
        progressDialog.show();
        Log.e("Akash","Yahan tak toh pahuncha2");
        new StoreUserAddressAsyncTask(user, userCallBack).execute();
    }
    public class fetchreply extends  AsyncTask<Void,Void,List<reply>>
    {
    String mob;
    Getreplycallback getreplycallback;
    public fetchreply(String mob,Getreplycallback getreplycallback)
    {
    this.mob=mob;
        this.getreplycallback=getreplycallback;
    }
        @Override
        protected List<reply> doInBackground(Void... params)
        {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob", mob));

            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchreply.php");
            //Item returnedItem[]=new Item[150];
            List<reply> all = new ArrayList<reply>();
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);
                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                Log.e("answer",result);
                JSONArray a = new JSONArray(result);
                //Log.e("sst",result);
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = (JSONObject) a.get(i);
                    String mob = o.getString("mob");
                    String rep = o.getString("rep");
                    String date=o.getString("date_reply");
                    reply r=new reply(date,rep,mob);
                    all.add(r);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return all;
        }
        @Override
        protected void onPostExecute(List<reply> all) {
            super.onPostExecute(all);
            progressDialog.dismiss();
            getreplycallback.done(all);
        }
    }

    public class addquery extends AsyncTask<Void ,Void,Void>
    {
    String mob,pid,complaint;
    GetOrderCallback getOrderCallback;
    public addquery(String mob,String pid,String complaint,GetOrderCallback getOrderCallback)
    {
    this.mob=mob;
    this.pid=pid;
        this.complaint=complaint;
      this.getOrderCallback=getOrderCallback;
    }
        @Override
        protected Void doInBackground(Void... params)
        {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob",mob ));
            dataToSend.add(new BasicNameValuePair("pid", pid));
            dataToSend.add(new BasicNameValuePair("query", complaint));
            HttpParams httpRequestParams = getHttpRequestParams();

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Addcomplaint.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }
        @Override
        protected void onPostExecute(Void a) {
            super.onPostExecute(a);
            progressDialog.dismiss();
            getOrderCallback.done(1);
        }

    }

    public class fetchemail extends  AsyncTask<Void,Void,User>
    {
    String email;
        GetUserCallback getUserCallback;
    public fetchemail(String email,GetUserCallback getUserCallback)
    {
    this.email=email;
        this.getUserCallback=getUserCallback;
    }
        @Override
        protected User doInBackground(Void... params)
        {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("email", email));
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchemail.php");
              User user=null;
            String name,mob,pass;
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);
                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                Log.e("answer",result);
               JSONObject o=new JSONObject(result);
                 name=o.getString("name");
                mob=o.getString("mob");
                pass=o.getString("password");
                user=new User(name,mob,pass);
                }
             catch (Exception e) {
                e.printStackTrace();
            }

            return user;
        }
        @Override
        protected void onPostExecute(User a) {
            super.onPostExecute(a);
            progressDialog.dismiss();
            getUserCallback.done(a);
        }
    }
    public class deletemessage extends  AsyncTask<Void,Void,Void>
    {
    String mob1,mob2,timestamp,delmob;
    GetOrderCallback getOrderCallback;
    public deletemessage(String mob1,String mob2,String timestamp,String delmob,GetOrderCallback getOrderCallback)
    {
        this.mob1=mob1;
        this.mob2=mob2;
        this.delmob=delmob;
        Log.e("SENDER",mob1);
        Log.e("Rec",mob2);
        Log.e("DELE",delmob);
        this.timestamp=timestamp;
        this.getOrderCallback=getOrderCallback;
    }
        @Override
        protected Void doInBackground(Void... params)
        {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob1",mob1));
            dataToSend.add(new BasicNameValuePair("mob2", mob2));
            dataToSend.add(new BasicNameValuePair("detail",timestamp));
            dataToSend.add(new BasicNameValuePair("delmob",delmob));
            HttpParams httpRequestParams = getHttpRequestParams();

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Deletemessage.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }
        @Override
        protected void onPostExecute(Void a) {
            super.onPostExecute(a);
            progressDialog.dismiss();
            getOrderCallback.done(1);
        }
    }

    public class addmessage extends  AsyncTask<Void,Void,Void>
    {
    GetOrderCallback getOrderCallback;
    message m;
    public addmessage(message m,GetOrderCallback getOrderCallback)
    {
    this.getOrderCallback=getOrderCallback;
        this.m=m;
    }
        @Override
        protected Void doInBackground(Void... params)
        {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob1",m.mob1 ));
            dataToSend.add(new BasicNameValuePair("mob2", m.mob2));
            dataToSend.add(new BasicNameValuePair("text", m.text));
            HttpParams httpRequestParams = getHttpRequestParams();

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Addmessage.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }
        @Override
        protected void onPostExecute(Void a) {
            super.onPostExecute(a);
            progressDialog.dismiss();
            getOrderCallback.done(1);
        }
    }
    public class fetchmessage extends  AsyncTask<Void,Void,List<messagedata>>
    {
    String mob1,mob2;
    Getfetchmessagecallback getfetchmessagecallback;
        public fetchmessage(String mob1,String mob2,Getfetchmessagecallback getfetchmessagecallback)
        {
        this.mob1=mob1;
            this.mob2=mob2;
            this.getfetchmessagecallback=getfetchmessagecallback;
        }
        @Override
        protected List<messagedata> doInBackground(Void... params)
        {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob1", mob1));
            dataToSend.add(new BasicNameValuePair("mob2", mob2));

            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchmessage.php");
            //Item returnedItem[]=new Item[150];
            List<messagedata> all = new ArrayList<messagedata>();
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);
                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                Log.e("answer",result);
                JSONArray a = new JSONArray(result);
                //Log.e("sst",result);
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = (JSONObject) a.get(i);
                    String mob1 = o.getString("mob1");
                    String mob2 = o.getString("mob2");
                    String name1=o.getString("name1");
                    String name2=o.getString("name2");
                    String message=o.getString("message");
                    String detail=o.getString("detail");
                    messagedata m=new messagedata(name1,name2,mob1,mob2,message,detail);
                    all.add(m);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return all;
        }
        @Override
        protected void onPostExecute(List<messagedata> all) {
            super.onPostExecute(all);
            progressDialog.dismiss();
            getfetchmessagecallback.done(all);
        }

    }


   public class sharepurchases extends  AsyncTask<Void,Void,Void>
   {
   String mob1,mob2;
       GetOrderCallback getOrderCallback;
    public sharepurchases(String mob1,String mob2,GetOrderCallback getOrderCallback)
    {
    this.mob1=mob1;
        this.mob2=mob2;
        this.getOrderCallback=getOrderCallback;
    }
       @Override
       protected Void doInBackground(Void... params)
       {
           ArrayList<NameValuePair> dataToSend = new ArrayList<>();
           dataToSend.add(new BasicNameValuePair("mob1",mob1 ));
           dataToSend.add(new BasicNameValuePair("mob2", mob2));
           HttpParams httpRequestParams = getHttpRequestParams();

           HttpClient client = new DefaultHttpClient(httpRequestParams);
           HttpPost post = new HttpPost(SERVER_ADDRESS + "Addfriend.php");

           try {
               post.setEntity(new UrlEncodedFormEntity(dataToSend));
               client.execute(post);
           } catch (Exception e) {
               e.printStackTrace();
           }

           return null;
       }
       private HttpParams getHttpRequestParams() {
           HttpParams httpRequestParams = new BasicHttpParams();
           HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                   CONNECTION_TIMEOUT);
           HttpConnectionParams.setSoTimeout(httpRequestParams,
                   CONNECTION_TIMEOUT);
           return httpRequestParams;
       }
       @Override
       protected void onPostExecute(Void a) {
           super.onPostExecute(a);
           progressDialog.dismiss();
           getOrderCallback.done(1);
       }
   }

    ///
    public class unsharepurchases extends  AsyncTask<Void,Void,Void>
    {
        String mob1,mob2;
        GetOrderCallback getOrderCallback;
        public unsharepurchases(String mob1,String mob2,GetOrderCallback getOrderCallback)
        {
            this.mob1=mob1;
            this.mob2=mob2;
            this.getOrderCallback=getOrderCallback;
        }
        @Override
        protected Void doInBackground(Void... params)
        {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob1",mob1 ));
            dataToSend.add(new BasicNameValuePair("mob2", mob2));
            HttpParams httpRequestParams = getHttpRequestParams();

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Removefriend.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }
        @Override
        protected void onPostExecute(Void a) {
            super.onPostExecute(a);
            progressDialog.dismiss();
            getOrderCallback.done(1);
        }
    }
public class fetchexecutedorderbymob extends  AsyncTask<Void,Void,List<String>>
{
 String mob;
    Getexceutedordercallback getexceutedordercallback;
 public fetchexecutedorderbymob(String mob,Getexceutedordercallback getexceutedordercallback)
 {
  this.mob=mob;
  this.getexceutedordercallback=getexceutedordercallback;
 }
    @Override
    protected List<String> doInBackground(Void... params)
    {
        ArrayList<NameValuePair> dataToSend = new ArrayList<>();
        dataToSend.add(new BasicNameValuePair("mob", mob));
        Log.e("in serverrequest",mob);
        HttpParams httpRequestParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpRequestParams,
                CONNECTION_TIMEOUT);

        HttpClient client = new DefaultHttpClient(httpRequestParams);
        HttpPost post = new HttpPost(SERVER_ADDRESS + "fetchexecuted.php");
        //Item returnedItem[]=new Item[150];
        List<String> all = new ArrayList<String>();
        try {
            post.setEntity(new UrlEncodedFormEntity(dataToSend));
            HttpResponse httpResponse = client.execute(post);
            HttpEntity entity = httpResponse.getEntity();
            String result = EntityUtils.toString(entity);
            Log.e("answer",result);
            JSONArray a = new JSONArray(result);
            Log.e("sst",result);
            for (int i = 0; i < a.length(); i++) {
                JSONObject o = (JSONObject) a.get(i);
                String pid = o.getString("pid");
                all.add(pid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return all;
    }
    @Override
    protected void onPostExecute(List<String> all) {
        super.onPostExecute(all);
        progressDialog.dismiss();
        getexceutedordercallback.done(all);
    }
}




    public class fetchstatus extends  AsyncTask<Void,Void,List<status>>
    {
    GetStatusCallback getStatusCallback;
    String mob;
    public fetchstatus(String mob,GetStatusCallback getStatusCallback)
    {
     this.getStatusCallback=getStatusCallback;this.mob=mob;
    }
        @Override
        protected List<status> doInBackground(Void... params)
        {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob", mob));
            Log.e("sentmob",mob);
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchstatus.php");

            //Item returnedItem[]=new Item[150];
            List<status> all = new ArrayList<status>();
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                if (result == null) {
                    Log.e("all user","null result");
                    return null;
                }
                Log.e("here",result);
                JSONArray a = new JSONArray(result);
                Log.e("sstfff",result);
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = (JSONObject) a.get(i);
                    String mob = o.getString("mob2");
                    int status=o.getInt("status");
                    //fetch mobileno and status
                    all.add(new status(mob,status));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return all;
        }
        @Override
        protected void onPostExecute(List<status> all) {
            super.onPostExecute(all);
            progressDialog.dismiss();
            getStatusCallback.done(all);
        }
    }


    public class fetchallusers extends  AsyncTask<Void,Void,List<User>>
    {
    GetAllUserCallback getAllUserCallback;
        String mob;
    public fetchallusers(String mob,GetAllUserCallback getAllUserCallback)
    {
      this.getAllUserCallback=getAllUserCallback;
        this.mob=mob;
    }
        @Override
        protected List<User> doInBackground(Void... params)
        {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob", mob));
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchallusers.php");

            //Item returnedItem[]=new Item[150];
            List<User> allusers = new ArrayList<User>();
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                if (result == null) {
                    Log.e("all user","null result");
                    return null;
                }
                JSONArray a = new JSONArray(result);
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = (JSONObject) a.get(i);
                    String name = o.getString("name");
                    String mob = o.getString("mob");
                    //String name,String reviewdata,String mob,String pid
                    allusers.add(new User(name,mob));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return allusers;
        }
        @Override
        protected void onPostExecute(List<User> all) {
            super.onPostExecute(all);
            progressDialog.dismiss();
            getAllUserCallback.done(all);
        }
    }
    /**
     * parameter sent to task upon execution progress published during
     * background computation result of the background computation
     */
    public class fetchrandom extends  AsyncTask<Void,Void,Item>
    {
    GetSellerCallback2 getSellerCallback;
    public fetchrandom(GetSellerCallback2 getSellerCallback)
    {
    this.getSellerCallback=getSellerCallback;
    }
        @Override
        protected Item doInBackground(Void... params) {
            Log.e("ok", "ok");
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            String category=new String("Offer zone");
            dataToSend.add(new BasicNameValuePair("category", category));
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchrandom.php");

            //Item returnedItem[]=new Item[150];
          Item item=new Item();
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                Log.e("fffl",result);
                if (result == null)
                    return null;

                JSONArray a = new JSONArray(result);
                Log.e("lllen",String.valueOf(a.length()));
                if(a.length()==0)
                    return null;
                Random r = new Random();
                Log.e("th","yo");
                int randomObjectIndex = r.nextInt(a.length()-0) + 0;
                JSONObject o=new JSONObject();
               o=a.getJSONObject(randomObjectIndex);
                String price = o.getString("price");
                String qty = o.getString("qty");
                String description = o.getString("description");
                String image = o.getString("pic");
                String pname=o.getString("pid");
                InputStream stream = new ByteArrayInputStream(Base64.decode(image.getBytes(), Base64.DEFAULT));
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap bitmap = BitmapFactory.decodeStream(stream, null, options);
               item=new Item(price, qty, bitmap, category, description,pname);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return item;
        }

        @Override
        protected void onPostExecute(Item all) {
            super.onPostExecute(all);
            progressDialog.dismiss();
            getSellerCallback.done(all);
        }
    }

    //TO MOVE FROM ORDERINFO TO EXECUTED ORDER INFO nd executed order address THOSE ITEMS WHICH R AVAILABIBLE IN STOCK  i.e (r visble on cart)
   public class applycode extends AsyncTask<Void,Void,Integer>
    {
     String code;
     Useraddress info;
     GetOrderCallback orderCallback;
     public  applycode(String code,Useraddress address,GetOrderCallback orderCallback)
     {
         this.code=code;
         this.info=address;
         this.orderCallback=orderCallback;
     }
        @Override
        protected Integer doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mobno", info.mob));
            dataToSend.add(new BasicNameValuePair("location", info.location));
            dataToSend.add(new BasicNameValuePair("sector", info.sector));
            dataToSend.add(new BasicNameValuePair("hno",info.hno));
            dataToSend.add(new BasicNameValuePair("code",code));
            Log.e("code",code);
            HttpParams httpRequestParams = getHttpRequestParams();
            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "applycoupon.php");
             Integer r =new Integer(0);
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);
                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                Log.e("resultaka",result);
                JSONObject o=new JSONObject(result);
                //if(Integer.parseInt(response)==1)
                 String disc=o.getString("disc");
                 r=new Integer(Integer.valueOf(disc));
                Log.e("akaka",disc);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.e("executerequest","done!!");
            return r;
        }
        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }
        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            Log.e("akash","onpost");
            int r=result.intValue();
            Log.e("akaks",String.valueOf(r));
             progressDialog.dismiss();
            orderCallback.done(r);
        }
    }

    public class addreviewInBackground extends AsyncTask<Void,Void,Void>
    {
    review r;
    GetReviewCallback2 getReviewCallback;
    public addreviewInBackground(review r,GetReviewCallback2 getReviewCallback)
    {this.r=r;this.getReviewCallback=getReviewCallback;}
        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob", r.mob));
            dataToSend.add(new BasicNameValuePair("name", r.name));
            dataToSend.add(new BasicNameValuePair("pid", r.pid));
            dataToSend.add(new BasicNameValuePair("reviews", r.reviewdata));


            HttpParams httpRequestParams = getHttpRequestParams();

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Addreviews.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //progressDialog.dismiss();
            getReviewCallback.done(1);
        }
    }

    //FETCH ALL REVIEWS
    public class  fetchallreviews extends  AsyncTask<Void,Void,List<review>>
    {
    String pid;
    GetReviewCallback getReviewCallback;
    public fetchallreviews(String pid,GetReviewCallback getReviewCallback)
    {
     this.getReviewCallback=getReviewCallback;this.pid=pid;
    }
        @Override
        protected List<review> doInBackground(Void... params) {
            Log.e("ok", "ok");
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("pid", pid));
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchreviews.php");

            //Item returnedItem[]=new Item[150];
            List<review> allreviews = new ArrayList<review>();
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                if (result == null) {
                 Log.e("reviewserver","null result");
                    return null;
                }
                JSONArray a = new JSONArray(result);
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = (JSONObject) a.get(i);
                    String name = o.getString("name");
                    String mob = o.getString("mob");
                    String reviews = o.getString("reviews");
                    String pid = o.getString("pid");
                    String certified=o.getString("iscertified");
                    //String name,String reviewdata,String mob,String pid
                    allreviews.add(new review(name, reviews,mob,pid,certified));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return allreviews;
        }

        @Override
        protected void onPostExecute(List<review> all) {
            super.onPostExecute(all);
            progressDialog.dismiss();
            getReviewCallback.done(all);
        }
    }
    public class fetchSellerDataAsyncTaskordered extends  AsyncTask<Void,Void,List<Item>>
    {
    String category;
    int order;
    GetSellerCallback SellerCallback;
    public  fetchSellerDataAsyncTaskordered(String category,int order,GetSellerCallback SellerCallback)
    {
     this.category=category;
     this.order=order;
    this.SellerCallback=SellerCallback;
    }
        @Override
        protected List<Item> doInBackground(Void... params) {
            Log.e("ok", "ok");
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("category", category));
            dataToSend.add(new BasicNameValuePair("order", String.valueOf(order)));
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchinorder.php");

            //Item returnedItem[]=new Item[150];
            List<Item> allitems = new ArrayList<Item>();
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                if (result == null)
                    return null;

                JSONArray a = new JSONArray(result);
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = (JSONObject) a.get(i);
                    String price = o.getString("price");
                    String qty = o.getString("qty");
                    String description = o.getString("description");
                    String image = o.getString("pic");
                    String pname=o.getString("pid");
                    InputStream stream = new ByteArrayInputStream(Base64.decode(image.getBytes(), Base64.DEFAULT));
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    Bitmap bitmap = BitmapFactory.decodeStream(stream, null, options);
                    allitems.add(new Item(price, qty, bitmap, category, description,pname));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return allitems;
        }

        @Override
        protected void onPostExecute(List<Item> all) {
            super.onPostExecute(all);
            progressDialog.dismiss();
            SellerCallback.done(all);
        }
    }
    public class executeorderAsynctask extends  AsyncTask<Void,Void,Void>
    {
    Useraddress info;
    GetOrderCallback orderCallback;

        public executeorderAsynctask(Useraddress info,GetOrderCallback orderCallback)
        {
         this.info=info;
            this.orderCallback=orderCallback;
        }
        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mobno", info.mob));
            dataToSend.add(new BasicNameValuePair("location", info.location));
            dataToSend.add(new BasicNameValuePair("sector", info.sector));
            dataToSend.add(new BasicNameValuePair("hno",info.hno));

            HttpParams httpRequestParams = getHttpRequestParams();
            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Executedorder.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("executerequest","done!!");
            return null;
        }
        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Log.e("akash","onpost");
            progressDialog.dismiss();
            orderCallback.done(1);
        }


    }

    //TO DELTE ORDER BY ORDER NO.
    public class  delteorderAsynctask extends AsyncTask<Void,Void,Void>
    {
    int orderno;
    GetOrderCallback orderCallback;
    public delteorderAsynctask(int orderno,GetOrderCallback orderCallback)
    {
        this.orderCallback=orderCallback;
        this.orderno=orderno;

    }

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("orderno", String.valueOf(orderno)));
            HttpParams httpRequestParams = getHttpRequestParams();
           Log.e("ORDER NO=",String.valueOf(orderno));
            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Deleteorder.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("Delterequest","done!!");
            return null;
        }
        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            orderCallback.done(1);
        }

    }
//TO STORE ORDER OF CUSTOMER..
 public class storeorderInBackgroundAsynctask extends AsyncTask<Void,Void,Void>
    {
    order neworder;
    GetOrderCallback getOrderCallback;
    public storeorderInBackgroundAsynctask(order neworder,GetOrderCallback getOrderCallback)
    {
    this.neworder=neworder;
    this.getOrderCallback=getOrderCallback;
    }
        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mobno", neworder.mobno));
            dataToSend.add(new BasicNameValuePair("pid", neworder.pid));
            dataToSend.add(new BasicNameValuePair("cost", String.valueOf(neworder.cost)));
            dataToSend.add(new BasicNameValuePair("quantity", String.valueOf(neworder.quantity)));

            HttpParams httpRequestParams = getHttpRequestParams();

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Addorder.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            getOrderCallback.done(1);
        }

    }


    public class StoreUserDataAsyncTask extends AsyncTask<Void, Void, Void> {
        User user;
        String email;
        GetUserCallback userCallBack;

        public StoreUserDataAsyncTask(User user,String email, GetUserCallback userCallBack) {
            this.user = user;
            this.email=email;
            this.userCallBack = userCallBack;
        }

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("name", user.name));
            dataToSend.add(new BasicNameValuePair("mob", user.mob));
            dataToSend.add(new BasicNameValuePair("password", user.password));
            dataToSend.add(new BasicNameValuePair("email", email));
            HttpParams httpRequestParams = getHttpRequestParams();

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Register.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            userCallBack.done(null);
        }

    }

    //To store user address
    public class StoreUserAddressAsyncTask extends  AsyncTask<Void,Void,Void>
    {
        Useraddress user;
        GetUserCallback2 userCallBack;
        public StoreUserAddressAsyncTask(Useraddress user,GetUserCallback2 userCallBack)
        {
            this.user = user;
            this.userCallBack = userCallBack;
        }


        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("location", user.location));
            dataToSend.add(new BasicNameValuePair("sector", user.sector));
            dataToSend.add(new BasicNameValuePair("hno", user.hno));
            dataToSend.add(new BasicNameValuePair("mob", user.mob));
            HttpParams httpRequestParams = getHttpRequestParams();

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "address.php");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                client.execute(post);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
        private HttpParams getHttpRequestParams() {
            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            return httpRequestParams;
        }
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            userCallBack.done(null);
        }
    }

    // TO FETCH USER ADDRESS
    public class fetchUserAddressAsyncTask extends AsyncTask<Void,Void,List<Useraddress>>
    {
    String mobno;
    GetAddressCallback addressCallback;
    public fetchUserAddressAsyncTask(String mobno,GetAddressCallback addressCallback)
    {
    this.mobno=mobno;
    this.addressCallback=addressCallback;
    }
        @Override
        protected List<Useraddress> doInBackground(Void... params) {
            Log.v("ok", "ok");
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mobno", mobno));
            // dataToSend.add(new BasicNameValuePair("password", user.password));

            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "Fetchaddress.php");

            List<Useraddress> alladd = new ArrayList<>();
            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);
                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                if (result == null)
                    return null;

                JSONArray a = new JSONArray(result);
                for (int i = 0; i < a.length(); i++) {
                    JSONObject o = (JSONObject) a.get(i);
                    String location = o.getString("location");
                    String sector = o.getString("sector");
                    String hno = o.getString("hno");
                    alladd.add(new Useraddress(location,sector,hno,mobno));
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            return alladd;
        }
        protected void onPostExecute(List<Useraddress> all) {
            super.onPostExecute(all);
            progressDialog.dismiss();
            addressCallback.done(all);
        }

    }
    public class fetchUserDataAsyncTask extends AsyncTask<Void, Void, User> {
        User user;
        GetUserCallback userCallBack;

        public fetchUserDataAsyncTask(User user, GetUserCallback userCallBack) {
            Log.v("ok","ok");
            this.user = user;
            this.userCallBack = userCallBack;
        }

        @Override
        protected User doInBackground(Void... params) {
            Log.v("ok","ok");
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("name", user.name));
            dataToSend.add(new BasicNameValuePair("password", user.password));

            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "FetchUserData.php");

            User returnedUser =null;

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                JSONObject jObject = new JSONObject(result);

                if (jObject.length() != 0){
                    Log.v("happened", "882");
                    String name = jObject.getString("name");
                    String password = jObject.getString("password");
                    String mob=jObject.getString("mob");
                    returnedUser = new User(name,mob, password);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return returnedUser;
        }

        @Override
        protected void onPostExecute(User returnedUser) {
            super.onPostExecute(returnedUser);
            progressDialog.dismiss();
            userCallBack.done(returnedUser);
        }
    }
    public class fetchUserDataAsyncTaskbynumber extends AsyncTask<Void, Void, User> {
        User user;
        GetUserCallback userCallBack;

        public fetchUserDataAsyncTaskbynumber(User user, GetUserCallback userCallBack) {
            Log.v("ok2","ok2");
            this.user = user;
            this.userCallBack = userCallBack;
        }

        @Override
        protected User doInBackground(Void... params) {
            Log.v("ok3", "ok3");
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("mob", user.mob));


            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams,
                    CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "FetchUserData2.php");

            User returnedUser =null;

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                JSONObject jObject = new JSONObject(result);

                if (jObject.length() != 0){
                    Log.v("ok", "happened in serverrequest");
                    String name = jObject.getString("name");
                    String password = jObject.getString("password");

                    returnedUser = new User(name, user.mob, password);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return returnedUser;
        }

        @Override
        protected void onPostExecute(User returnedUser) {
            super.onPostExecute(returnedUser);
            progressDialog.dismiss();
            userCallBack.done(returnedUser);
        }
    }

}