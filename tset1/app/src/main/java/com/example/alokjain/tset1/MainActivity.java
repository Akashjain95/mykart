package com.example.alokjain.tset1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{
    EditText name,password,mob,email;
    Button bSignup;
    Button bLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       /* final ImageView animImageView = (ImageView) findViewById(R.id.ivAnimation);
        animImageView.setBackgroundResource(R.drawable.anim);
        animImageView.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation =
                        (AnimationDrawable) animImageView.getBackground();
                frameAnimation.start();
            }
        });*/
        name = (EditText) findViewById(R.id.etName);
        password = (EditText) findViewById(R.id.etPassword);
        mob = (EditText) findViewById(R.id.etMobile);
        bSignup=(Button)findViewById(R.id.bSignup);
        email=(EditText)findViewById(R.id.etEmail);
        bSignup.setOnClickListener(this);
        bLogin=(Button)findViewById(R.id.bLogin);
        bLogin.setOnClickListener(this);
        Log.e("Akash", "main ka on create ");
    }


            @Override
            public void onClick(View v) {
                switch(v.getId())
                {
                    case R.id.bSignup:
                        int f=1;
                final String emailid=email.getText().toString();
                 if(!isEmailValid(emailid))
                 {
                 email.setError("Invalid email id");
                 f=0;
                 }
                final String Name = name.getText().toString();
                if (!isValidName(Name)) {
                    name.setError("Invalid Name 1-10 characters allowed!");
                  f=0;
                }

                final String pass = password.getText().toString();
                if (!isValidPassword(pass)) {
                    password.setError("Invalid Password");
                f=0;
                }
                final String Mob = mob.getText().toString();
                if (!isValidmob(Mob)) {
                    mob.setError("Invalid Mobile number!");
                f=0;
                }
                if(f==1)
                { Log.e("Akash", "f=1 ");
                    String n=name.getText().toString();
                    String p=password.getText().toString();
                    String m=mob.getText().toString();
                    User user=new User(n,m,p);
                    registerUser(user);

                    // SMS PART UNCOMMENT LATER!
                    String sms="Welcome"+" "+ n + " " + "thanx for shopping with Mykart!";
                   /* try {
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(m, null, sms, null, null);
                        Toast.makeText(getApplicationContext(), "Registered!",
                                Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(),
                                "SMS faild, please try again later!",
                                Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }*/


                    Log.e("Akash", "after reg user ");
                }
              break;
                    case R.id.bLogin:
                        startActivity(new Intent(this,login_activity.class));
                    break;
                }

            }


    private void registerUser(User user) {
        ServerRequests serverRequest = new ServerRequests(this);
        String emailid=email.getText().toString();
        Log.e("reggg",emailid);
        serverRequest.storeUserDataInBackground(user,emailid, new GetUserCallback() {
            @Override
            public void done(User returnedUser) {
                startActivity(new Intent(MainActivity.this,login_activity.class));
            }
        });
    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    private boolean isValidName(String name) {
       if(name.length()==0||name.length()>10)
        return false;
        return true;
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 7&& pass.length()<=15) {
            return true;
        }
        return false;
    }
    private boolean isValidmob(String Mob) {
        if (Mob.length() >=7 &&Mob.length()<=13) {
            return true;
        }
        return false;
    }

}

