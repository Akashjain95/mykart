package com.example.alokjain.tset1;

import android.graphics.Bitmap;

/**
 * Created by Alok Jain on 11/11/2015.
 */
public class Item
{
String price,quantity;
Bitmap decodedbyte;
String category;
String description;
String pname;
    public Item()
    {

    }
    public Item(String p, String q, Bitmap d, String category, String description)
{
price=p;
quantity=q;
decodedbyte=d;
    this.category=category;
    this.description=description;

}  public Item(String p, String q, Bitmap d, String category, String description,String pname)
{
    price=p;
    quantity=q;
    decodedbyte=d;
    this.category=category;
    this.description=description;
    this.pname=pname;

}
    public Item(String p, String q, String category)
    {
        price=p;
        quantity=q;
        this.category=category;

    }
}
