package com.example.alokjain.tset1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;





import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.app.Activity;

import android.os.Bundle;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import java.util.Timer;
import java.util.TimerTask;
import android.os.Bundle;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.os.Handler;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
public class shopage extends ActionBarActivity implements View.OnClickListener {
    private ShakeDetector mShakeDetector;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    public int currentimageindex = 0;
    Button c1,c2,c3,c4,c5;
    ImageView v1,v2,v3;
    Timer timer;
    TimerTask task;
    ImageView slidingimage;
    String name,pass,mob;
    private int[] IMAGE_IDS = {
            R.drawable.p7,
            R.drawable.p1, R.drawable.p2, R.drawable.p3,
            R.drawable.p4
    };
// MAKE WIDTH OF ALL IMAGES EQUAL!!
@Override
public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_shopage, menu);
    ActionBar actionBar=getSupportActionBar();
   actionBar.setLogo(R.drawable.logo1);
    return super.onCreateOptionsMenu(menu);
}
    private void showMessage(String s) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(Html.fromHtml("<font color='#FF7F27'>Shake the device for latest offers!!</font>"));
        dialogBuilder.setMessage(s);
        LayoutInflater factory = LayoutInflater.from(shopage.this);
        final View view = factory.inflate(R.layout.sample, null);
        dialogBuilder.setView(view);
        dialogBuilder.setPositiveButton("Continue", null);
        dialogBuilder.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle(" ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrollable_contents);

        // ShakeDetector initialization
        showMessage(" ");
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector(new ShakeDetector.OnShakeListener() {
            @Override
            public void onShake() {
              //send to new act
                Vibrator v = (Vibrator) shopage.this.getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                v.vibrate(500);
                Intent intent = new Intent(shopage.this, offerzone.class);
                intent.putExtra("mob", mob);
                intent.putExtra("name", name);
                intent.putExtra("pass", pass);
                startActivity(intent);

                //Toast.makeText(shopage.this, "shake!", Toast.LENGTH_SHORT).show();
            }
        });
        mDrawerList = (ListView)findViewById(R.id.navList);mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        ScrollView scrollable_contents = (ScrollView) findViewById(R.id.scrollableContents);
        getLayoutInflater().inflate(R.layout.activity_contents, scrollable_contents);
        final Handler mHandler = new Handler();
        final ImageView animImageView = (ImageView) findViewById(R.id.cat1);
        animImageView.setBackgroundResource(R.drawable.anim1);
        animImageView.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation =
                        (AnimationDrawable) animImageView.getBackground();
                frameAnimation.start();
            }
        });
        final ImageView animImageView2 = (ImageView) findViewById(R.id.cat2);
        animImageView2.setBackgroundResource(R.drawable.anim2);
        animImageView2.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation1 =
                        (AnimationDrawable) animImageView2.getBackground();
                frameAnimation1.start();
            }
        });
        final ImageView animImageView3 = (ImageView) findViewById(R.id.cat3);
        animImageView3.setBackgroundResource(R.drawable.anim3);
        animImageView3.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation2 =
                        (AnimationDrawable) animImageView3.getBackground();
                frameAnimation2.start();
            }
        });

        final ImageView animImageView4 = (ImageView) findViewById(R.id.cat4);
        animImageView4.setBackgroundResource(R.drawable.anim4);
        animImageView4.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation3 =
                        (AnimationDrawable) animImageView4.getBackground();
                frameAnimation3.start();
            }
        });
        final ImageView animImageView5 = (ImageView) findViewById(R.id.cat5);
        animImageView5.setBackgroundResource(R.drawable.anim5);
        animImageView5.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation4 =
                        (AnimationDrawable) animImageView5.getBackground();
                frameAnimation4.start();
            }
        });

         name=getIntent().getStringExtra("name");
         pass=getIntent().getStringExtra("pass");
         mob=getIntent().getStringExtra("mob");

        c1=(Button)findViewById(R.id.c1);
        c1.setOnClickListener(this);
        c2=(Button)findViewById(R.id.c2);
        c2.setOnClickListener(this);
        c3=(Button)findViewById(R.id.c3);
        c3.setOnClickListener(this);
        c4=(Button)findViewById(R.id.c4);
        c4.setOnClickListener(this);
        c5=(Button)findViewById(R.id.c5);
        c5.setOnClickListener(this);
        Button friend=(Button)findViewById(R.id.friends);
        friend.setOnClickListener(this);
        final Runnable mUpdateResults = new Runnable() {
            public void run() {

                AnimateandSlideShow();
            }
        };

        int delay = 1000; // delay for 1 sec.

        int period = 8000; // repeat every 4 sec.

        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {

                mHandler.post(mUpdateResults);

            }

        }, delay, period);

    }
    private void addDrawerItems() {
        String[] osArray = { " ","Home", "View Cart", "Add delivery address" };
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = "garbage";
                text = parent.getItemAtPosition(position).toString();
                if (text.equals("Home")) {
                    Toast.makeText(shopage.this, "Please feel free to browse all categories!", Toast.LENGTH_SHORT).show();
                } else if (text.equals("View Cart")) {
                    Intent intent = new Intent(shopage.this, cartview.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    startActivity(intent);
                } else if (text.equals("Add delivery address")) {
                    Intent intent = new Intent(shopage.this, autocomplete.class);
                    intent.putExtra("mob", mob);
                    intent.putExtra("name", name);
                    intent.putExtra("pass", pass);
                    startActivity(intent);

                }
                mDrawerList.setItemChecked(position, true);
                setTitle(text);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Welcome" + " " + name + "!");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.imageView1:
                Toast.makeText(shopage.this, "Please feel free to browse all categories!", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.imageView2:
                Intent intent = new Intent(shopage.this, cartview.class);
                intent.putExtra("mob",mob);
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                startActivity(intent);
                return true;
            case R.id.search:
                Intent intent2 = new Intent(shopage.this, searchitem.class);
                intent2.putExtra("mob",mob);
                intent2.putExtra("name",name);
                intent2.putExtra("pass",pass);
                startActivity(intent2);
             return true;
            case R.id.email:
                Toast.makeText(shopage.this, "Please feel free to give us feedback!", Toast.LENGTH_SHORT).show();
                sendEmail();
            return true;
            case R.id.assistant:
                Toast.makeText(shopage.this, "Please feel free to address any enquiry or complaint.", Toast.LENGTH_SHORT).show();
                Intent intent3 = new Intent(shopage.this, assistance.class);
                intent3.putExtra("mob",mob);
                intent3.putExtra("name",name);
                intent3.putExtra("pass",pass);
                startActivity(intent3);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    protected void sendEmail() {
        Log.i("Send email", "");
        String[] TO = {"seller@mykart.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback/Reviews");
        emailIntent.putExtra(Intent.EXTRA_TEXT, " ");

        try {
            startActivity(Intent.createChooser(emailIntent, "Write to us..."));
            //finish();
            Log.e("Finished", "");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(shopage.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.e("akash", "LOW memory!");
        //slidingimage=null;
    }
    int backButtonCount=0;
    @Override
    public void onBackPressed()
    {
        if(backButtonCount >= 1)
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }
    public void onClick(View v)
    {
      String category;
        Intent intent;
        switch ((v.getId()))
        {
            case R.id.c2:
               intent = new Intent(shopage.this, Itemlistpage.class);
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                intent.putExtra("mob",mob);
                Log.e("MOBILE NO:",mob);
                 category=new String("electronics");
                intent.putExtra("category",category);
                startActivity(intent);
             break;
            case R.id.c1:
                intent = new Intent(shopage.this, Itemlistpage.class);
                intent.putExtra("name",name);
                intent.putExtra("pass", pass);
                intent.putExtra("mob",mob);
                 category=new String("clothes");
                intent.putExtra("category",category);
                startActivity(intent);
                break;
            case R.id.c3:
                intent = new Intent(shopage.this, Itemlistpage.class);
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                intent.putExtra("mob",mob);
                 category=new String("jewellery");
                intent.putExtra("category",category);
                startActivity(intent);
                break;
            case R.id.c4:
                 intent = new Intent(shopage.this, Itemlistpage.class);
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                intent.putExtra("mob",mob);
                 category=new String("toys");
                intent.putExtra("category",category);
                startActivity(intent);
                break;
            case R.id.c5:
                 intent = new Intent(shopage.this, Itemlistpage.class);
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                intent.putExtra("mob",mob);
                 category=new String("others");
                intent.putExtra("category",category);
                startActivity(intent);
                break;
            case R.id.friends:
                intent = new Intent(shopage.this, friendpage.class);
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                intent.putExtra("mob",mob);
                startActivity(intent);
             break;
        }
    }

    /**
     * Helper method to start the animation on the splash screen
     */
    private void AnimateandSlideShow() {
        slidingimage = (ImageView)findViewById(R.id.ImageView3_Left);
        slidingimage.setImageResource(IMAGE_IDS[currentimageindex%IMAGE_IDS.length]);
        currentimageindex++;
        Animation rotateimage = AnimationUtils.loadAnimation(this, R.anim.custom_anim);
        slidingimage.startAnimation(rotateimage);

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }
}