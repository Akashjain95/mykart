package com.example.alokjain.tset1;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.telephony.SmsManager;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class sendsms extends Activity implements View.OnClickListener{
     Button retrieve,back,mail;
    EditText mobile,etemailid;
    User user;
    String email;
   String mobno="";
    private static final String username = "akash.95born2win@gmail.com";
    private static final String password = "facebook619";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendsms);
        retrieve=(Button) findViewById(R.id.bretrieve);
        retrieve.setOnClickListener(this);
        back=(Button)findViewById(R.id.bgetback);
        back.setOnClickListener(this);
        mobile=(EditText)findViewById(R.id.mobileNumber);
        etemailid=(EditText)findViewById(R.id.email_id);
        email=etemailid.getText().toString();
        mail=(Button)findViewById(R.id.btn_email_id);
        mail.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sendsms, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bretrieve:
                 mobno=mobile.getText().toString();
                if(mobno ==" "|| mobno.length()<10)
                {showErrorMessage();
                Log.e("l1","something is wrong.");
                }
                else
                {
                String name="";
                    String pass="";
                user=new User(name,mobno,pass);
                Log.e("aka","create new user");
                    Log.e("aka", mobno);
                authenticate(user);
                }
                break;
            case R.id.bgetback:
                startActivity(new Intent(this,login_activity.class));
                break;
            case R.id.btn_email_id:
                email=etemailid.getText().toString();
               ServerRequests s=new ServerRequests(sendsms.this);
               s.fetchemail(email, new GetUserCallback() {
                   @Override
                   public void done(User returnedUser) {
                    if(returnedUser==null)
                        Toast.makeText(sendsms.this, "Email id  not registered", Toast.LENGTH_SHORT).show();
                       else
                    {
                    Log.e("found!", returnedUser.name);
                    String message="Your Mykart username is:" + " " +returnedUser.name+ " "+"and password is:" + " " + returnedUser.password +" "+"Happy shopping! " ;
                    String subject="Password Recovery";
                        Log.e("final email",email);
                        sendMail(email,subject,message);
                    }
                   }
               });
                /*String email="akash.95there4u@gmail.com";
                String subject="Password Recovery";
                String message="";*/
            //sendMail(email, subject, message);
             break;
        }

    }
    private void authenticate(User user) {
        ServerRequests serverRequest = new ServerRequests(this);
        serverRequest.fetchUserDataAsyncTaskbynumber(user, new GetUserCallback() {
            @Override
            public void done(User returnedUser) {
                Log.e("happened", "2");
                if (returnedUser == null) {
                    showErrorMessage();
                } else {
                    sendsms(returnedUser);
                    showsuccessmessage();
                }
            }
        });
    }
    private void showErrorMessage() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(sendsms.this);
        dialogBuilder.setMessage("Incorrect user details");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }
    private void  showsuccessmessage()
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(sendsms.this);
        dialogBuilder.setMessage("Login details sent to your mobile!Please login to continue.");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }
    private  void sendsms(User returnedUser)
    {
        String sms="Your Mykart username is:" + " " +returnedUser.name+ " "+"and password is:" + " " + returnedUser.password +"Happy shopping! " ;
                    try {
                        String m=mobno;
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(m, null, sms, null, null);
                        Toast.makeText(getApplicationContext(), "Message sent!",
                                Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(),
                                "SMS faild, please try again later!",
                                Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }

    }
    private void sendMail(String email, String subject, String messageBody) {
        Session session = createSessionObject();

        try {
            Message message = createMessage(email, subject, messageBody, session);
            new SendMailTask().execute(message);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    private Message createMessage(String email, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("akash.95born2win@gmail.com", "Mykart"));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }

    private Session createSessionObject() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        return Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
    }

    private class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(sendsms.this, "Please wait", "Sending mail", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(sendsms.this, "Login details sent to your Email Id!", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();

        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
