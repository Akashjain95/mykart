package com.example.alokjain.tset1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class chooseaddress extends Activity implements View.OnClickListener {
    String mobno,name,pass;
    int totalbill;
    RadioGroup ButtonLayout;

    List<Useraddress> a=new ArrayList<Useraddress>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooseaddress);
        ServerRequests s=new ServerRequests(this);
        mobno = getIntent().getStringExtra("mob");
        name=getIntent().getStringExtra("name");
        pass=getIntent().getStringExtra("pass");
        totalbill=Integer.parseInt(getIntent().getStringExtra("bill"));
      Button addanother=(Button)findViewById(R.id.addanother);
        addanother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(chooseaddress.this, autocomplete.class);
                intent.putExtra("mob", mobno);
                intent.putExtra("name",name);
                intent.putExtra("pass",pass);
                startActivity(intent);
            }
        });
        s.fetchUserAddressAsyncTask(mobno, new GetAddressCallback() {
            @Override
            public void done(List<Useraddress> address) {
                if (address == null || address.size() == 0) {
                    Toast.makeText(chooseaddress.this, "No Delivery address added.Please add a delivery address!", Toast.LENGTH_SHORT).show();
                    //showMessage("No Delivery address added.Please add a delivery address!");
                    Intent intent = new Intent(chooseaddress.this, autocomplete.class); //GET APP
                    intent.putExtra("mob", mobno);
                    intent.putExtra("name",name);
                    intent.putExtra("pass",pass);
                    startActivity(intent);
                } else
                {
                    //showMessage("Please choose a delivery address.");
                    Toast.makeText(chooseaddress.this, "Please choose a delivery address", Toast.LENGTH_SHORT).show();
                    a = address;
                    ButtonLayout = (RadioGroup) findViewById(R.id.address_radio_group);
                    for (int i = 0; i < a.size(); i++) {
                        RadioButton button = new RadioButton(chooseaddress.this);
                        button.setId(i);
                        if (i == 0) {
                            boolean mBool = true;
                            button.setChecked(mBool);
                        }
                        button.setText(a.get(i).hno+" "+a.get(i).sector+" "+a.get(i).location);
                        ButtonLayout.addView(button);
                    }
                    Button coupon=(Button)findViewById(R.id.couponbtn);
                    coupon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                        EditText e=(EditText)findViewById(R.id.couponcode);
                        String couponcode=e.getText().toString();
                            if(couponcode.length()==0)
                                Toast.makeText(chooseaddress.this, "Invalid coupon code", Toast.LENGTH_SHORT).show();
                            else
                            {
                            int selected=ButtonLayout.getCheckedRadioButtonId();
                            String location=a.get(selected).location;
                            String sector=a.get(selected).sector;
                            String hno=a.get(selected).hno;
                            //showMessage(location);
                            ServerRequests s=new ServerRequests(chooseaddress.this);
                            Useraddress info=new Useraddress(location,sector,hno,mobno);
                            s.applycode( info,couponcode, new GetOrderCallback() {
                                @Override
                                public void done(int f)
                                {
                                if(f==0) Toast.makeText(chooseaddress.this, "Invalid coupon code", Toast.LENGTH_SHORT).show();
                                else
                                {  totalbill=totalbill-(totalbill*f)/100;
                                    Toast.makeText(chooseaddress.this, "Order placed successfully.Total Billing amount is Rs"+" "+totalbill , Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(chooseaddress.this, shopage.class);//GET APP
                                    intent.putExtra("mob",mobno);
                                    intent.putExtra("name",name);
                                    intent.putExtra("pass",pass);
                                    startActivity(intent);
                                }}
                            });
                            }
                        }
                    });
                    Button placeorder=(Button)findViewById(R.id.placeorder);
                    placeorder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                        Log.e("chooseadd", "clicked!");
                        int selected=ButtonLayout.getCheckedRadioButtonId();
                        String location=a.get(selected).location;
                        String sector=a.get(selected).sector;
                        String hno=a.get(selected).hno;
                        //showMessage(location);
                            ServerRequests s=new ServerRequests(chooseaddress.this);
                        Useraddress info=new Useraddress(location,sector,hno,mobno);
                        //showMessage(mobno);
                            s.executeorderInBackground(info, new GetOrderCallback() {
                            @Override
                            public void done(int f) {
                                Toast.makeText(chooseaddress.this, "Order placed successfully!Total billing amount is Rs"+" "+totalbill, Toast.LENGTH_SHORT).show();//GET APP
                            //showMessage("Order placed succesfully!");
                                Intent intent = new Intent(chooseaddress.this, shopage.class);//GET APP
                                intent.putExtra("mob",mobno);
                                intent.putExtra("name",name);
                                intent.putExtra("pass",pass);
                                //intent.putExtra("name",name);
                                //intent.putExtra("pass",pass);
                                startActivity(intent);
                            }
                        });
                        }
                    });
                    ButtonLayout.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            RadioButton radiobutton = (RadioButton) group.findViewById(checkedId);
                            Toast.makeText(chooseaddress.this, radiobutton.getText(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }


            }
        });

    }
    private void showMessage(String s) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(Html.fromHtml("<font color='#FF7F27'>Shake the device for latest offers!!</font>"));
        dialogBuilder.setMessage(s);
        dialogBuilder.setPositiveButton("Continue", null);
        dialogBuilder.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chooseaddress, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up butston, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {

    }
}
