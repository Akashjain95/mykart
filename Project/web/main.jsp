<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF=8" />
<link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/tabs.css" />
    <link rel="stylesheet" type="text/css" href="css/tabstyles.css" />
      <script src="js/modernizr.custom.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery-1.11.3.min.js"></script>

<script src="js/jquery-1.8.3.min.js"></script>

<!-- bxSlider Javascript file -->
<script src="js/jquery.bxslider.min.js"></script>
<!-- bxSlider CSS file -->
<link href="lib/jquery.bxslider.css" rel="stylesheet" />
<link rel="stylesheet" href="styles.css">
<script>
    $(document).ready(function() {
        $('.bxslider').bxSlider({
            auto: true,
            autoControls: true
        });
    });
</script>
<style>
    
footer {
  padding-top: 20px;
  padding-bottom: 20px;
  padding-left: 2%;
  background-color: black;
  color: white;
  clear: left;
}    
.txt
{
color:blue;
}
#text {
	font-family: 'Myriad Pro', Helvetica, Arial sans-serif;
	font-size: 2em;
	width: 11%;
	text-align: left;
	margin: 20px 0;
        
}
#text a {
	text-decoration: none;
	font-weight: bold;
	color: #ffffff;
	text-shadow: 0 2px 0 #721e1e, 0px 2px 5px rgba(0,0,0,0.5);
	position: relative;
	-webkit-transition: all 0.1s ease-in;
}
#text a:hover {
	text-shadow: 0 2px 0 #d84f4f, 0 4px 0 #d54646, 0 6px 0 #ce3333, 0 8px 0 #b92e2e, 0 10px 0 #912525, 0 12px 0 #721e1e, 0px 12px 10px rgba(0,0,0,0.5);
	top: -12px;
}
#text a:active {
	text-shadow: none;
	bottom: 0;
	text-shadow: 0px 0px 7px rgba(0,0,0,0.5);
}
table
{
border-collapse:collapse;
border-spacing:0;
}
body
{
overflow-x: hidden;
overflow-y: scroll;
background-color:white;
}
#ab
{
-webkit-background-size: 100% 100%;
 -moz-background-size: 100% 100%;
 -o-background-size: 100% 100%;
 background-size: 100% 100%;    
}
</style>
</head>
<body>    
    
<div class="container">
      <!-- Top Navigation -->
      
      
      <section>
        <div class="tabs tabs-style-bar">
          <nav>
            <ul>
              <li><a href="f3.jsp" class="icon icon-home" style="text-decoration:none"><span style="font-size:18px">MYKART</span></a></li>
              <li><a href="" class="" style="text-decoration:none"><span></span></a></li>
              <li><a href="contactus.jsp" class="icon icon-box" style="text-decoration:none"><span>CONTACT US</span></a></li>
              <li><a href="aboutus.jsp" class="icon icon-display"style="text-decoration:none"><span>ABOUT US</span></a></li>
              <li><a href="f3.jsp" class="icon icon-upload"style="text-decoration:none"><span>SIGN UP</span></a></li>
              <li><a href="f2.jsp" class="icon icon-tools"style="text-decoration:none"><span>LOGIN</span></a></li>
            </ul>
          </nav>
          <div class="content-wrap">
            
          </div><!-- /content -->
        </div><!-- /tabs -->
      </section>
      
      
      
    </div><!-- /container -->
    <script src="js/cbpFWTabs.js"></script>
    <script>
      (function() {

        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
          new CBPFWTabs( el );
        });

      })();
    </script>
  
<!--<a href="f3.jsp">
<img src="logo.png" style="position:fixed;top:0px;width:103px;height:100px;left:-7px;" title="GET STARTED!">
</a>-->
<div class="container-fluid">    
<img src="main.png" class="ab" style="position:absolute;top:67px;z-index:-1;width:1372px;height:500px;left:35px">
<img src="midribbon.png" style="position:absolute;top:579px;z-index:-1;width:1200px;height:300px;left:100px">
</div>
<div style="width:1200px;height:20px;align:center;padding-left:250px;position:absolute;top:900px;z-index:-1;left:-20px">
<ul class="bxslider">
  <li><img src="1.jpg" /></li>
  <li><img src="2.jpg" /></li>  
  <li><img src="3.jpg" /></li>
  <li><img src="4.jpg" /></li>
  <li><img src="5.jpg" /></li>
</ul>
</div>
<div>
  <footer style="position:absolute;top:1700px ;width:1349px"> � Copyright 2015 MyKart</footer>
</div>
</body>
</html>