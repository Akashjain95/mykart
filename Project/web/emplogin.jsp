<!doctype html>
<html>
<head>
<script src="jquery/jquery-2.0.3.min.js"></script>
<script src="bootstrap/bootstrap-2.3.1.min.js"></script>    
<style>
/* 
login page
*/
@charset "utf-8";
@import url(http://fonts.googleapis.com/css?family=Titan+One);
@import url(http://weloveiconfonts.com/api/?family=fontawesome);
@import url(http://meyerweb.com/eric/tools/css/reset/reset.css);

body { 
  background: url(back.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

	
  background-image:-webkit-linear-gradient(right);
	background-image:-moz-linear-gradient(right);
  background-image:-o-linear-gradient();
	background-image:-ms-linear-gradient();
	background-image:linear-gradient(to left);
  text-align: center;
	font-family: Arial, sans-serif;
	font-size: 14px;
	line-height: 1.5em;
} 


[class*="fontawesome-"]:before {
  font-family: 'FontAwesome', sans-serif;
}

input {
	font-size: 1em;
	line-height: 1.5em;
	margin: 0;
	padding: 0;
	-webkit-appearance: none;
}

.Registro {
	margin: 50px auto;
	width: 242px;
}

.Registro span {
	color: hsl(5, 50%, 57%);
	display: block;
	height: 48px;
	line-height: 48px;
	position: absolute;
	text-align: center;
	width: 36px;
}

.Registro input {
	border: none;
	height: 48px;
	outline: none;
}

.Registro input[type="text"] {
	background-color: #fff;
	border-top: 2px solid #2c90c6;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-radius: 5px 5px 0 0;
	-moz-border-radius: 5px 5px 0 0;
	-webkit-border-radius: 5px 5px 0 0;
  -o-border-radius: 5px 5px 0 0;
  -ms-border-radius: 5px 5px 0 0;
	color: #363636;
	padding-left: 36px;
	width: 204px;
}

.Registro input[type="password"] {
	background-color: #fff;
	border-top: 2px solid #2c90c6;
	border-right: 1px solid #000;
	border-bottom: 2px solid #2c90c6;
	border-left: 1px solid #000;
	border-radius: 0 0 5px 5px;
	-moz-border-radius: 0 0 5px 5px;
	-webkit-border-radius: 0 0 5px 5px;
  -o-border-radius: 0 0 5px 5px;
  -ms-border-radius: 0 0 5px 5px;
	color: #363636;
	margin-bottom: 20px;
	padding-left: 36px;
	width: 204px;
}

.Registro input[type="submit"] {
	background-color: #2c90c6;
	border: 1px solid #2c90c6;
	border-radius: 15px;
	-moz-border-radius: 15px;
	-webkit-border-radius: 15px;
  -ms-border-radius: 15px;
  -o-border-radius: 15px;
	color: #fff;
	font-weight: bold;
	line-height: 48px;
	text-align: center;
	text-transform: uppercase;
	width: 240px;
}

.Registro input[type="submit"]:hover {
	background-color: #2c70c6;
  box-shadow: 2px 2px 20px  #2c90c6, #fff 0 -1px 2px;
}

.texto {
  color: #2c90c6; 
  font-size: 40px; 
  margin: 2% auto;
  text-align: center;
  font-family: 'Titan One';   
  text-shadow: 1px 2px 1px  rgba(0,0,0,.5);
  padding-top: 40px;
}

p:hover {
  text-shadow: 2px 2px 20px  #2c90c6, #fff 0 -1px 2px;
}

</style>
</head>
<body>
<!-- 
Design register pure CSS
Developed by @mrjopino
-->
<script>
function validate()
{
var f=1;
var x=document.getElementById("f2");
var uname=x.uname.value;
//alert(uname);
if(uname.length==0)
{
p1.innerHTML="*Required Field";
f=0;
}
else
{
p1.innerHTML="<br>"	
}
var password=x.password.value;
if(password.length==0)
{
p2.innerHTML="*Required Field";
f=0;
}
else
{
p2.innerHTML="<br>"	
}
if(f==0)
return false;
return true;
}
</script>
<p class="texto"> Employee Login!</p>
<div class="Registro">
<form method="post" id="f2">
<span class="fontawesome-user"></span><input type="text"  id="uname" name="uname" required placeholder="User name"  autocomplete="off" > 
<p id="p1" style="color:red"> <br></p>
<span class="fontawesome-lock"></span><input type="password" name="password" id="password" required placeholder="Password" autocomplete="off"> 
<p id="p2" style="color:red"> <br></p>
<input type="submit" value="Login!" title="Login" onclick="return validate()"  formaction="emplog.jsp">
<br><br>
<input type="submit" value="Home" title="home" formaction="main.jsp">
<br><br>
<input type="submit" value="Sellers login" title="Sellers" formaction="f2.jsp">
</form>
</div>
</body>
</html>