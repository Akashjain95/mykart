package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class contact_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!doctype html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("  <style>\n");
      out.write("body {\n");
      out.write("  margin: 0px;\n");
      out.write("  color: #151515;\n");
      out.write("  font-family: \"Gill Sans\", \"Gill Sans MT\", \"Myriad Pro\", \"DejaVu Sans Condensed\", Helvetica, Arial, sans-serif;\n");
      out.write("  background-color: #EFF5F8;\n");
      out.write("}\n");
      out.write("\n");
      out.write("h1, h2 {\n");
      out.write("  color: black;\n");
      out.write("  font-style: normal;\n");
      out.write("  font-weight: 600;\n");
      out.write("  font-family: source-sans-pro;\n");
      out.write("}\n");
      out.write("\n");
      out.write("h1 {\n");
      out.write("  font-size: 72px;\n");
      out.write("  margin-top: 0px;\n");
      out.write("  margin-bottom: 0px;\n");
      out.write(" float:left;\n");
      out.write("  text-transform: uppercase;\n");
      out.write("}\n");
      out.write("h2 {\n");
      out.write("  margin-top: 0.5em;\n");
      out.write("}\n");
      out.write("#main {\n");
      out.write("  width: 58%;\n");
      out.write("  margin-left: 2%;\n");
      out.write("  float: left;\n");
      out.write("}\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("footer {\n");
      out.write("  padding-top: 2px;\n");
      out.write("  padding-bottom: 2px;\n");
      out.write("  padding-left: 2%;\n");
      out.write("  background-color: black;\n");
      out.write("  color: white;\n");
      out.write("  clear: left;\n");
      out.write("}\n");
      out.write("\n");
      out.write("  </style>\n");
      out.write("<meta charset=\"utf-8\">\n");
      out.write("<title>Contact Us</title>\n");
      out.write("<link href=\"ab.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("<script>var __adobewebfontsappname__=\"dreamweaver\"</script><script src=\"http://use.edgefonts.net/source-sans-pro:n2,n6,n3:default.js\" type=\"text/javascript\"></script>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("<div id=\"wrapper\">\n");
      out.write("  <header id=\"top\">\n");
      out.write("    <h1>Contact Us</h1>\n");
      out.write("    </header><hr size=\"10\" color=\"black\">\n");
      out.write("    <br><br><br><br>\n");
      out.write("  <div>\n");
      out.write("  <h3>Phone number:9871998901</h3>\n");
      out.write("  <h3>Email id:s33.adhikari@gmail.com</h3>\n");
      out.write("  <h3>Office Location:Sector 62,Noida(U.P)</h3>\n");
      out.write("  </div>\n");
      out.write("  <footer style=\"position:absolute;bottom:10px ;width:1329px\">© Copyright 2015 MyKart</footer>\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
