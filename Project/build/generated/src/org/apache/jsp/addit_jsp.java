package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import javax.sql.*;
import javax.naming.InitialContext;
import java.sql.PreparedStatement.*;

public final class addit_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=windows-1252");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");

   
    String pid = request.getParameter("pid");
    int q = Integer.parseInt(request.getParameter("qty"));
     int price = Integer.parseInt(request.getParameter("price"));
    String brand = request.getParameter("brand");
    String cat=request.getParameter("s1");
    String uid = (String)session.getAttribute("user_id");
    String up=request.getParameter("pic");
    java.io.File f=new java.io.File("up2"); 
    java.io.FileInputStream fis= new java.io.FileInputStream(f); 
 
      out.write("\n");
      out.write("   ");

    Class.forName("com.mysql.jdbc.Driver").newInstance();
    String url="jdbc:mysql://localhost:3306/mykart";
    Connection con = DriverManager.getConnection(url,"root", "password");
    Statement st = con.createStatement();   
   Statement st1 = con.createStatement();   
    ResultSet rs=st.executeQuery("select * from item where u_id='" + uid + "' and pid='"+pid+"'");
    ResultSet rs1=st1.executeQuery("select * from item where u_id='" + uid + "' and category='" +cat + "' and brand ='" + brand + "' and pid='"+pid+"'");  
         
       if(rs.next())
       { 
         if(rs1.next())
         {   
         String h=rs.getString(2);
       int t=q+Integer.parseInt(h);
       String query = "update item set qty = ? where u_id = ? and pid= ?";
      PreparedStatement preparedStmt = con.prepareStatement(query);
      preparedStmt.setInt(1, t);
      preparedStmt.setString(2, uid);
      preparedStmt.setString(3, pid);
      // execute the java preparedstatement
      preparedStmt.executeUpdate();
      
      out.write("\n");
      out.write("      <script>\n");
      out.write("          alert(\" Product quantity succesfully increased!\" );\n");
      out.write("          window.location = 'additem.jsp';\n");
      out.write("     </script> \n");
      out.write("      ");
}
      else
         {
      out.write("\n");
      out.write("         <script>\n");
      out.write("          alert(\" product id already exists!\" );\n");
      out.write("          window.location = 'additem.jsp';\n");
      out.write("     </script>  \n");
      out.write("         \n");
      out.write("         ");
}       
       }
       else
       {    
       int i = st.executeUpdate("insert into item(pid, qty, price,brand,category,u_id) values ('" + pid + "','" + q + "','" + price +"','"+ brand +"','"+ cat +"','"+ uid+ "')");  
      out.write("\n");
      out.write("       <script>\n");
      out.write("          alert(\" Product added succesfully!\" );\n");
      out.write("          window.location = 'additem.jsp';\n");
      out.write("     </script> \n");
      out.write("      \n");
      out.write("     \n");
      out.write("     ");
  }
      
      out.write("\n");
      out.write("      \n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
