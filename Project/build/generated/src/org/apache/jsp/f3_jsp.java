package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class f3_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!doctype html>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("    \r\n");
      out.write("<script src=\"jquery/jquery-2.0.3.min.js\"></script>\r\n");
      out.write("<script src=\"bootstrap/bootstrap-2.3.1.min.js\"></script>    \r\n");
      out.write("<style>\r\n");
      out.write("/* \r\n");
      out.write("Registration/signup\r\n");
      out.write("*/\r\n");
      out.write("@charset \"utf-8\";\r\n");
      out.write("@import url(http://fonts.googleapis.com/css?family=Titan+One);\r\n");
      out.write("@import url(http://weloveiconfonts.com/api/?family=fontawesome);\r\n");
      out.write("@import url(http://meyerweb.com/eric/tools/css/reset/reset.css);\r\n");
      out.write("\r\n");
      out.write("body { \r\n");
      out.write("  background: url(back.jpg) no-repeat center center fixed; \r\n");
      out.write("  -webkit-background-size: cover;\r\n");
      out.write("  -moz-background-size: cover;\r\n");
      out.write("  -o-background-size: cover;\r\n");
      out.write("  background-size: cover;\r\n");
      out.write("\r\n");
      out.write("\t\r\n");
      out.write("  background-image:-webkit-linear-gradient(right);\r\n");
      out.write("\tbackground-image:-moz-linear-gradient(right);\r\n");
      out.write("  background-image:-o-linear-gradient();\r\n");
      out.write("\tbackground-image:-ms-linear-gradient();\r\n");
      out.write("\tbackground-image:linear-gradient(to left);\r\n");
      out.write("  text-align: center;\r\n");
      out.write("\tfont-family: Arial, sans-serif;\r\n");
      out.write("\tfont-size: 14px;\r\n");
      out.write("\tline-height: 1.5em;\r\n");
      out.write("} \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("[class*=\"fontawesome-\"]:before {\r\n");
      out.write("  font-family: 'FontAwesome', sans-serif;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("input {\r\n");
      out.write("\tfont-size: 1em;\r\n");
      out.write("\tline-height: 1.5em;\r\n");
      out.write("\tmargin: 0;\r\n");
      out.write("\tpadding: 0;\r\n");
      out.write("\t-webkit-appearance: none;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".Registro {\r\n");
      out.write("\tmargin: 50px auto;\r\n");
      out.write("\twidth: 242px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".Registro span {\r\n");
      out.write("\tcolor: hsl(5, 50%, 57%);\r\n");
      out.write("\tdisplay: block;\r\n");
      out.write("\theight: 48px;\r\n");
      out.write("\tline-height: 48px;\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\ttext-align: center;\r\n");
      out.write("\twidth: 36px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".Registro input {\r\n");
      out.write("\tborder: none;\r\n");
      out.write("\theight: 48px;\r\n");
      out.write("\toutline: none;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".Registro input[type=\"text\"] {\r\n");
      out.write("\tbackground-color: #fff;\r\n");
      out.write("\tborder-top: 2px solid #2c90c6;\r\n");
      out.write("\tborder-right: 1px solid #000;\r\n");
      out.write("\tborder-left: 1px solid #000;\r\n");
      out.write("\tborder-radius: 5px 5px 0 0;\r\n");
      out.write("\t-moz-border-radius: 5px 5px 0 0;\r\n");
      out.write("\t-webkit-border-radius: 5px 5px 0 0;\r\n");
      out.write("  -o-border-radius: 5px 5px 0 0;\r\n");
      out.write("  -ms-border-radius: 5px 5px 0 0;\r\n");
      out.write("\tcolor: #363636;\r\n");
      out.write("\tpadding-left: 36px;\r\n");
      out.write("\twidth: 204px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".Registro input[type=\"password\"] {\r\n");
      out.write("\tbackground-color: #fff;\r\n");
      out.write("\tborder-top: 2px solid #2c90c6;\r\n");
      out.write("\tborder-right: 1px solid #000;\r\n");
      out.write("\tborder-bottom: 2px solid #2c90c6;\r\n");
      out.write("\tborder-left: 1px solid #000;\r\n");
      out.write("\tborder-radius: 0 0 5px 5px;\r\n");
      out.write("\t-moz-border-radius: 0 0 5px 5px;\r\n");
      out.write("\t-webkit-border-radius: 0 0 5px 5px;\r\n");
      out.write("  -o-border-radius: 0 0 5px 5px;\r\n");
      out.write("  -ms-border-radius: 0 0 5px 5px;\r\n");
      out.write("\tcolor: #363636;\r\n");
      out.write("\tmargin-bottom: 20px;\r\n");
      out.write("\tpadding-left: 36px;\r\n");
      out.write("\twidth: 204px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".Registro input[type=\"submit\"] {\r\n");
      out.write("\tbackground-color: #2c90c6;\r\n");
      out.write("\tborder: 1px solid #2c90c6;\r\n");
      out.write("\tborder-radius: 15px;\r\n");
      out.write("\t-moz-border-radius: 15px;\r\n");
      out.write("\t-webkit-border-radius: 15px;\r\n");
      out.write("  -ms-border-radius: 15px;\r\n");
      out.write("  -o-border-radius: 15px;\r\n");
      out.write("\tcolor: #fff;\r\n");
      out.write("\tfont-weight: bold;\r\n");
      out.write("\tline-height: 48px;\r\n");
      out.write("\ttext-align: center;\r\n");
      out.write("\ttext-transform: uppercase;\r\n");
      out.write("\twidth: 240px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".Registro input[type=\"submit\"]:hover {\r\n");
      out.write("\tbackground-color: #2c70c6;\r\n");
      out.write("  box-shadow: 2px 2px 20px  #2c90c6, #fff 0 -1px 2px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".texto {\r\n");
      out.write("  color: #2c90c6; \r\n");
      out.write("  font-size: 40px; \r\n");
      out.write("  margin: 2% auto;\r\n");
      out.write("  text-align: center;\r\n");
      out.write("  font-family: 'Titan One';   \r\n");
      out.write("  text-shadow: 1px 2px 1px  rgba(0,0,0,.5);\r\n");
      out.write("  padding-top: 40px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("p:hover {\r\n");
      out.write("  text-shadow: 2px 2px 20px  #2c90c6, #fff 0 -1px 2px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("</style>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("<script>\r\n");
      out.write("function validate()\r\n");
      out.write("{\r\n");
      out.write("var f=1;\r\n");
      out.write("var x=document.getElementById(\"f1\");\r\n");
      out.write("var uname=x.uname.value;\r\n");
      out.write("//alert(uname);\r\n");
      out.write("if(uname.length==0)\r\n");
      out.write("{\r\n");
      out.write("p1.innerHTML=\"*Required Field\";\r\n");
      out.write("f=0;\r\n");
      out.write("}\r\n");
      out.write("else if(uname.length>10)\r\n");
      out.write("{\r\n");
      out.write("p1.innerHTML=\"*User name MAX 10 characters!\";\r\n");
      out.write("f=0;\t\r\n");
      out.write("}\r\n");
      out.write("else if(uname.length<4)\r\n");
      out.write("{\r\n");
      out.write("p1.innerHTML=\"*User name MIN 4 characters!\";\r\n");
      out.write("f=0;    \r\n");
      out.write("}    \r\n");
      out.write("else\r\n");
      out.write("{\r\n");
      out.write("p1.innerHTML=\"<br>\"\t\r\n");
      out.write("}\r\n");
      out.write("var email=x.email.value;\r\n");
      out.write("if (/^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$/.test(email))\r\n");
      out.write("{\r\n");
      out.write("if(email.length<50)    \r\n");
      out.write("p2.innerHTML=\"<br>\";\r\n");
      out.write("else\r\n");
      out.write("{\r\n");
      out.write("f=0;    \r\n");
      out.write("p2.innerHTML=\"*Email id too long(MAX 50 characters)!\";    \r\n");
      out.write("}\r\n");
      out.write("}\r\n");
      out.write("else\r\n");
      out.write("{\r\n");
      out.write("f=0;\r\n");
      out.write("p2.innerHTML=\"*Enter a valid email address\";\r\n");
      out.write("}\t\r\n");
      out.write("\r\n");
      out.write("var password=x.password.value;\r\n");
      out.write("if(password.length==0)\r\n");
      out.write("{\r\n");
      out.write("p3.innerHTML=\"*Required Field\";\r\n");
      out.write("f=0;\r\n");
      out.write("}\r\n");
      out.write("else if(password.length<5)\r\n");
      out.write("{\r\n");
      out.write("p3.innerHTML=\"Password too weak(Min 5 characters)!\";\r\n");
      out.write("f=0;\t\r\n");
      out.write("}\r\n");
      out.write("else if(password.length>10)\r\n");
      out.write("{\r\n");
      out.write("p3.innerHTML=\"Password too long(Max 10 characters)!\";\r\n");
      out.write("f=0;    \r\n");
      out.write("}    \r\n");
      out.write("else\r\n");
      out.write("{\r\n");
      out.write("p3.innerHTML=\"<br>\"\t\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("if(f==0)\r\n");
      out.write("return false;\r\n");
      out.write("return true;\r\n");
      out.write("}\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("<p class=\"texto\">Signup!</p>\r\n");
      out.write("<div class=\"Registro\">\r\n");
      out.write("<form method=\"post\" id=\"f1\" action=\"register.jsp\">\r\n");
      out.write("<span class=\"fontawesome-user\"></span><input type=\"text\"  id=\"uname\" name=\"uname\" required placeholder=\"User name\"  autocomplete=\"off\" > \r\n");
      out.write("<p id=\"p1\" style=\"color:red\"> <br></p>\r\n");
      out.write("<span class=\"fontawesome-envelope-alt\"></span><input type=\"text\" name=\"email\" id=\"email\" required placeholder=\"Email id\" autocomplete=\"off\">\r\n");
      out.write("<p id=\"p2\" style=\"color:red\"> <br></p>\r\n");
      out.write("<span class=\"fontawesome-lock\"></span><input type=\"password\" name=\"password\" id=\"password\" required placeholder=\"Password\" autocomplete=\"off\"> \r\n");
      out.write("<p id=\"p3\" style=\"color:red\"> <br></p>\r\n");
      out.write("<input type=\"submit\" value=\"Register!\" title=\"Register\" onclick=\"return validate()\">\r\n");
      out.write("<br><br>\r\n");
      out.write("<input type=\"submit\" value=\"Home\" title=\"home\" onclick=\"window.location='main.jsp' \">\r\n");
      out.write("</form>\r\n");
      out.write("</div> \r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
