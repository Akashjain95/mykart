package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class main_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!doctype html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("<meta charset=\"UTF=8\" />\n");
      out.write("<link rel=\"shortcut icon\" href=\"../favicon.ico\">\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/normalize.css\" />\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/demo.css\" />\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/tabs.css\" />\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/tabstyles.css\" />\n");
      out.write("      <script src=\"js/modernizr.custom.js\"></script>\n");
      out.write("<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("<script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("<script src=\"js/bootstrap.js\"></script>\n");
      out.write("<script src=\"js/jquery-1.11.3.min.js\"></script>\n");
      out.write("\n");
      out.write("<script src=\"js/jquery-1.8.3.min.js\"></script>\n");
      out.write("\n");
      out.write("<!-- bxSlider Javascript file -->\n");
      out.write("<script src=\"js/jquery.bxslider.min.js\"></script>\n");
      out.write("<!-- bxSlider CSS file -->\n");
      out.write("<link href=\"lib/jquery.bxslider.css\" rel=\"stylesheet\" />\n");
      out.write("<link rel=\"stylesheet\" href=\"styles.css\">\n");
      out.write("<script>\n");
      out.write("    $(document).ready(function() {\n");
      out.write("        $('.bxslider').bxSlider({\n");
      out.write("            auto: true,\n");
      out.write("            autoControls: true\n");
      out.write("        });\n");
      out.write("    });\n");
      out.write("</script>\n");
      out.write("<style>\n");
      out.write("    \n");
      out.write("footer {\n");
      out.write("  padding-top: 20px;\n");
      out.write("  padding-bottom: 20px;\n");
      out.write("  padding-left: 2%;\n");
      out.write("  background-color: black;\n");
      out.write("  color: white;\n");
      out.write("  clear: left;\n");
      out.write("}    \n");
      out.write(".txt\n");
      out.write("{\n");
      out.write("color:blue;\n");
      out.write("}\n");
      out.write("#text {\n");
      out.write("\tfont-family: 'Myriad Pro', Helvetica, Arial sans-serif;\n");
      out.write("\tfont-size: 2em;\n");
      out.write("\twidth: 11%;\n");
      out.write("\ttext-align: left;\n");
      out.write("\tmargin: 20px 0;\n");
      out.write("        \n");
      out.write("}\n");
      out.write("#text a {\n");
      out.write("\ttext-decoration: none;\n");
      out.write("\tfont-weight: bold;\n");
      out.write("\tcolor: #ffffff;\n");
      out.write("\ttext-shadow: 0 2px 0 #721e1e, 0px 2px 5px rgba(0,0,0,0.5);\n");
      out.write("\tposition: relative;\n");
      out.write("\t-webkit-transition: all 0.1s ease-in;\n");
      out.write("}\n");
      out.write("#text a:hover {\n");
      out.write("\ttext-shadow: 0 2px 0 #d84f4f, 0 4px 0 #d54646, 0 6px 0 #ce3333, 0 8px 0 #b92e2e, 0 10px 0 #912525, 0 12px 0 #721e1e, 0px 12px 10px rgba(0,0,0,0.5);\n");
      out.write("\ttop: -12px;\n");
      out.write("}\n");
      out.write("#text a:active {\n");
      out.write("\ttext-shadow: none;\n");
      out.write("\tbottom: 0;\n");
      out.write("\ttext-shadow: 0px 0px 7px rgba(0,0,0,0.5);\n");
      out.write("}\n");
      out.write("table\n");
      out.write("{\n");
      out.write("border-collapse:collapse;\n");
      out.write("border-spacing:0;\n");
      out.write("}\n");
      out.write("body\n");
      out.write("{\n");
      out.write("overflow-x: hidden;\n");
      out.write("overflow-y: scroll;\n");
      out.write("background-color:white;\n");
      out.write("}\n");
      out.write("#ab\n");
      out.write("{\n");
      out.write("-webkit-background-size: 100% 100%;\n");
      out.write(" -moz-background-size: 100% 100%;\n");
      out.write(" -o-background-size: 100% 100%;\n");
      out.write(" background-size: 100% 100%;    \n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>    \n");
      out.write("    \n");
      out.write("<div class=\"container\">\n");
      out.write("      <!-- Top Navigation -->\n");
      out.write("      \n");
      out.write("      \n");
      out.write("      <section>\n");
      out.write("        <div class=\"tabs tabs-style-bar\">\n");
      out.write("          <nav>\n");
      out.write("            <ul>\n");
      out.write("              <li><a href=\"f3.jsp\" class=\"icon icon-home\" style=\"text-decoration:none\"><span style=\"font-size:18px\">MYKART</span></a></li>\n");
      out.write("              <li><a href=\"\" class=\"\" style=\"text-decoration:none\"><span></span></a></li>\n");
      out.write("              <li><a href=\"contactus.jsp\" class=\"icon icon-box\" style=\"text-decoration:none\"><span>CONTACT US</span></a></li>\n");
      out.write("              <li><a href=\"aboutus.jsp\" class=\"icon icon-display\"style=\"text-decoration:none\"><span>ABOUT US</span></a></li>\n");
      out.write("              <li><a href=\"f3.jsp\" class=\"icon icon-upload\"style=\"text-decoration:none\"><span>SIGN UP</span></a></li>\n");
      out.write("              <li><a href=\"f2.jsp\" class=\"icon icon-tools\"style=\"text-decoration:none\"><span>LOGIN</span></a></li>\n");
      out.write("            </ul>\n");
      out.write("          </nav>\n");
      out.write("          <div class=\"content-wrap\">\n");
      out.write("            \n");
      out.write("          </div><!-- /content -->\n");
      out.write("        </div><!-- /tabs -->\n");
      out.write("      </section>\n");
      out.write("      \n");
      out.write("      \n");
      out.write("      \n");
      out.write("    </div><!-- /container -->\n");
      out.write("    <script src=\"js/cbpFWTabs.js\"></script>\n");
      out.write("    <script>\n");
      out.write("      (function() {\n");
      out.write("\n");
      out.write("        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {\n");
      out.write("          new CBPFWTabs( el );\n");
      out.write("        });\n");
      out.write("\n");
      out.write("      })();\n");
      out.write("    </script>\n");
      out.write("  \n");
      out.write("<!--<a href=\"f3.jsp\">\n");
      out.write("<img src=\"logo.png\" style=\"position:fixed;top:0px;width:103px;height:100px;left:-7px;\" title=\"GET STARTED!\">\n");
      out.write("</a>-->\n");
      out.write("<div class=\"container-fluid\">    \n");
      out.write("<img src=\"main.png\" class=\"ab\" style=\"position:absolute;top:67px;z-index:-1;width:1372px;height:500px;left:35px\">\n");
      out.write("<img src=\"midribbon.png\" style=\"position:absolute;top:579px;z-index:-1;width:1200px;height:300px;left:100px\">\n");
      out.write("</div>\n");
      out.write("<div style=\"width:1200px;height:20px;align:center;padding-left:250px;position:absolute;top:900px;z-index:-1;left:-20px\">\n");
      out.write("<ul class=\"bxslider\">\n");
      out.write("  <li><img src=\"1.jpg\" /></li>\n");
      out.write("  <li><img src=\"2.jpg\" /></li>  \n");
      out.write("  <li><img src=\"3.jpg\" /></li>\n");
      out.write("  <li><img src=\"4.jpg\" /></li>\n");
      out.write("  <li><img src=\"5.jpg\" /></li>\n");
      out.write("</ul>\n");
      out.write("</div>\n");
      out.write("<div>\n");
      out.write("  <footer style=\"position:absolute;top:1700px ;width:1349px\"> © Copyright 2015 MyKart</footer>\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
