package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class f2_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!doctype html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<script src=\"jquery/jquery-2.0.3.min.js\"></script>\n");
      out.write("<script src=\"bootstrap/bootstrap-2.3.1.min.js\"></script>    \n");
      out.write("<style>\n");
      out.write("/* \n");
      out.write("login page\n");
      out.write("*/\n");
      out.write("@charset \"utf-8\";\n");
      out.write("@import url(http://fonts.googleapis.com/css?family=Titan+One);\n");
      out.write("@import url(http://weloveiconfonts.com/api/?family=fontawesome);\n");
      out.write("@import url(http://meyerweb.com/eric/tools/css/reset/reset.css);\n");
      out.write("\n");
      out.write("body { \n");
      out.write("  background: url(back.jpg) no-repeat center center fixed; \n");
      out.write("  -webkit-background-size: cover;\n");
      out.write("  -moz-background-size: cover;\n");
      out.write("  -o-background-size: cover;\n");
      out.write("  background-size: cover;\n");
      out.write("\n");
      out.write("\t\n");
      out.write("  background-image:-webkit-linear-gradient(right);\n");
      out.write("\tbackground-image:-moz-linear-gradient(right);\n");
      out.write("  background-image:-o-linear-gradient();\n");
      out.write("\tbackground-image:-ms-linear-gradient();\n");
      out.write("\tbackground-image:linear-gradient(to left);\n");
      out.write("  text-align: center;\n");
      out.write("\tfont-family: Arial, sans-serif;\n");
      out.write("\tfont-size: 14px;\n");
      out.write("\tline-height: 1.5em;\n");
      out.write("} \n");
      out.write("\n");
      out.write("\n");
      out.write("[class*=\"fontawesome-\"]:before {\n");
      out.write("  font-family: 'FontAwesome', sans-serif;\n");
      out.write("}\n");
      out.write("\n");
      out.write("input {\n");
      out.write("\tfont-size: 1em;\n");
      out.write("\tline-height: 1.5em;\n");
      out.write("\tmargin: 0;\n");
      out.write("\tpadding: 0;\n");
      out.write("\t-webkit-appearance: none;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".Registro {\n");
      out.write("\tmargin: 50px auto;\n");
      out.write("\twidth: 242px;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".Registro span {\n");
      out.write("\tcolor: hsl(5, 50%, 57%);\n");
      out.write("\tdisplay: block;\n");
      out.write("\theight: 48px;\n");
      out.write("\tline-height: 48px;\n");
      out.write("\tposition: absolute;\n");
      out.write("\ttext-align: center;\n");
      out.write("\twidth: 36px;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".Registro input {\n");
      out.write("\tborder: none;\n");
      out.write("\theight: 48px;\n");
      out.write("\toutline: none;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".Registro input[type=\"text\"] {\n");
      out.write("\tbackground-color: #fff;\n");
      out.write("\tborder-top: 2px solid #2c90c6;\n");
      out.write("\tborder-right: 1px solid #000;\n");
      out.write("\tborder-left: 1px solid #000;\n");
      out.write("\tborder-radius: 5px 5px 0 0;\n");
      out.write("\t-moz-border-radius: 5px 5px 0 0;\n");
      out.write("\t-webkit-border-radius: 5px 5px 0 0;\n");
      out.write("  -o-border-radius: 5px 5px 0 0;\n");
      out.write("  -ms-border-radius: 5px 5px 0 0;\n");
      out.write("\tcolor: #363636;\n");
      out.write("\tpadding-left: 36px;\n");
      out.write("\twidth: 204px;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".Registro input[type=\"password\"] {\n");
      out.write("\tbackground-color: #fff;\n");
      out.write("\tborder-top: 2px solid #2c90c6;\n");
      out.write("\tborder-right: 1px solid #000;\n");
      out.write("\tborder-bottom: 2px solid #2c90c6;\n");
      out.write("\tborder-left: 1px solid #000;\n");
      out.write("\tborder-radius: 0 0 5px 5px;\n");
      out.write("\t-moz-border-radius: 0 0 5px 5px;\n");
      out.write("\t-webkit-border-radius: 0 0 5px 5px;\n");
      out.write("  -o-border-radius: 0 0 5px 5px;\n");
      out.write("  -ms-border-radius: 0 0 5px 5px;\n");
      out.write("\tcolor: #363636;\n");
      out.write("\tmargin-bottom: 20px;\n");
      out.write("\tpadding-left: 36px;\n");
      out.write("\twidth: 204px;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".Registro input[type=\"submit\"] {\n");
      out.write("\tbackground-color: #2c90c6;\n");
      out.write("\tborder: 1px solid #2c90c6;\n");
      out.write("\tborder-radius: 15px;\n");
      out.write("\t-moz-border-radius: 15px;\n");
      out.write("\t-webkit-border-radius: 15px;\n");
      out.write("  -ms-border-radius: 15px;\n");
      out.write("  -o-border-radius: 15px;\n");
      out.write("\tcolor: #fff;\n");
      out.write("\tfont-weight: bold;\n");
      out.write("\tline-height: 48px;\n");
      out.write("\ttext-align: center;\n");
      out.write("\ttext-transform: uppercase;\n");
      out.write("\twidth: 240px;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".Registro input[type=\"submit\"]:hover {\n");
      out.write("\tbackground-color: #2c70c6;\n");
      out.write("  box-shadow: 2px 2px 20px  #2c90c6, #fff 0 -1px 2px;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".texto {\n");
      out.write("  color: #2c90c6; \n");
      out.write("  font-size: 40px; \n");
      out.write("  margin: 2% auto;\n");
      out.write("  text-align: center;\n");
      out.write("  font-family: 'Titan One';   \n");
      out.write("  text-shadow: 1px 2px 1px  rgba(0,0,0,.5);\n");
      out.write("  padding-top: 40px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("p:hover {\n");
      out.write("  text-shadow: 2px 2px 20px  #2c90c6, #fff 0 -1px 2px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("<!-- \n");
      out.write("Design register pure CSS\n");
      out.write("Developed by @mrjopino\n");
      out.write("-->\n");
      out.write("<script>\n");
      out.write("function validate()\n");
      out.write("{\n");
      out.write("var f=1;\n");
      out.write("var x=document.getElementById(\"f2\");\n");
      out.write("var uname=x.uname.value;\n");
      out.write("//alert(uname);\n");
      out.write("if(uname.length==0)\n");
      out.write("{\n");
      out.write("p1.innerHTML=\"*Required Field\";\n");
      out.write("f=0;\n");
      out.write("}\n");
      out.write("else\n");
      out.write("{\n");
      out.write("p1.innerHTML=\"<br>\"\t\n");
      out.write("}\n");
      out.write("var password=x.password.value;\n");
      out.write("if(password.length==0)\n");
      out.write("{\n");
      out.write("p2.innerHTML=\"*Required Field\";\n");
      out.write("f=0;\n");
      out.write("}\n");
      out.write("else\n");
      out.write("{\n");
      out.write("p2.innerHTML=\"<br>\"\t\n");
      out.write("}\n");
      out.write("if(f==0)\n");
      out.write("return false;\n");
      out.write("return true;\n");
      out.write("}\n");
      out.write("</script>\n");
      out.write("<p class=\"texto\">Login!</p>\n");
      out.write("<div class=\"Registro\">\n");
      out.write("<form method=\"post\" id=\"f2\" >\n");
      out.write("<span class=\"fontawesome-user\"></span><input type=\"text\"  id=\"uname\" name=\"uname\" required placeholder=\"User name\"  autocomplete=\"off\" > \n");
      out.write("<p id=\"p1\" style=\"color:red\"> <br></p>\n");
      out.write("<span class=\"fontawesome-lock\"></span><input type=\"password\" name=\"password\" id=\"password\" required placeholder=\"Password\" autocomplete=\"off\"> \n");
      out.write("<p id=\"p2\" style=\"color:red\"> <br></p>\n");
      out.write("<input type=\"submit\" value=\"Login!\" title=\"Login\" onclick=\"return validate()\" formaction=\"login.jsp\">\n");
      out.write("<br><br>\n");
      out.write("<input type=\"submit\" value=\"Home\" title=\"home\" formaction=\"main.jsp\">\n");
      out.write("</form>\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
