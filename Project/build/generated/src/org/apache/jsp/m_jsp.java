package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class m_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!doctype html>\n");
      out.write("<html lang=''>\n");
      out.write("<head>\n");
      out.write("   <meta charset='utf-8'>\n");
      out.write("   <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("   <link rel=\"stylesheet\" href=\"styles.css\">\n");
      out.write("   <script src=\"http://code.jquery.com/jquery-latest.min.js\" type=\"text/javascript\"></script>\n");
      out.write("   <script src=\"script.js\"></script>\n");
      out.write("   <style>\n");
      out.write("   *{ margin: 0; padding: 0;}\n");
      out.write("\n");
      out.write("body {\n");
      out.write("\t/*To hide the horizontal scroller appearing during the animation*/\n");
      out.write("\toverflow: hidden;\n");
      out.write("}\n");
      out.write("\n");
      out.write("#clouds{\n");
      out.write("\tpadding: 100px 0;\n");
      out.write("\tbackground: #c9dbe9;\n");
      out.write("\tbackground: -webkit-linear-gradient(top, #c9dbe9 0%, #fff 100%);\n");
      out.write("\tbackground: -linear-gradient(top, #c9dbe9 0%, #fff 100%);\n");
      out.write("\tbackground: -moz-linear-gradient(top, #c9dbe9 0%, #fff 100%);\n");
      out.write("}\n");
      out.write("\n");
      out.write("/*Time to finalise the cloud shape*/\n");
      out.write(".cloud {\n");
      out.write("\twidth: 200px; height: 60px;\n");
      out.write("\tbackground: #fff;\n");
      out.write("\t\n");
      out.write("\tborder-radius: 200px;\n");
      out.write("\t-moz-border-radius: 200px;\n");
      out.write("\t-webkit-border-radius: 200px;\n");
      out.write("\t\n");
      out.write("\tposition: relative; \n");
      out.write("}\n");
      out.write("\n");
      out.write(".cloud:before, .cloud:after {\n");
      out.write("\tcontent: '';\n");
      out.write("\tposition: absolute; \n");
      out.write("\tbackground: #fff;\n");
      out.write("\twidth: 100px; height: 80px;\n");
      out.write("\tposition: absolute; top: -15px; left: 10px;\n");
      out.write("\t\n");
      out.write("\tborder-radius: 100px;\n");
      out.write("\t-moz-border-radius: 100px;\n");
      out.write("\t-webkit-border-radius: 100px;\n");
      out.write("\t\n");
      out.write("\t-webkit-transform: rotate(30deg);\n");
      out.write("\ttransform: rotate(30deg);\n");
      out.write("\t-moz-transform: rotate(30deg);\n");
      out.write("}\n");
      out.write("\n");
      out.write(".cloud:after {\n");
      out.write("\twidth: 120px; height: 120px;\n");
      out.write("\ttop: -55px; left: auto; right: 15px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("/*Time to animate*/\n");
      out.write(".x1 {\n");
      out.write("\t-webkit-animation: moveclouds 15s linear infinite;\n");
      out.write("\t-moz-animation: moveclouds 15s linear infinite;\n");
      out.write("\t-o-animation: moveclouds 15s linear infinite;\n");
      out.write("}\n");
      out.write("\n");
      out.write("/*variable speed, opacity, and position of clouds for realistic effect*/\n");
      out.write(".x2 {\n");
      out.write("\tleft: 200px;\n");
      out.write("\t\n");
      out.write("\t-webkit-transform: scale(0.6);\n");
      out.write("\t-moz-transform: scale(0.6);\n");
      out.write("\ttransform: scale(0.6);\n");
      out.write("\topacity: 0.6; /*opacity proportional to the size*/\n");
      out.write("\t\n");
      out.write("\t/*Speed will also be proportional to the size and opacity*/\n");
      out.write("\t/*More the speed. Less the time in 's' = seconds*/\n");
      out.write("\t-webkit-animation: moveclouds 25s linear infinite;\n");
      out.write("\t-moz-animation: moveclouds 25s linear infinite;\n");
      out.write("\t-o-animation: moveclouds 25s linear infinite;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".x3 {\n");
      out.write("\tleft: -250px; top: -200px;\n");
      out.write("\t\n");
      out.write("\t-webkit-transform: scale(0.8);\n");
      out.write("\t-moz-transform: scale(0.8);\n");
      out.write("\ttransform: scale(0.8);\n");
      out.write("\topacity: 0.8; /*opacity proportional to the size*/\n");
      out.write("\t\n");
      out.write("\t-webkit-animation: moveclouds 20s linear infinite;\n");
      out.write("\t-moz-animation: moveclouds 20s linear infinite;\n");
      out.write("\t-o-animation: moveclouds 20s linear infinite;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".x4 {\n");
      out.write("\tleft: 470px; top: -250px;\n");
      out.write("\t\n");
      out.write("\t-webkit-transform: scale(0.75);\n");
      out.write("\t-moz-transform: scale(0.75);\n");
      out.write("\ttransform: scale(0.75);\n");
      out.write("\topacity: 0.75; /*opacity proportional to the size*/\n");
      out.write("\t\n");
      out.write("\t-webkit-animation: moveclouds 18s linear infinite;\n");
      out.write("\t-moz-animation: moveclouds 18s linear infinite;\n");
      out.write("\t-o-animation: moveclouds 18s linear infinite;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".x5 {\n");
      out.write("\tleft: -150px; top: -150px;\n");
      out.write("\t\n");
      out.write("\t-webkit-transform: scale(0.8);\n");
      out.write("\t-moz-transform: scale(0.8);\n");
      out.write("\ttransform: scale(0.8);\n");
      out.write("\topacity: 0.8; /*opacity proportional to the size*/\n");
      out.write("\t\n");
      out.write("\t-webkit-animation: moveclouds 20s linear infinite;\n");
      out.write("\t-moz-animation: moveclouds 20s linear infinite;\n");
      out.write("\t-o-animation: moveclouds 20s linear infinite;\n");
      out.write("}\n");
      out.write("\n");
      out.write("@-webkit-keyframes moveclouds {\n");
      out.write("\t0% {margin-left: 1000px;}\n");
      out.write("\t100% {margin-left: -1000px;}\n");
      out.write("}\n");
      out.write("@-moz-keyframes moveclouds {\n");
      out.write("\t0% {margin-left: 1000px;}\n");
      out.write("\t100% {margin-left: -1000px;}\n");
      out.write("}\n");
      out.write("@-o-keyframes moveclouds {\n");
      out.write("\t0% {margin-left: 1000px;}\n");
      out.write("\t100% {margin-left: -1000px;}\n");
      out.write("}    \n");
      out.write(".button\n");
      out.write("{\n");
      out.write("  text-transform: uppercase;\n");
      out.write("  letter-spacing: 2px;\n");
      out.write("  text-align: center;\n");
      out.write("  color: #0C5;\n");
      out.write("\n");
      out.write("  font-size: 24px;\n");
      out.write("  font-family: \"Nunito\", sans-serif;\n");
      out.write("  font-weight: 300;\n");
      out.write("  \n");
      out.write("  margin: 5em auto;\n");
      out.write("  \n");
      out.write("  position: absolute; \n");
      out.write("  top:0; right:0; bottom:0; left:0;\n");
      out.write("  \n");
      out.write("  padding: 20px 0;\n");
      out.write("  width: 160px;\n");
      out.write("  height:10px;\n");
      out.write("\n");
      out.write("  background:red;\n");
      out.write("  border: 1px solid #0D6;\n");
      out.write("  color: #FFF;\n");
      out.write("  overflow: hidden;\n");
      out.write("  \n");
      out.write("  transition: all 0.5s;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".button:hover, .button:active \n");
      out.write("{\n");
      out.write("  text-decoration: none;\n");
      out.write("  color: #0C5;\n");
      out.write("  border-color: #0C5;\n");
      out.write("  background: yellow;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".button span \n");
      out.write("{\n");
      out.write("  display: inline-block;\n");
      out.write("  position: relative;\n");
      out.write("  padding-right: 0;\n");
      out.write("  \n");
      out.write("  transition: padding-right 0.5s;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".button span:after \n");
      out.write("{\n");
      out.write("  content: ' ';  \n");
      out.write("  position: absolute;\n");
      out.write("  top: 0;\n");
      out.write("  right: -18px;\n");
      out.write("  opacity: 0;\n");
      out.write("  width: 10px;\n");
      out.write("  height: 10px;\n");
      out.write("  margin-top: -10px;\n");
      out.write("\n");
      out.write("  background: rgba(0, 0, 0, 0);\n");
      out.write("  border: 3px solid #FFF;\n");
      out.write("  border-top: none;\n");
      out.write("  border-right: none;\n");
      out.write("\n");
      out.write("  transition: opacity 0.5s, top 0.5s, right 0.5s;\n");
      out.write("  transform: rotate(-45deg);\n");
      out.write("}\n");
      out.write("\n");
      out.write(".button:hover span, .button:active span \n");
      out.write("{\n");
      out.write("  padding-right: 30px;\n");
      out.write("}\n");
      out.write("\n");
      out.write(".button:hover span:after, .button:active span:after \n");
      out.write("{\n");
      out.write("  transition: opacity 0.5s, top 0.5s, right 0.5s;\n");
      out.write("  opacity: 1;\n");
      out.write("  border-color: #0C5;\n");
      out.write("  right: 0;\n");
      out.write("  top: 50%;\n");
      out.write("}    \n");
      out.write("   </style>   \n");
      out.write("   <title>Welcome , ");
      out.print( session.getAttribute("user_id"));
      out.write(" </title>\n");
      out.write("</head>\n");
      out.write("<body> \n");
      out.write("<div id='cssmenu'>\n");
      out.write("<ul>\n");
      out.write("    <li><a href='#'><img src=\"logo.jpg\"></a></li>\n");
      out.write("    <li style=\"width:50px\"><a href=''><span></span></a></li>\n");
      out.write("\n");
      out.write("   <li style=\"top:90px\"><a href='#'><span>About Us</span></a></li>\n");
      out.write("   <li style=\"top:90px\"><a href='additem.jsp'><span>Contact Us</span></a>\n");
      out.write("   </li>\n");
      out.write("  <li style=\"top:90px\"><a href='removeitem.jsp'><span>Sign Up</span></a></li>\n");
      out.write(" <li style=\"top:90px\"><a href='manageacc.jsp'><span>Login</span></a></li>\n");
      out.write(" \n");
      out.write("</ul>\n");
      out.write(" <div style=\"position:absolute;top:-120px;right:160px\">    \n");
      out.write(" </div>\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
