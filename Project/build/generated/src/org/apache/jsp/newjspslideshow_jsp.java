package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class newjspslideshow_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<script type=\"text/javascript\"> \n");
      out.write("var i = 0; \n");
      out.write("var image = new Array();   \n");
      out.write("// LIST OF IMAGES \n");
      out.write("image[0] = \"STEP 1.jpg\"; \n");
      out.write("image[1] = \"2.jpg\";\n");
      out.write("image[2]=\"STEP 3.jpg\";\n");
      out.write("image[3]=\"step3.jpg\";\n");
      out.write("var k = image.length-1;    \n");
      out.write("var caption = new Array(); \n");
      out.write("\n");
      out.write("// LIST OF CAPTIONS \n");
      out.write("caption[0] = \"DOING BUSINESS ON MYKART IS REALLY EASY!\"; \n");
      out.write("caption[1] = \"DOING BUSINESS ON MYKART IS REALLY EASY!\"; \n");
      out.write("caption[2] = \"DOING BUSINESS ON MYKART IS REALLY EASY!\"; \n");
      out.write("caption[3]=\"DOING BUSINESS ON MYKART IS REALLY EASY!\";\n");
      out.write("function next(){\n");
      out.write("var el = document.getElementById(\"mydiv\"); \n");
      out.write("el.innerHTML=caption[i]; \n");
      out.write("var img= document.getElementById(\"slide\"); \n");
      out.write("img.src= image[i]; \n");
      out.write("if(i < k ) { i++;}  \n");
      out.write("else  { i = 0; }\n");
      out.write("}\n");
      out.write("\n");
      out.write("function prev(){\n");
      out.write("var el = document.getElementById(\"mydiv\"); \n");
      out.write("el.innerHTML=caption[i]; \n");
      out.write("var img= document.getElementById(\"slide\"); \n");
      out.write("img.src= image[i]; \n");
      out.write("if(i >0 ) { i--;}  \n");
      out.write("else  { i = k; }\n");
      out.write("}\n");
      out.write("\n");
      out.write("function swapImage(){ \n");
      out.write("var el = document.getElementById(\"mydiv\"); \n");
      out.write("el.innerHTML=caption[i]; \n");
      out.write("var img= document.getElementById(\"slide\"); \n");
      out.write("img.src= image[i]; \n");
      out.write("if(i < k ) { i++;}  \n");
      out.write("else  { i = 0; } \n");
      out.write("setTimeout(\"swapImage()\",5000); \n");
      out.write("} \n");
      out.write("function addLoadEvent(func) { \n");
      out.write("var oldonload = window.onload; \n");
      out.write("if (typeof window.onload != 'function') { \n");
      out.write("window.onload = func; \n");
      out.write("} else  { \n");
      out.write("window.onload = function() { \n");
      out.write("if (oldonload) { \n");
      out.write("oldonload(); \n");
      out.write("} \n");
      out.write("func(); \n");
      out.write("} \n");
      out.write("} \n");
      out.write("} \n");
      out.write("addLoadEvent(function() { \n");
      out.write("swapImage(); \n");
      out.write("});  \n");
      out.write("</script> \n");
      out.write(" <table align=\"bottom\" style=\"border:none;background-color:transparent;position:absolute;top:500px;left:200px\"> \n");
      out.write("<tr> \n");
      out.write("<td>\n");
      out.write("<img onclick=\"prev()\" title=\"Previous\" alt =\"Prev\" height=\"25\" width=\"15\"src=\"prev.jpg\"/> \n");
      out.write("</td>\n");
      out.write("<td> \n");
      out.write("<img name=\"slide\" id=\"slide\" alt =\"my images\" height=\"285\" width=\"485\" src=\"image-1.png\"/> \n");
      out.write("</td> \n");
      out.write("<td>\n");
      out.write("<img onclick=\"next()\" title=\"Next\" alt =\"Next\" height=\"25\" width=\"15\" src=\"next.jpg\"/> \n");
      out.write("</td>\n");
      out.write("</tr> \n");
      out.write("<tr> \n");
      out.write("<td>\n");
      out.write("</td>\n");
      out.write("<td align=\"center\"style=\"font:small-caps bold 15px georgia; color:blue;\"> \n");
      out.write("<div id =\"mydiv\"></div>\n");
      out.write("</td> \n");
      out.write("<td>\n");
      out.write("</td>\n");
      out.write("</tr>  \n");
      out.write("</table>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
