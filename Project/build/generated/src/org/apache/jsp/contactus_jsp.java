package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class contactus_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<style>  \r\n");
      out.write("\r\n");
      out.write("body\r\n");
      out.write("{\r\n");
      out.write("  font-family: helvetica;\r\n");
      out.write("  text-align: left;\r\n");
      out.write("  padding-left:170px;\r\n");
      out.write("}  \r\n");
      out.write(".btn {\r\n");
      out.write("  border: 0;\r\n");
      out.write("  background: #0066CC;\r\n");
      out.write("  border-radius: 4px;\r\n");
      out.write("  box-shadow: 0 5px 0 #006599;\r\n");
      out.write("  color: #fff;\r\n");
      out.write("  cursor: pointer;\r\n");
      out.write("  font: inherit;\r\n");
      out.write("  margin: 0;\r\n");
      out.write("  outline: 0;\r\n");
      out.write("  padding: 12px 20px;\r\n");
      out.write("  transition: all .1s linear;\r\n");
      out.write("  text-transform: uppercase;\r\n");
      out.write("  width: 100px;\r\n");
      out.write("  font-weight: bold;\r\n");
      out.write("  text-decoration: none;\r\n");
      out.write("  text-align: center;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".btn:active {\r\n");
      out.write("  box-shadow: 0 2px 0 #006599;\r\n");
      out.write("  transform: translateY(3px);\r\n");
      out.write("}\r\n");
      out.write("</style>\r\n");
      out.write("<title>Contact Us</title>\r\n");
      out.write("<meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body background=\"back.jpg\" style=\"text-align:left\"><p><br><br>\r\n");
      out.write("<a href=\"main.jsp\" class=\"btn\" style=\"position:absolute;bottom:20px;left:220px\"> Back</a>        \r\n");
      out.write("<!-- Shell -->\r\n");
      out.write("<div>\r\n");
      out.write("  <div align=\"left \">\r\n");
      out.write("          \r\n");
      out.write("           \r\n");
      out.write("                    <b><span style=\"color:#3498db; font-size:40px\">Contact Us</span></b><br><br>\r\n");
      out.write("             \r\n");
      out.write("  <p style=\"color:white;font-size:20px\">\r\n");
      out.write("          Have a question? We have 24x7 customer service.Tell us about your issue and<br><br> we will get back to you within a few hours! \r\n");
      out.write("              \r\n");
      out.write("      <br>\r\n");
      out.write("      \r\n");
      out.write("      \r\n");
      out.write("  <br><br>\r\n");
      out.write("  Call us :<br><br>Pick up the phone and call us at +919957546483<br><br>\r\n");
      out.write("          We're available 24 hours a day, 7 days a week <br><br>\r\n");
      out.write("         <img src=\"images/email1.jpg\" width=\"32\" height=\"25\"/><b>  Mail us :<a href='mailto:sell@mykart.com' style=\"color:#0066CC\"> sell@mykart.com<br></a></b><br>\r\n");
      out.write("          Mykart Internet Private Limited,Sector 62,Opposite jiit<br><br>\r\n");
      out.write("          Noida, UP-786170</p>\r\n");
      out.write("\r\n");
      out.write("  <br><br><br><br><br><br><br><br><br><br><br></p>\r\n");
      out.write(" \r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
