package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class aboutus_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>About Us</title>\r\n");
      out.write("<meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("<style>\r\n");
      out.write("\r\n");
      out.write(".btn {\r\n");
      out.write("  border: 0;\r\n");
      out.write("  background: #0066CC;;\r\n");
      out.write("  border-radius: 4px;\r\n");
      out.write("  box-shadow: 0 5px 0 #006599;\r\n");
      out.write("  color: #fff;\r\n");
      out.write("  cursor: pointer;\r\n");
      out.write("  font: inherit;\r\n");
      out.write("  margin: 0;\r\n");
      out.write("  outline: 0;\r\n");
      out.write("  padding: 12px 20px;\r\n");
      out.write("  transition: all .1s linear;\r\n");
      out.write("  text-transform: uppercase;\r\n");
      out.write("  width: 100px;\r\n");
      out.write("  font-weight: bold;\r\n");
      out.write("  text-decoration: none;\r\n");
      out.write("  text-align: center;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".btn:active {\r\n");
      out.write("  box-shadow: 0 2px 0 #006599;\r\n");
      out.write("  transform: translateY(3px);\r\n");
      out.write("}\r\n");
      out.write("</style>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body background=\"back.jpg\" style=\"font-family: Gill, Helvetica, sans-serif\"><br><br>\r\n");
      out.write("    \r\n");
      out.write("    \r\n");
      out.write("    <a href=\"main.jsp\" class=\"btn\" style=\"position:absolute;bottom:20px;left:220px\"> Back</a>    \r\n");
      out.write("<!-- Shell -->\r\n");
      out.write("<div class>\r\n");
      out.write("  <div align=\"center\">\r\n");
      out.write("          <table width=\"850\" border=\"0\" >\r\n");
      out.write("            <tr>\r\n");
      out.write("                <td height=\"60\" scope=\"col\" style=\" color:#3498db; font-size: 37px\">\r\n");
      out.write("                    <b><span style=\"font-style:Comic Sans Ms\">About Us</span></b><br>\r\n");
      out.write("                    <br>\r\n");
      out.write("                </td>    \r\n");
      out.write("            </tr>\r\n");
      out.write("          </table>\r\n");
      out.write("  \r\n");
      out.write("          <table width=\"850\" border=\"0\" >\r\n");
      out.write("              <tr> \r\n");
      out.write("                  <td height=\"30\" style=\" color: white; font-size: 20px\"> Hello! </td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"15\"></td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"30\" style=\" color: white; font-size: 20px\">It's nice of you to take the time to get to know us better. Here are some things about us that we thought you might like to know.</td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"15\"></td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"30\" style=\" color: white; font-size: 20px\">Mykart seller portal went live on 2015 with the objective of making window shopping easily available to anyone who had internet access. Today, we're</td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"30\" style=\" color: white; font-size: 20px\">present across various categories including mobiles, cameras, computers and personal products, home appliances and electronics,</td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"30\" style=\" color: white; font-size: 20px\">stationery, perfumes, toys, shoes ?and still counting!</td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"15\"></td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"30\" style=\" color: white; font-size: 20px\">Be it our path-breaking services like  free shipping - and of course the great perks that we offer, everything we</td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"30\" style=\" color: white; font-size: 20px\">do revolves around our obsession with providing our sellers a memorable online selling experience. Then there's our </td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"30\" style=\" color: white; font-size: 20px\">dedicated delivery partners who work round the clock to personally make sure the packages reach on time.</td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"15\"></td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                  <td height=\"30\" style=\" color: white; font-size: 20px\">So it's no surprise that we're a favourite online shopping destination.</td>\r\n");
      out.write("              </tr>\r\n");
      out.write("          </table>\r\n");
      out.write("</div>\r\n");
      out.write("  <br><br><br><br><br><br><br>\r\n");
      out.write("  </div>\r\n");
      out.write("  \r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
