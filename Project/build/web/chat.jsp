
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang=''>
<script>
function chk()
{
var u=<%=session.getAttribute("user_id")%>    
if(u==null||u=="")
{
alert("You have been logged out!");
window.location="f2.jsp";
}
return;    
}    
</script>    
<head>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="styles.css">
   <link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery-1.11.3.min.js"></script>    
   <style>
   *{ margin: 0; padding: 0;}

body {
	/*To hide the horizontal scroller appearing during the animation*/
	overflow: hidden;
}

#clouds{
	padding: 100px 0;
	background: #c9dbe9;
	background: -webkit-linear-gradient(top, #c9dbe9 0%, #fff 100%);
	background: -linear-gradient(top, #c9dbe9 0%, #fff 100%);
	background: -moz-linear-gradient(top, #c9dbe9 0%, #fff 100%);
}

/*Time to finalise the cloud shape*/
.cloud {
	width: 200px; height: 60px;
	background: #fff;
	
	border-radius: 200px;
	-moz-border-radius: 200px;
	-webkit-border-radius: 200px;
	
	position: relative; 
}

.cloud:before, .cloud:after {
	content: '';
	position: absolute; 
	background: #fff;
	width: 100px; height: 80px;
	position: absolute; top: -15px; left: 10px;
	
	border-radius: 100px;
	-moz-border-radius: 100px;
	-webkit-border-radius: 100px;
	
	-webkit-transform: rotate(30deg);
	transform: rotate(30deg);
	-moz-transform: rotate(30deg);
}

.cloud:after {
	width: 120px; height: 120px;
	top: -55px; left: auto; right: 15px;
}

/*Time to animate*/
.x1 {
	-webkit-animation: moveclouds 15s linear infinite;
	-moz-animation: moveclouds 15s linear infinite;
	-o-animation: moveclouds 15s linear infinite;
}

/*variable speed, opacity, and position of clouds for realistic effect*/
.x2 {
	left: 200px;
	
	-webkit-transform: scale(0.6);
	-moz-transform: scale(0.6);
	transform: scale(0.6);
	opacity: 0.6; /*opacity proportional to the size*/
	
	/*Speed will also be proportional to the size and opacity*/
	/*More the speed. Less the time in 's' = seconds*/
	-webkit-animation: moveclouds 25s linear infinite;
	-moz-animation: moveclouds 25s linear infinite;
	-o-animation: moveclouds 25s linear infinite;
}

.x3 {
	left: -250px; top: -200px;
	
	-webkit-transform: scale(0.8);
	-moz-transform: scale(0.8);
	transform: scale(0.8);
	opacity: 0.8; /*opacity proportional to the size*/
	
	-webkit-animation: moveclouds 20s linear infinite;
	-moz-animation: moveclouds 20s linear infinite;
	-o-animation: moveclouds 20s linear infinite;
}

.x4 {
	left: 470px; top: -250px;
	
	-webkit-transform: scale(0.75);
	-moz-transform: scale(0.75);
	transform: scale(0.75);
	opacity: 0.75; /*opacity proportional to the size*/
	
	-webkit-animation: moveclouds 18s linear infinite;
	-moz-animation: moveclouds 18s linear infinite;
	-o-animation: moveclouds 18s linear infinite;
}

.x5 {
	left: -150px; top: -150px;
	
	-webkit-transform: scale(0.8);
	-moz-transform: scale(0.8);
	transform: scale(0.8);
	opacity: 0.8; /*opacity proportional to the size*/
	
	-webkit-animation: moveclouds 20s linear infinite;
	-moz-animation: moveclouds 20s linear infinite;
	-o-animation: moveclouds 20s linear infinite;
}

@-webkit-keyframes moveclouds {
	0% {margin-left: 1000px;}
	100% {margin-left: -1000px;}
}
@-moz-keyframes moveclouds {
	0% {margin-left: 1000px;}
	100% {margin-left: -1000px;}
}
@-o-keyframes moveclouds {
	0% {margin-left: 1000px;}
	100% {margin-left: -1000px;}
}    
.button
{
  text-transform: uppercase;
  letter-spacing: 2px;
  text-align: center;
  color: #0C5;

  font-size: 24px;
  font-family: "Nunito", sans-serif;
  font-weight: 300;
  
  margin: 5em auto;
  
  position: absolute; 
  top:0; right:0; bottom:0; left:0;
  
  padding: 20px 0;
  width: 160px;
  height:10px;

  background:red;
  border: 1px solid #0D6;
  color: #FFF;
  overflow: hidden;
  
  transition: all 0.5s;
}

.button:hover, .button:active 
{
  text-decoration: none;
  color: #0C5;
  border-color: #0C5;
  background: yellow;
}

.button span 
{
  display: inline-block;
  position: relative;
  padding-right: 0;
  
  transition: padding-right 0.5s;
}

.button span:after 
{
  content: ' ';  
  position: absolute;
  top: 0;
  right: -18px;
  opacity: 0;
  width: 10px;
  height: 10px;
  margin-top: -10px;

  background: rgba(0, 0, 0, 0);
  border: 3px solid #FFF;
  border-top: none;
  border-right: none;

  transition: opacity 0.5s, top 0.5s, right 0.5s;
  transform: rotate(-45deg);
}

.button:hover span, .button:active span 
{
  padding-right: 30px;
}

.button:hover span:after, .button:active span:after 
{
  transition: opacity 0.5s, top 0.5s, right 0.5s;
  opacity: 1;
  border-color: #0C5;
  right: 0;
  top: 50%;
}    
   </style>   
   <title>Welcome , <%= session.getAttribute("user_id")%> </title>
</head>
<body>    
 <script>chk();</script>   
<div id='cssmenu'>
<ul>
   <li><a href='#'><span>Home</span></a></li>
   <li><a href='additem.jsp'><span>Add Product</span></a>
   </li>
  <li><a href='removeitem.jsp'><span>Remove product</span></a></li>
 <li><a href='manageacc.jsp'><span>Manage account</span></a></li>
 <li><a href='viewproduct.jsp'><span>View Products</span></a></li>
<li><a href='modifyproduct.jsp'><span>Modify Product</span></a></li>
<li><a href='providediscount.jsp'><span>Manage Discount</span></a></li>
</ul>  
  <div style="position:absolute;top:10px;right:110px">    
<a href="logout.jsp" class="btn btn-info" role="button">Logout</a>
</div> 
</div>
<div id="clouds"  >
	<div class="cloud x1" style="z-index:-1"></div>
	<!-- Time for multiple clouds to dance around -->
	<div class="cloud x2" ></div>
	<div class="cloud x3"></div>
        <div class="cloud x4"></div>
	<div class="cloud x5"></div>
        </div> 
<div class="container" style="position:absolute;top:80px ">
  <h2>Welcome to Customer Query Section</h2>
  </div>
 <%Class.forName("com.mysql.jdbc.Driver").newInstance();
      Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mykart", "root","password");  
Statement st = con.createStatement(); 
    String sq;
      Statement st2 = con.createStatement();
     Statement st1 = con.createStatement(); 
    String u=(String)session.getAttribute("user_id");
    ResultSet rs1=st1.executeQuery("select * from complaint");
    ResultSet rs2=st2.executeQuery("select distinct mob from complaint");
%>       
<form style="position:absolute;top:140px" action="sendreply.jsp">
Select Customer:<br><select style="width:150px" id="s2" name="s2" >
<%
while(rs2.next())
{
sq= rs2.getString("mob");
%> 
<option value= "<%= sq %>" ><%=sq %></option> 
<%
} 
%>
</select>
<br>
Enter Reply:<br>
<input type="text" name="creply" id="creply" style="width:500px;height:100px"><br>
<button type="submit" class="btn btn-default" >
    Send
    </button>
</form>
<TABLE  class="table table-hover" style="position:absolute;top:350px;left:40px">
            <TR>
                <TH>QUERY</TH>
                <TH>TIME/DATE</TH>
                <TH>PID</TH>
                <TH>CUSTOMER</TH>
            </TR>
            
             
            <% while(rs1.next())
             {  
                 
            
            %>
          
            <TR>
                
                <TD> <%= rs1.getString(1) %></TD>
                <TD> <%= rs1.getString(2) %></TD>
                <TD> <%= rs1.getString(4) %></TD>
                <TD> <%= rs1.getString(3) %></TD>                
           </TR>
           
           <% } %>
         <% con.close(); rs1.close();  %>  
        </TABLE> 
</body>
</html>
