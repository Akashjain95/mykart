<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import ="java.sql.*" %>
<%@ page import="java.util.*" %>
<!doctype html>
<html lang=''>
    <script>
function chk()
{
var u=<%=session.getAttribute("user_id")%>    
if(u==null||u=="")
{
alert("You have been logged out!");
window.location="f2.jsp";
}
return;    
}    
</script>
<script type='text/javascript'> 
//<![CDATA[
msg = "==>MYKART<==";
msg = " Welcome!. " + msg;pos = 0;
function scrollMSG() {
document.title = msg.substring(pos, msg.length) + msg.substring(0, pos); pos++;
if (pos > msg.length) pos = 0
window.setTimeout("scrollMSG()",200);
}
scrollMSG();
//]]>
</script> 

<head>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="styles.css">
   <link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery-1.11.3.min.js"></script>
   <style>
   *{ margin: 0; padding: 0;}

body {
	/*To hide the horizontal scroller appearing during the animation*/
	overflow: hidden;
}

#clouds{
	padding: 100px 0;
	background: #c9dbe9;
	background: -webkit-linear-gradient(top, #c9dbe9 0%, #fff 100%);
	background: -linear-gradient(top, #c9dbe9 0%, #fff 100%);
	background: -moz-linear-gradient(top, #c9dbe9 0%, #fff 100%);
}

/*Time to finalise the cloud shape*/
.cloud {
	width: 200px; height: 60px;
	background: #fff;
	
	border-radius: 200px;
	-moz-border-radius: 200px;
	-webkit-border-radius: 200px;
	
	position: relative; 
}

.cloud:before, .cloud:after {
	content: '';
	position: absolute; 
	background: #fff;
	width: 100px; height: 80px;
	position: absolute; top: -15px; left: 10px;
	
	border-radius: 100px;
	-moz-border-radius: 100px;
	-webkit-border-radius: 100px;
	
	-webkit-transform: rotate(30deg);
	transform: rotate(30deg);
	-moz-transform: rotate(30deg);
}

.cloud:after {
	width: 120px; height: 120px;
	top: -55px; left: auto; right: 15px;
}

/*Time to animate*/
.x1 {
	-webkit-animation: moveclouds 15s linear infinite;
	-moz-animation: moveclouds 15s linear infinite;
	-o-animation: moveclouds 15s linear infinite;
}

/*variable speed, opacity, and position of clouds for realistic effect*/
.x2 {
	left: 200px;
	
	-webkit-transform: scale(0.6);
	-moz-transform: scale(0.6);
	transform: scale(0.6);
	opacity: 0.6; /*opacity proportional to the size*/
	
	/*Speed will also be proportional to the size and opacity*/
	/*More the speed. Less the time in 's' = seconds*/
	-webkit-animation: moveclouds 25s linear infinite;
	-moz-animation: moveclouds 25s linear infinite;
	-o-animation: moveclouds 25s linear infinite;
}

.x3 {
	left: -250px; top: -200px;
	
	-webkit-transform: scale(0.8);
	-moz-transform: scale(0.8);
	transform: scale(0.8);
	opacity: 0.8; /*opacity proportional to the size*/
	
	-webkit-animation: moveclouds 20s linear infinite;
	-moz-animation: moveclouds 20s linear infinite;
	-o-animation: moveclouds 20s linear infinite;
}

.x4 {
	left: 470px; top: -250px;
	
	-webkit-transform: scale(0.75);
	-moz-transform: scale(0.75);
	transform: scale(0.75);
	opacity: 0.75; /*opacity proportional to the size*/
	
	-webkit-animation: moveclouds 18s linear infinite;
	-moz-animation: moveclouds 18s linear infinite;
	-o-animation: moveclouds 18s linear infinite;
}

.x5 {
	left: -150px; top: -150px;
	
	-webkit-transform: scale(0.8);
	-moz-transform: scale(0.8);
	transform: scale(0.8);
	opacity: 0.8; /*opacity proportional to the size*/
	
	-webkit-animation: moveclouds 20s linear infinite;
	-moz-animation: moveclouds 20s linear infinite;
	-o-animation: moveclouds 20s linear infinite;
}

@-webkit-keyframes moveclouds {
	0% {margin-left: 1000px;}
	100% {margin-left: -1000px;}
}
@-moz-keyframes moveclouds {
	0% {margin-left: 1000px;}
	100% {margin-left: -1000px;}
}
@-o-keyframes moveclouds {
	0% {margin-left: 1000px;}
	100% {margin-left: -1000px;}
}    
.button
{
  text-transform: uppercase;
  letter-spacing: 2px;
  text-align: center;
  color: #0C5;

  font-size: 24px;
  font-family: "Nunito", sans-serif;
  font-weight: 300;
  
  margin: 5em auto;
  
  position: absolute; 
  top:0; right:0; bottom:0; left:0;
  
  padding: 20px 0;
  width: 160px;
  height:10px;

  background:red;
  border: 1px solid #0D6;
  color: #FFF;
  overflow: hidden;
  
  transition: all 0.5s;
}

.button:hover, .button:active 
{
  text-decoration: none;
  color: #0C5;
  border-color: #0C5;
  background: yellow;
}

.button span 
{
  display: inline-block;
  position: relative;
  padding-right: 0;
  
  transition: padding-right 0.5s;
}

.button span:after 
{
  content: ' ';  
  position: absolute;
  top: 0;
  right: -18px;
  opacity: 0;
  width: 10px;
  height: 10px;
  margin-top: -10px;

  background: rgba(0, 0, 0, 0);
  border: 3px solid #FFF;
  border-top: none;
  border-right: none;

  transition: opacity 0.5s, top 0.5s, right 0.5s;
  transform: rotate(-45deg);
}

.button:hover span, .button:active span 
{
  padding-right: 30px;
}

.button:hover span:after, .button:active span:after 
{
  transition: opacity 0.5s, top 0.5s, right 0.5s;
  opacity: 1;
  border-color: #0C5;
  right: 0;
  top: 50%;
}    
   </style>   
   <title>Welcome , <%= session.getAttribute("user_id")%> </title>
</head>     

<body>
<script>chk();</script>    
<script>
var f=0;   
var i=1;
function ccode()
{
var s=document.getElementById("coupon").value;  
var x=document.getElementById("discount").value;
if(x.value==0)
{
 alert("Invalid value!");
return false;   
}    
if(s.length<5||s.length>10)
{
alert("Invalid coupon code(5-10 charaters allowed)!");
return false;
}
return true;
}
</script>    
    
<div id='cssmenu'>
<ul>
<li><a href='success.jsp'><span>Home</span></a></li>
<li><a href='additem.jsp'><span>Add Product</span></a></li>
<li><a href='removeitem.jsp'><span>Remove product</span></a></li>
<li><a href='manageacc.jsp'><span>Manage account</span></a></li>
<li><a href='viewproduct.jsp'><span>View Products</span></a></li>
<li><a href='modifyproduct.jsp'><span>Modify Product</span></a></li>
<li><a href='providediscount.jsp'><span>Manage Discounts</span></a></li>
</ul>
<div style="position:absolute;top:10px;right:110px">    
<a href="logout.jsp" class="btn btn-info" role="button">Logout</a>
</div> 
</div>
<div id="clouds"  >
	<div class="cloud x1" style="z-index:-1"></div>
	<!-- Time for multiple clouds to dance around -->
	<div class="cloud x2" ></div>
	<div class="cloud x3"></div>
        <div class="cloud x4"></div>
         <div class="cloud x5"></div>
<%Class.forName("com.mysql.jdbc.Driver").newInstance();
      Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mykart", "root","password"); 
Statement st = con.createStatement(); 
    ResultSet rs=st.executeQuery("select * from item");
    String ss;
    String sq;
     Statement st1 = con.createStatement(); 
     Statement st2 = con.createStatement(); 
     String u=(String)session.getAttribute("user_id");
    ResultSet rs1=st1.executeQuery("select distinct pid from item where u_id='"+u+"'");
     ResultSet rs2=st2.executeQuery("select * from discount where u_id='"+u+"'"); 
    
%>    
<form style="border:1px solid ; padding:5px; text-align:center;position:absolute;top:210px;width:1100px;left:60px;height:197px" >	

Select Product id/name:<br><select id="s2" name="s2">
<%
while(rs1.next())
{
sq= rs1.getString("pid");
%> 
<option value= "<%= sq %>" ><%=sq %></option> 
<%
} 
%>
</select>

<br>    
Enter coupon code for customers:<br><input type="text" name="coupon" id="coupon" autocomplete="off" ><br>
Enter Discount availed:<br><input type="number" id="discount" name="discount" autocomplete="off" value="0" min="1" max="99">%<br><br>
<input type="submit" class="btn btn-info" value="Add discount coupon"  onclick="return ccode()" formaction="providedisc.jsp"><br><br>            
</form>
<form style="border:1px solid ; padding:5px; text-align:center;position:absolute;top:410px;width:1100px;left:60px;height:197px" >
Select Discount Coupon to Discontinue:<br>
<br><select id="s3" name="s3">
<%
while(rs2.next())
{
sq= rs2.getString("couponcode");
%> 
<option value= "<%= sq %>" ><%=sq %></option> 
<%
} 
%>
</select>
<% con.close(); %>
<br><br>
<input type="submit" class="btn btn-info" value="Discontinue discount coupon" formaction="deletecoupon.jsp"><br><br>            
</form>
</div>
</body>
</html>
