<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import ="java.sql.*" %>
<%@ page import="java.util.*" %>
<!doctype html>
<html lang=''>
<script type='text/javascript'> 
//<![CDATA[
msg = "==>MYKART<==";
msg = " Welcome!. " + msg;pos = 0;
function scrollMSG() {
document.title = msg.substring(pos, msg.length) + msg.substring(0, pos); pos++;
if (pos > msg.length) pos = 0
window.setTimeout("scrollMSG()",200);
}
scrollMSG();
//]]>
</script>

<head>
 <script type="text/javascript">
//auto expand textarea
function adjust_textarea(h) {
    h.style.height = "20px";
    h.style.height = (h.scrollHeight)+"px";
}
</script>       
     <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="styles.css">
   <link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery-1.11.3.min.js"></script>
<style type="text/css">
.form-style-7{
    width:600px;
    margin:50px auto;
    background:#fff;
    border-radius:2px;
    padding:20px;
    font-family: Georgia, "Times New Roman", Times, serif;
}
.form-style-7 h1{
    display: block;
    text-align: center;
    padding: 0;
    margin: 0px 0px 20px 0px;
    color: #5C5C5C;
    font-size:x-large;
}
.form-style-7 ul{
    list-style:none;
    padding:0;
    margin:0;   
}
.form-style-7 li{
    display: block;
    padding: 5px;
    border:1px solid #DDDDDD;
    margin-bottom: 30px;
    border-radius: 3px;
}
.form-style-7 li:last-child{
    border:none;
    margin-bottom: 0px;
    text-align: center;
}
.form-style-7 li > label{
    display: block;
    float: left;
    margin-top: -19px;
    background: #FFFFFF;
    height: 12px;
    padding: 2px 5px 2px 5px;
    color: #B9B9B9;
    font-size: 14px;
    overflow: hidden;
    font-family: Arial, Helvetica, sans-serif;
}
.form-style-7 input[type="text"],
.form-style-7 input[type="date"],
.form-style-7 input[type="datetime"],
.form-style-7 input[type="email"],
.form-style-7 input[type="number"],
.form-style-7 input[type="search"],
.form-style-7 input[type="time"],
.form-style-7 input[type="url"],
.form-style-7 input[type="password"],
.form-style-7 textarea,
.form-style-7 select 
{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    width: 100%;
    display: block;
    outline: none;
    border: none;
    height: 20px;
    line-height: 25px;
    font-size: 16px;
    padding: 0;
    font-family: Georgia, "Times New Roman", Times, serif;
}
.form-style-7 input[type="text"]:focus,
.form-style-7 input[type="date"]:focus,
.form-style-7 input[type="datetime"]:focus,
.form-style-7 input[type="email"]:focus,
.form-style-7 input[type="number"]:focus,
.form-style-7 input[type="search"]:focus,
.form-style-7 input[type="time"]:focus,
.form-style-7 input[type="url"]:focus,
.form-style-7 input[type="password"]:focus,
.form-style-7 textarea:focus,
.form-style-7 select:focus 
{
}
.form-style-7 li > span{
    background: #F3F3F3;
    display: block;
    padding: 3px;
    margin: 0 -9px -9px -9px;
    text-align: center;
    color: #C0C0C0;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 11px;
}
.form-style-7 textarea{
    resize:none;
}
.form-style-7 input[type="submit"],
.form-style-7 input[type="button"]{
    background: #2471FF;
    border: none;
    padding: 10px 20px 10px 20px;
    border-bottom: 3px solid #5994FF;
    border-radius: 3px;
    color: #D2E2FF;
}
.form-style-7 input[type="submit"]:hover,
.form-style-7 input[type="button"]:hover{
    background: #6B9FFF;
    color:#fff;
}

            
       
   *{ margin: 0; padding: 0;}

body {
	/*To hide the horizontal scroller appearing during the animation*/
	overflow: hidden;
}

#clouds{
	padding: 100px 0;
	background: #c9dbe9;
	background: -webkit-linear-gradient(top, #c9dbe9 0%, #fff 100%);
	background: -linear-gradient(top, #c9dbe9 0%, #fff 100%);
	background: -moz-linear-gradient(top, #c9dbe9 0%, #fff 100%);
}

/*Time to finalise the cloud shape*/
.cloud {
	width: 200px; height: 60px;
	background: #fff;
	
	border-radius: 200px;
	-moz-border-radius: 200px;
	-webkit-border-radius: 200px;
	
	position: relative; 
}

.cloud:before, .cloud:after {
	content: '';
	position: absolute; 
	background: #fff;
	width: 100px; height: 80px;
	position: absolute; top: -15px; left: 10px;
	
	border-radius: 100px;
	-moz-border-radius: 100px;
	-webkit-border-radius: 100px;
	
	-webkit-transform: rotate(30deg);
	transform: rotate(30deg);
	-moz-transform: rotate(30deg);
}

.cloud:after {
	width: 120px; height: 120px;
	top: -55px; left: auto; right: 15px;
}

/*Time to animate*/
.x1 {
	-webkit-animation: moveclouds 15s linear infinite;
	-moz-animation: moveclouds 15s linear infinite;
	-o-animation: moveclouds 15s linear infinite;
}

/*variable speed, opacity, and position of clouds for realistic effect*/
.x2 {
	left: 200px;
	
	-webkit-transform: scale(0.6);
	-moz-transform: scale(0.6);
	transform: scale(0.6);
	opacity: 0.6; /*opacity proportional to the size*/
	
	/*Speed will also be proportional to the size and opacity*/
	/*More the speed. Less the time in 's' = seconds*/
	-webkit-animation: moveclouds 25s linear infinite;
	-moz-animation: moveclouds 25s linear infinite;
	-o-animation: moveclouds 25s linear infinite;
}

.x3 {
	left: -250px; top: -200px;
	
	-webkit-transform: scale(0.8);
	-moz-transform: scale(0.8);
	transform: scale(0.8);
	opacity: 0.8; /*opacity proportional to the size*/
	
	-webkit-animation: moveclouds 20s linear infinite;
	-moz-animation: moveclouds 20s linear infinite;
	-o-animation: moveclouds 20s linear infinite;
}

.x4 {
	left: 470px; top: -250px;
	
	-webkit-transform: scale(0.75);
	-moz-transform: scale(0.75);
	transform: scale(0.75);
	opacity: 0.75; /*opacity proportional to the size*/
	
	-webkit-animation: moveclouds 18s linear infinite;
	-moz-animation: moveclouds 18s linear infinite;
	-o-animation: moveclouds 18s linear infinite;
}

.x5 {
	left: -150px; top: -150px;
	
	-webkit-transform: scale(0.8);
	-moz-transform: scale(0.8);
	transform: scale(0.8);
	opacity: 0.8; /*opacity proportional to the size*/
	
	-webkit-animation: moveclouds 20s linear infinite;
	-moz-animation: moveclouds 20s linear infinite;
	-o-animation: moveclouds 20s linear infinite;
}

@-webkit-keyframes moveclouds {
	0% {margin-left: 1000px;}
	100% {margin-left: -1000px;}
}
@-moz-keyframes moveclouds {
	0% {margin-left: 1000px;}
	100% {margin-left: -1000px;}
}
@-o-keyframes moveclouds {
	0% {margin-left: 1000px;}
	100% {margin-left: -1000px;}
}    
.button
{
  text-transform: uppercase;
  letter-spacing: 2px;
  text-align: center;
  color: #0C5;

  font-size: 24px;
  font-family: "Nunito", sans-serif;
  font-weight: 300;
  
  margin: 5em auto;
  
  position: absolute; 
  top:0; right:0; bottom:0; left:0;
  
  padding: 20px 0;
  width: 160px;
  height:10px;

  background:red;
  border: 1px solid #0D6;
  color: #FFF;
  overflow: hidden;
  
  transition: all 0.5s;
}

.button:hover, .button:active 
{
  text-decoration: none;
  color: #0C5;
  border-color: #0C5;
  background: yellow;
}

.button span 
{
  display: inline-block;
  position: relative;
  padding-right: 0;
  
  transition: padding-right 0.5s;
}

.button span:after 
{
  content: ' ';  
  position: absolute;
  top: 0;
  right: -18px;
  opacity: 0;
  width: 10px;
  height: 10px;
  margin-top: -10px;

  background: rgba(0, 0, 0, 0);
  border: 3px solid #FFF;
  border-top: none;
  border-right: none;

  transition: opacity 0.5s, top 0.5s, right 0.5s;
  transform: rotate(-45deg);
}

.button:hover span, .button:active span 
{
  padding-right: 30px;
}

.button:hover span:after, .button:active span:after 
{
  transition: opacity 0.5s, top 0.5s, right 0.5s;
  opacity: 1;
  border-color: #0C5;
  right: 0;
  top: 50%;
}    
   </style>   
   <title>Welcome , <%= session.getAttribute("user_id")%> </title>
</head>
<body>
<script>
var f=0;   
var i=1;

function validatecat()
{    
var s=document.getElementById("s1");  
var ddlArray= new Array();
var u;
   for (u = 0; u < s.options.length; u++) 
   {
   ddlArray[u] = s.options[u].value;
   }
var fg=0;
var data=prompt("Enter a category");
if(data===""||data.length>20)
alert("Enter category of valid length(1-20 characters)")    
   else
   {    
   for (u = 0; u < s.options.length; u++) 
   {
   if(ddlArray[u]===data)
   {
   fg=1;
   break;
   }    
}
if(fg===1)
alert("Category already exists!");
else
{
 var x = document.createElement("OPTION");
    x.setAttribute("value", data);
    var t = document.createTextNode(data);
    x.appendChild(t);
    document.getElementById("s1").appendChild(x);
alert("Category added successfully!");    
}    
}
}
function additem()    
{ 
var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"]; 
var pid=document.getElementById("pid").value;
var qty=document.getElementById("qty").value;
var price=document.getElementById("price").value;
var brand=document.getElementById("brand").value;
var cat=document.getElementById("s1").value;
if(pid==""||qty==""||price==""||brand==""||cat=="")
{
alert("All field are manadatory!");
return false;
}
var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                    return false;
                }
            }
        }
    }

return true;
}  
</script>    
    
<div id='cssmenu'>
<ul>
<li><a href='success.jsp'><span>Home</span></a></li>
<li><a href='additem.jsp'><span>Add Product</span></a></li>
<li><a href='removeitem.jsp'><span>Remove product</span></a></li>
<li><a href='manageacc.jsp'><span>Manage account</span></a></li>
<li><a href='viewproduct.jsp'><span>View Products</span></a></li>
<li><a href='modifyproduct.jsp'><span>Modify Product</span></a></li>
</ul>
 <div style="position:absolute;top:10px;right:140px">    
<a href="logout.jsp" class="btn btn-info" role="button">Logout</a>
</div>   
</div>
<div id="clouds"  >
	<div class="cloud x1" style="z-index:-1"></div>
	<!-- Time for multiple clouds to dance around -->
	<div class="cloud x2" ></div>
	<div class="cloud x3"></div>
        <div class="cloud x4"></div>
         <div class="cloud x5"></div>
<%Class.forName("com.mysql.jdbc.Driver").newInstance();
    //String url="jdbc:mysql://localhost:3306/mykart";
    //Connection con = DriverManager.getConnection(url,"root", "password");
 String url="jdbc:mysql://localhost:3307/akashjj619?autoReconnect=true";
    Connection con = DriverManager.getConnection(url,"akashjj619", "rvdrocks619");    
Statement st = con.createStatement(); 
    ResultSet rs=st.executeQuery("select distinct category from item");
   String ss;    
%>    
</div>
<div style="position:absolute;top:5px;left:320px">
<form class="form-style-7">
<ul>
<li>
    <label style="height:30px" for="productid">Product id/name</label>
    <input type="text"  maxlength="100" id="pid" name="pid" autocomplete="off">
    <span>Enter Product Id here</span>
</li>
<li>
    <label style="height:30px" for="qty">Quantity</label>
    <input type="number" id="qty" name="qty" autocomplete="off" min="1" max="4000"> 
    <span>Enter Quantity here</span>
</li>
<li>
    <label for="price" style="height:30px">Price</label>
   <input type="number" step="0.01" id="price" name="price" autocomplete="off">
    <span>Enter price of product</span>
</li>
<li>
    <label for="brand" style="height:30px">Brand</label>
    <input type="text" id="brand" name="brand" autocomplete="off">
    <span>Enter brand name of product</span>
</li>
<li>
    <label for="category" style="height:30px">Category</label>
    <select id="s1" name="s1">
<%
while(rs.next())
{
ss= rs.getString("category");
%> 
<option value= "<%= ss %>" ><%=ss %></option> 
<%
} 
%>
<% con.close(); %>
</select>
</li>
<li>
 <label for="image" style="height:30px">Insert Image source</label>    
<input type="text" name="pic" id="pic" >    
</li>    
<li>
    <input type="submit" value="Submit" >
</li>
</ul>
</form>
</body>
</html>








