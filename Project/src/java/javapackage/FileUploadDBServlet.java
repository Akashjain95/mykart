package javapackage;
import java.sql.ResultSet;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 * Servlet implementation class FileUploadDBServlet
 */
@WebServlet("/FileUploadDBServlet")
@MultipartConfig(maxFileSize = 10177215) // upload file's size up to 16MB
public class FileUploadDBServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUploadDBServlet() {
        super();
    }
 //String url="jdbc:mysql://localhost:3307/akashjj619?autoReconnect=true";
   // Connection con = DriverManager.getConnection(url,"akashjj619", "rvdrocks619");
   /* private String dbURL = "jdbc:mysql://localhost:3306/mykart";
    private String dbUser = "root";
    private String dbPass = "password";*/
     private String dbURL = "jdbc:mysql://localhost:3307/akashjj619?autoReconnect=true";
    private String dbUser = "akashjj619";
    private String dbPass = "rvdrocks619";
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // gets values of text fields
        PrintWriter out = response.getWriter();
        HttpSession session=request.getSession();
      String pid = request.getParameter("pid");
    int q = Integer.parseInt(request.getParameter("qty"));
     int price = Integer.parseInt(request.getParameter("price"));
    String brand = request.getParameter("brand");
    String cat=request.getParameter("s1");
    String des=request.getParameter("description");
    String uid = (String)session.getAttribute("user_id"); 
        /*String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");*/
         
        InputStream inputStream = null; // input stream of the upload file
         
        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("pic");
        if (filePart != null) {
            // prints out some information for debugging
            System.out.println(filePart.getName());
            System.out.println(filePart.getSize());
            System.out.println(filePart.getContentType());
             
            // obtains input stream of the upload file
            inputStream = filePart.getInputStream();
        }
         String Message;
        Connection conn = null; // connection to the database
        String message = null;  // message will be sent back to client
            // connects to the database
         try{  
             DriverManager.registerDriver(new com.mysql.jdbc.Driver());
             conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mykart", "root","password");
            //conn = DriverManager.getConnection(dbURL, dbUser, dbPass);
             Statement st = conn.createStatement();   
   Statement st1 = conn.createStatement();   
    ResultSet rs=st.executeQuery("select * from item where u_id='" + uid + "' and pid='"+pid+"'");
    ResultSet rs1=st1.executeQuery("select * from item where u_id='" + uid + "' and category='" +cat + "' and brand ='" + brand + "' and pid='"+pid+"'");  
        if(rs.next())
       { 
         if(rs1.next())
         {   
         String h=rs.getString(2);
       int t=q+Integer.parseInt(h);
       String query = "update item set qty = ? where u_id = ? and pid= ?";
      PreparedStatement preparedStmt = conn.prepareStatement(query);
      preparedStmt.setInt(1, t);
      preparedStmt.setString(2, uid);
      preparedStmt.setString(3, pid);
      // execute the java preparedstatement
      preparedStmt.executeUpdate();
      st.close();
      st1.close();
      rs.close();
      rs1.close();
      preparedStmt.close();
      conn.close();
      out.println("<script type=\"text/javascript\">");  
out.println("alert('Product quantity increased!');");
out.println("location='additem.jsp';");
out.println("</script>");
 getServletContext().getRequestDispatcher("/additem.jsp").forward(request, response);
     }
      else     
         { st.close();
      st1.close();
      rs.close();
      rs1.close();    
             conn.close();
      out.println("<script type=\"text/javascript\">");  
out.println("alert('Product id already exists');");
out.println("location='additem.jsp';");
out.println("</script>");
 getServletContext().getRequestDispatcher("/additem.jsp").forward(request, response);       
     /* %>
         <script>
          alert(" product id already exists!" );
          window.location = 'additem.jsp';
     </script>  
         
         <%*/}       
       }
       else
       {
       try
       {    
      PreparedStatement psmt=conn.prepareStatement("insert into item(pid,qty,price,brand,category,u_id,pic)" + "values(?,?,?,?,?,?,?)");
psmt.setString(1,pid);
psmt.setInt(2,q);
psmt.setInt(3,price);
psmt.setString(4,brand);
psmt.setString(5,cat);
psmt.setString(6,uid);      
            if (inputStream != null) {
                // fetches input stream of the upload file for the blob column
                psmt.setBlob(7, inputStream);
            }
PreparedStatement psm =conn.prepareStatement("insert into itemdesc(description,pid)" + "values(?,?)");
       psm.setString(1,des);
       psm.setString(2,pid);
       int k=psm.executeUpdate();     
psm.close(); 
            // sends the statement to the database server
            int row = psmt.executeUpdate();       
response.setContentType("text/html");  
out.println("<script type=\"text/javascript\">");  
out.println("alert('Product addedd succesfully!');");  
out.println("location='additem.jsp';");
out.println("</script>");
getServletContext().getRequestDispatcher("/additem.jsp").forward(request, response);
       }
  catch(Exception e)
  {
 response.setContentType("text/html");  
out.println("<script type=\"text/javascript\">");  
out.println("alert('Error in file type!');");  
out.println("location='f2.jsp';");
out.println("</script>");
getServletContext().getRequestDispatcher("/f2.jsp").forward(request, response);     
  }  }
         } catch (SQLException ex) {
             response.setContentType("text/html");  
out.println("<script type=\"text/javascript\">");  
out.println("alert('Error!');");  
out.println("location='f2.jsp';");
out.println("</script>");
getServletContext().getRequestDispatcher("/f2.jsp").forward(request, response);
            Logger.getLogger(FileUploadDBServlet.class.getName()).log(Level.SEVERE, null, ex);
        }}}
            /*String s = "select * from employee where user_id=?";
            PreparedStatement st = conn.prepareStatement(s);
            st.setString(1, firstname);  
           ResultSet rs= st.executeQuery();
           int f=0;
          if(!rs.next())
          {
           f=1;
           message="okay";
          }
          if(f==1)
          {  
            String sql = "INSERT INTO employee (user_id, pwd, photo) values (?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, firstname);
            statement.setString(2, lastname);
          
             
            if (inputStream != null) {
                // fetches input stream of the upload file for the blob column
                statement.setBlob(3, inputStream);
            }
 
            // sends the statement to the database server
            int row = statement.executeUpdate();
            PrintWriter out = response.getWriter();  
response.setContentType("text/html");  
out.println("<script type=\"text/javascript\">");  
out.println("alert('Please login to continue!');");  
out.println("location='emplogin.jsp';");
out.println("</script>");
 //getServletContext().getRequestDispatcher("/emplogin.jsp").forward(request, response);
          }
          else
          {
           PrintWriter out = response.getWriter();  
response.setContentType("text/html");  
out.println("<script type=\"text/javascript\">");  
out.println("alert('User id already exists');");
out.println("location='employee.jsp';");
out.println("</script>");
 getServletContext().getRequestDispatcher("/employee.jsp").forward(request, response);
          }    
         }
         catch(Exception e)
         {
             
         }
       
                 
         
            // sets the message in request scope
             
        // forwards to the message page
        }*/
    
