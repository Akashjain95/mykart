Mykart is a user-friendly and interactive e-commerce application for sellers and buyers. Web
application allows sellers to manage their account,add/remove products,view and analyse the
statistics of their sales and revenues.It also boasts of an integrated chat app which allows sellers to answer to queries/complaints of customers and hence retain and build relationship with their customer.
Android app allows customers to view different products,filter products on basis of
price,availability,popularity.With added functionality to chat with other customers and sellers.With other interesting functionalities like review,rate product,shake device to view offers,add to cart.

Android  application functionalities are as listed :
       1.Trendy Animated splash  screen
       2.Attractive login and signup interface
       3.Trendy flipview step through guide for customer
       4.Password recovery functionality 
               a)Through sms
               b)Through  email 
       5.Google places api to select delivery address
       6.Easy to use,handy Navigation drawer bar. 
      7.Shake device and get latest offers for the day functionality
       Description:Offers uploaded by seller through seller hub.They are fetched 
       randomly from the database.
     8.Add and remove items from cart functionality
      9.Filter items  on basis of 
             a)Prices low to high
              b)Prices high to low
              c)Popularity of product
     10.Add and view other people’s review on products
         with added feature to know buyer is certified or not
     11.COD functionality for customers
      Description:Customer can add multiple delivery addresses and choose from
      them
     12.Apply coupon code on products for added benefits
      Description:Coupon code provided by  sellers for attracting customers.
     13.Send feedback through e-mail
     Description:Implemented through Gmail Intent
     14.Dynamic search bar for searching products
        and filter products by item name
      15.Functionality to interact with other customers by allowing friends to share/unshare and view each others purchases
    16.Functionality to send/view message to each other /chat with each other for
     customers
    17.Functionality to register queries/complaints with sellers by allowing to register query/complaint 
    18.Allows to cutomers to see reply for query/complaints from sellers

Web application functionalities are as listed :
    WEB APPLICATION FOR SELLERS BOASTS OF SIMPLE INTERFACE TO    PRACTICALLY RENDER SELLERS OF DIFFERENT EDUCATIONAL BACKGROUND TO EASILY INTERACT WITH THE FUNCTIONALITITES.
      1.Sign up 
      2.Login 
      3.Add products for selling
      4.Remove products from selling
       5.Modify products availaible on sale
       6.View products available on sale
       7.Feedback to admin (through Gmail Intent)
       8.Manage  account like changing password,deleting account
      9. Discount Coupon code for offers
       Description:Allows sellers to upload coupon code for customer
     10.Use of bx slider for sellers to view steps for selling
    11.Functionality to add item to offer zone for customers
    12.Functionality to chat/messaging with customers and to handle their queries/complaints
  How to use:
1.Install APK on android device
2.Deploy web app on netbeans